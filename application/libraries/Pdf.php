<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Pdf {
    
    function Pdf()
    {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }

    function load($param = NULL)
    {
        include_once APPPATH.'third_party/mpdf/mpdf.php';

        if ($param == NULL){
            $param = '"utf-8","A4","","",5,5,5,5,6,3';         
        }

        return new mPDF('utf-8','A4', 0, '', 15, 15, 15, 15, 3, 3, 'P');
    }
}