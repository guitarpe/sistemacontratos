<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Propostas extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('propostas_model', 'propostas_model', TRUE);
    }

    public function index()
    {

    }

    //LISTA PROPOSTAS
    function getlistapropostas()
    {
        $sess_data = $this->session->userdata('user');

        $token = $sess_data['token'];

        $order = $this->input->get("order");

        $list = $this->propostas_model->getpropostas($token);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $customers) {

            $date = new DateTime($customers->PRP_DT_CREATED);

            $row = array();
            $row[] = $customers->PRP_ID;
            $row[] = $customers->STATUSVIEW == 'S' ? '<a href="' . base_url() . 'visualizar?tipo=p&id=' . $customers->PRP_ID . '" title="Clique para Visualizar" class="link_view" target="_blank">' . $customers->PRP_DESC . '</a>' : $customers->PRP_DESC;
            $row[] = $customers->CLI_CONTATO_NOME;
            $row[] = $customers->USER_CREATED;
            $row[] = $customers->SERV_DESC;
            $row[] = $date->format('d/m/Y');
            $row[] = $customers->SITUACAO;
            $row[] = $customers->STATUSVIEW == 'S' ? $customers->STATUS : 'Inativo';
            $row[] = null;
            $row[] = null;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->propostas_model->todospropostas($token),
            "recordsFiltered" => $this->propostas_model->filtropropostas($token),
            "data" => $data,
        );

        echo json_encode($output);
    }

    function removeacentosprop()
    {
        $this->load->helper("funcoes");
        $str = $this->input->post('string');

        $contador = $this->propostas_model->verificaExiste($str);

        if (intval($contador) > 0) {
            echo removeracentos($str . '_' . intval($contador + 1));
        } else {
            echo removeracentos($str);
        }
    }

    function insertedit()
    {

        if ($this->session->userdata('user')) {
            $sess_data = $this->session->userdata('user');

            $token = $sess_data['token'];

            if ($this->propostas_model->validaToken($token)) {

                $this->load->helper("funcoes");
                $this->load->library('form_validation');

                $modulo = $this->input->post('nomemodulo');
                $id = $this->input->post('id');

                $infosis = $this->propostas_model->infoSistema($modulo);

                if ($id == null) {
                    $is_unique = ''; //'|is_unique[TB_PROPOSTAS.PRP_DESC]';
                    $is_unique_url = '|is_unique[TB_PROPOSTAS.PRP_URL_INFO]';
                } else {
                    $is_unique = '';
                    $is_unique_url = '';
                }

                $emailpadrao = $this->input->post('email_padrao');
                $urlpadrao = $this->input->post('url_padrao');

                $this->form_validation->set_rules('descricao', ('Descrição da Proposta'), 'trim|required|xss_clean' . $is_unique);
                $this->form_validation->set_rules('modelo', ('Modelo de proposta'), 'trim|required|xss_clean|callback_check_default');

                $this->form_validation->set_rules('servico', ('Serviço'), 'trim|required|xss_clean|callback_check_default');

                $servico = $this->input->post('servico');

                if ($servico == 'outro') {
                    $this->form_validation->set_rules('outro_servicos', ('Digite o tipo de Serviço'), 'trim|required|xss_clean');
                }

                $this->form_validation->set_rules('nome_contato', ('Contato da Empresa'), 'trim|required|xss_clean');
                $this->form_validation->set_rules('email_contato', ('E-mail do Contato'), 'trim|required|xss_clean|callback_validar_any_email');
                $this->form_validation->set_rules('modelo_contrato', ('Modelo de Contrato'), 'trim|required|xss_clean|callback_check_default');

                if ((int) $infosis[0]->CONF_TROCA_EMAIL == 1) {
                    if ($emailpadrao != 'sim') {
                        $this->form_validation->set_rules('email_padrao', ('Email para Notificação'), 'trim|required|xss_clean');
                    }
                }

                if ($urlpadrao != 'sim') {
                    $this->form_validation->set_rules('urlproposta', ('Url da proposta'), 'trim|required|xss_clean' . $is_unique_url);
                }

                $this->form_validation->set_rules('valor_total', ('Valor á vista'), 'trim|required|xss_clean');
                $this->form_validation->set_rules('valor_parcelado', ('Valor Parcelado'), 'trim|required|xss_clean');
                $this->form_validation->set_rules('parcelas', ('Qtde. de Parcelas'), 'trim|required|xss_clean');
                $this->form_validation->set_rules('intervalo_parcelas', ('Intervalo entre Parcelas (dias)'), 'trim|required|xss_clean');

                $this->form_validation->set_message('is_unique', ('O valor do %s já está registrado no sistema'));
                $this->form_validation->set_message('required', ('O campo %s é obrigatório'));
                $this->form_validation->set_message('valid_email', ('O %s digitado não é válido'));
                $this->form_validation->set_message('validar_any_email', ('No campo %s foi digitado um algum item inválido'));
                $this->form_validation->set_message('check_default', ('O campo %s é obrigatório'));

                if ($this->form_validation->run() == FALSE) {
                    $errors = $this->form_validation->error_array();
                    echo json_encode(array('erro' => true, 'message' => false, 'list' => json_encode($errors)));
                } else {

                    if ((int) $infosis[0]->CONF_TROCA_EMAIL == 1) {
                        if ($emailpadrao != 'sim') {
                            $emailpadrao = 'nao';
                        }
                    } else {
                        $emailpadrao = 'nao';
                    }

                    if ($urlpadrao != 'sim') {
                        $urlpadrao = 'nao';
                    }

                    $servico = $this->input->post('servico');

                    if ($servico != 'outro') {
                        $serv = $this->propostas_model->get("servicos", $servico);
                        $servico_info = $serv[0]->nome;
                    } else {
                        $servico_info = $this->input->post('outro_servicos');
                    }

                    $data = array(
                        'token' => $token,
                        'PRP_EMAIL_DEFAULT' => $emailpadrao,
                        'PRP_URL_DEFAULT' => $urlpadrao,
                        'PRP_SEND_MAIL' => $this->input->post('email'),
                        'PRP_URL_INFO' => $this->input->post('urlproposta'),
                        'MDL_ID' => $this->input->post('modelo'),
                        'PRP_DESC' => $this->input->post('descricao'),
                        'SERV_ID' => $servico,
                        'SERV_DESC' => $servico_info,
                        'PRP_VALOR_TOTAL' => $this->input->post('valor_total'),
                        'PRP_VALOR_PARCELADO' => $this->input->post('valor_parcelado'),
                        'PRP_QTDE_FARMACIAS' => $this->input->post('qde_farmacias'),
                        'PRP_PARCELAS' => $this->input->post('parcelas'),
                        'PRP_INTERVALO_PARCELAS' => $this->input->post('intervalo_parcelas'),
                        'CTT_CONVENIOS' => $this->input->post('convenios'),
                        'CLI_CONTATO_NOME' => $this->input->post('nome_contato'),
                        'CLI_EMAIL_CONTATO' => $this->input->post('email_contato'),
                        'CTT_ID' => $this->input->post('modelo_contrato')
                    );

                    if ($id == null) {
                        $retorno = $this->propostas_model->insert($modulo, $data);

                        if (!$retorno) {
                            echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                        } else {
                            echo json_encode(array('erro' => false, 'message' => false, 'list' => array('id' => $retorno)));
                        }
                    } else {
                        $retorno = $this->propostas_model->edit($modulo, $data, $id);

                        if (!$retorno) {
                            echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                        } else {
                            echo json_encode(array('erro' => false, 'message' => false, 'list' => array('id' => $retorno)));
                        }
                    }
                }
            } else {
                $this->logout();
            }
        }
    }

    function delete()
    {
        if ($this->session->userdata('user')) {
            $sess_data = $this->session->userdata('user');

            $token = $sess_data['token'];

            $modulo = $this->input->post('nomemodulo');
            $id = $this->input->post('id');

            if ($this->propostas_model->validaToken($token)) {

                if ($id != null) {
                    if ($this->propostas_model->delete($modulo, $id)) {
                        echo json_encode(array('erro' => false, 'message' => false, 'list' => null));
                    } else {
                        echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                    }
                } else {
                    echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                }
            } else {
                $this->logout();
            }
        }
    }

    function ativarInativar()
    {
        if ($this->session->userdata('user')) {
            $sess_data = $this->session->userdata('user');

            $token = $sess_data['token'];

            $modulo = $this->input->post('nomemodulo');
            $id = $this->input->post('id');

            if ($this->propostas_model->validaToken($token)) {

                if ($id != null) {
                    if ($this->propostas_model->ativarInativar($modulo, $id)) {
                        echo json_encode(array('erro' => false, 'message' => false, 'list' => null));
                    } else {
                        echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                    }
                } else {
                    echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                }
            } else {
                $this->logout();
            }
        }
    }

    function get()
    {
        if ($this->session->userdata('user')) {
            $sess_data = $this->session->userdata('user');

            $token = $sess_data['token'];

            if ($this->propostas_model->validaToken($token)) {

                $modulo = $this->input->post('nomemodulo');
                $id = $this->input->post('id');

                if ($id != null) {
                    echo json_encode(array('erro' => false, 'message' => false, 'list' => json_encode($this->propostas_model->get($modulo, $id))));
                } else {
                    echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                }
            } else {
                $this->logout();
            }
        }
    }

    function duplicar()
    {
        if ($this->session->userdata('user')) {
            $sess_data = $this->session->userdata('user');

            $token = $sess_data['token'];

            if ($this->propostas_model->validaToken($token)) {

                $modulo = $this->input->post('nomemodulo');
                $id = $this->input->post('id');

                if ($this->propostas_model->duplicar($modulo, $id, $token)) {
                    echo json_encode(array('erro' => false, 'message' => false, 'list' => null));
                } else {
                    echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                }
            } else {
                $this->logout();
            }
        }
    }

    function logout()
    {

        $sess_data = $this->session->userdata('user');

        $token = $sess_data['token'];

        $acesso = array(
            'CONNECTED' => 0
        );

        if ($this->propostas_model->disconnect($token, $acesso)) {
            $this->session->unset_userdata('user');
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
    }

    function check_default($post_string)
    {
        return check_default($post_string);
    }

    function valid_email($email)
    {
        return validar_email($email);
    }

    function validar_any_email($emails)
    {

        $anyemails = explode(',', $emails);

        $count = 0;

        foreach ($anyemails as $email) {
            if (!validar_email($email)) {
                $count++;
            }
        }

        if ($count > 0) {
            return false;
        } else {
            return true;
        }
    }
}

?>