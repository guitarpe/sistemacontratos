<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('admin_model', 'admin_model', TRUE);
    }

    public function index()
    {
        $this->load->helper("funcoes");

        $this->load->library('pdf');

        $id = $this->input->get('id');

        $tipo = $this->input->get('tipo');

        $hoje = date('Y_m_d_H_i');

        $file = null;

        if ($tipo != null) {
            if ($tipo == 'p') {
                $doc = null;
                $proposta = $this->admin_model->getModeloPropostaId($id);

                $file = "Proposta_" . $proposta->PRP_ID . "_" . $proposta->PRP_IP_CLIENTE . '_' . $hoje;

                if ($this->admin_model->gerarLayoutProposta($proposta->MDL_ID, $id)) {
                    $doc = $this->admin_model->getPropostaId($id)[0];
                }
            } elseif ($tipo == 'c') {
                $doc = null;
                $contrato = $this->admin_model->getModeloContratoId($id);

                $file = "Contrato_" . $contrato->CTT_ID . '_' . $contrato->CTT_IP_CLIENTE . '_' . $hoje;

                if ($this->admin_model->gerarLayoutContrato($contrato->MDL_ID, $id)) {
                    $doc = $this->admin_model->getContratoId($id)[0];
                }
            }
        } else {
            if ($tipo == 'p') {
                $file = "Visualizar Proposta";
            } elseif ($tipo == 'c') {
                $file = "Visualizar Contrato";
            }
        }


        $data = array(
            'infodoc' => $doc,
            'tipo' => $tipo
        );

        $html = $this->load->view('pdf', $data, TRUE);
        $pdfFilePath = $file . ".pdf";

        $pdf = $this->pdf->load();

        //$style = file_get_contents(base_url() . 'assets/css/pdf.css');
        //$pdf->WriteHTML($style, 1);
        $PDFContent = mb_convert_encoding(utf8_decode($html), 'UTF-8', 'ISO-8859-1');
        $pdf->WriteHTML($PDFContent, 2);
        $pdf->Output($pdfFilePath, "D");
    }

    public function dataExtenso($datainfo)
    {
        $this->load->helper("funcoes");

        echo dataInfoExtenso($datainfo);
    }
}

?>