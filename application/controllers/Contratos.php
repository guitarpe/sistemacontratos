<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contratos extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('contratos_model', 'contratos_model', TRUE);
    }

    public function index()
    {

    }

    //LISTA CONTRATOS
    function getlistacontratos()
    {
        $sess_data = $this->session->userdata('user');

        $token = $sess_data['token'];

        $list = $this->contratos_model->getcontratos($token);

        $order = $this->input->get("order");

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $customers) {

            $date = new DateTime($customers->CTT_DT_CREATE);

            $row = array();
            $row[] = $customers->CTT_ID;
            $row[] = $customers->STATUSVIEW == 'S' ? '<a href="' . base_url() . 'visualizar?tipo=c&id=' . $customers->CTT_ID . '" title="Clique para Visualizar" class="link_view" target="_blank">' . $customers->CTT_DESC . '</a>' : $customers->CTT_DESC;
            $row[] = $customers->CLI_RAZAO_SOCIAL;
            $row[] = $customers->USER_CREATED;
            $row[] = $customers->SERV_DESC;
            $row[] = $date->format('d/m/Y');
            $row[] = $customers->STATUSVIEW == 'S' ? $customers->STATUS : 'Inativo';
            $row[] = $customers->ENVIADO;
            $row[] = null;
            $row[] = $customers->CTT_TAGS;
            $row[] = null;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->contratos_model->todoscontratos($token),
            "recordsFiltered" => $this->contratos_model->filtrocontratos($token),
            "data" => $data,
        );

        echo json_encode($output);
    }

    function removeracentosctt()
    {
        $this->load->helper("funcoes");
        $str = $this->input->post('string');

        $contador = $this->contratos_model->verificaExiste($str);

        if (intval($contador) > 0) {
            echo removeracentos($str . '_' . intval($contador + 1));
        } else {
            echo removeracentos($str);
        }
    }

    function insertedit()
    {

        if ($this->session->userdata('user')) {
            $sess_data = $this->session->userdata('user');

            $token = $sess_data['token'];

            if ($this->contratos_model->validaToken($token)) {

                $this->load->helper("funcoes");
                $this->load->library('form_validation');

                $modulo = $this->input->post('nomemodulo');
                $id = $this->input->post('id');

                $infosis = $this->contratos_model->infoSistema($modulo);

                if ($id == null) {
                    $is_unique = '|is_unique[TB_CONTRATOS.CTT_DESC]';
                    $is_unique_url = '|is_unique[TB_CONTRATOS.CTT_URL_INFO]';
                } else {
                    $is_unique = '';
                    $is_unique_url = '';
                }

                $emailpadrao = $this->input->post('email_padrao');
                $urlpadrao = $this->input->post('url_padrao');
                $parcelas = $this->input->post('parcelas');

                $forma_pagamento = $this->input->post('forma_pagamento');

                $this->form_validation->set_rules('descricao', ('Descrição do Contrato'), 'trim|required|xss_clean' . $is_unique);
                $this->form_validation->set_rules('modelo_contrato', ('Modelo de Contrato'), 'trim|required|xss_clean|callback_check_default');
                $this->form_validation->set_rules('servico', ('Serviço'), 'trim|required|xss_clean|callback_check_default');
                $this->form_validation->set_rules('forma_pagamento', ('Forma de Pagamento'), 'trim|required|xss_clean|callback_check_default');

                if ((int) $infosis[0]->CONF_TROCA_EMAIL == 1) {
                    if ($emailpadrao != 'sim') {
                        $this->form_validation->set_rules('email_notificacao', ('Email para Notificação'), 'trim|required|xss_clean|callback_validar_any_email');
                    }
                }

                if ($urlpadrao != 'sim') {
                    $this->form_validation->set_rules('urlcontrato', ('Url da proposta'), 'trim|required|xss_clean' . $is_unique_url);
                }

                //$this->form_validation->set_rules('razao_social', ('Razão Social'), 'trim|required|xss_clean');
                $this->form_validation->set_rules('tags', ('Tags'), 'trim|required|xss_clean');
                $this->form_validation->set_rules('modelo_contrato', ('Modelo de Contrato'), 'trim|required|xss_clean|callback_check_default');
                $this->form_validation->set_rules('valor_total', ('Valor Total'), 'trim|required|xss_clean');

                if (intval($forma_pagamento) == 2) {
                    $this->form_validation->set_rules('parcelas', ('Parcelas'), 'trim|required|xss_clean');
                    $this->form_validation->set_rules('valor_parcelado', ('Valor Parcelado'), 'trim|required|xss_clean');
                    $this->form_validation->set_rules('intervalo_parcelas', ('Intervalo entre Parcelas (dias)'), 'trim|required|xss_clean');
                    $this->form_validation->set_rules('primeira_parcela', ('Vencimento da Primeira Parcela'), 'trim|required|xss_clean');
                }

                $this->form_validation->set_rules('nome_contato', ('Contato da Empresa'), 'trim|required|xss_clean');
                $this->form_validation->set_rules('email_contato', ('E-mail do Contato'), 'trim|required|xss_clean');

                $this->form_validation->set_message('is_unique', ('O valor do %s já está registrado no sistema'));
                $this->form_validation->set_message('required', ('O campo %s é obrigatório'));
                $this->form_validation->set_message('valid_email', ('O %s digitado não é válido'));
                $this->form_validation->set_message('check_default', ('O campo %s é obrigatório'));
                $this->form_validation->set_message('validar_any_email', ('Foi informado algum e-mail inválido'));

                if ($this->form_validation->run() == FALSE) {
                    $errors = $this->form_validation->error_array();
                    echo json_encode(array('erro' => true, 'message' => false, 'list' => json_encode($errors)));
                } else {
                    if ((int) $infosis[0]->CONF_TROCA_EMAIL == 1) {
                        if ($emailpadrao != 'sim') {
                            $emailpadrao = 'nao';
                        }
                    } else {
                        $emailpadrao = 'nao';
                    }

                    if ($urlpadrao != 'sim') {
                        $urlpadrao = 'nao';
                    }

                    $servico = $this->input->post('servico');

                    if ($servico != 'outro') {
                        $serv = $this->contratos_model->get("servicos", $servico);
                        $servico_info = $serv[0]->nome;
                    } else {
                        $servico_info = $this->input->post('outro_servicos');
                    }

                    $data = array(
                        'token' => $token,
                        'CTT_EMAIL_DEFAULT' => $emailpadrao,
                        'CTT_URL_DEFAULT' => $this->input->post('url_padrao'), //$urlpadrao,
                        'CTT_SEND_MAIL' => $this->input->post('email_notificacao'),
                        'CTT_URL_INFO' => $this->input->post('urlcontrato'),
                        'CTT_SERVICO' => $servico_info,
                        'CTT_SERVICO_ID' => $servico,
                        'CTT_DESC' => $this->input->post('descricao'),
                        'MDL_ID' => $this->input->post('modelo_contrato'),
                        'CLI_RAZAO_SOCIAL' => $this->input->post('razao_social'),
                        'CLI_NOME_FANTASIA' => $this->input->post('nome_fantasia'),
                        'CLI_CNPJ' => $this->input->post('cnpj'),
                        'CLI_LOGRADOURO_EMPRESA' => $this->input->post('logradouro'),
                        'CLI_NUMERO_EMPRESA' => $this->input->post('numero'),
                        'CLI_COMPLEMENTO_EMPRESA' => $this->input->post('complemento'),
                        'CLI_BAIRRO_EMPRESA' => $this->input->post('bairro'),
                        'CLI_UF_EMPRESA' => $this->input->post('estado'),
                        'CLI_CIDADE_EMPRESA' => $this->input->post('cidade'),
                        'CLI_CEP_EMPRESA' => $this->input->post('cep'),
                        'CLI_CONTATO_NOME' => $this->input->post('nome_contato'),
                        'CLI_CONTATO_CARGO' => $this->input->post('cargo_contato'),
                        'CLI_RG_CONTATO' => $this->input->post('rg_contato'),
                        'CLI_CPF_CONTATO' => $this->input->post('cpf_contato'),
                        'CLI_EMAIL_CONTATO' => $this->input->post('email_contato'),
                        'CLI_LOGRADOURO_CONTATO' => $this->input->post('logradouro_contato'),
                        'CLI_NUMERO_CONTATO' => $this->input->post('numero_contato'),
                        'CLI_COMPLEMENTO_CONTATO' => $this->input->post('complemento_contato'),
                        'CLI_BAIRRO_CONTATO' => $this->input->post('bairro_contato'),
                        'CLI_UF_CONTATO' => $this->input->post('estado_contato'),
                        'CLI_CIDADE_CONTATO' => $this->input->post('cidade_contato'),
                        'CLI_CEP_CONTATO' => $this->input->post('cep_contato'),
                        'SERV_DESC' => $servico_info,
                        'CTT_TAGS' => $this->input->post('tags'),
                        'CTT_VALOR_TOTAL' => $this->input->post('valor_total'),
                        'CTT_VALOR_PARCELADO' => $this->input->post('valor_parcelado'),
                        'CTT_PARCELAS' => $this->input->post('parcelas'),
                        'CTT_INTERVALO_PARCELAS' => $this->input->post('intervalo_parcelas'),
                        'CTT_DATA_PRIMEIRA_PARCELA' => $this->input->post('primeira_parcela'),
                        'CTT_VENCIMENTOS' => null,
                        'CTT_OP_PAG_SLCT' => $this->input->post('forma_pagamento'),
                        'CTT_CONVENIOS' => $this->input->post('convenios'),
                        'CTT_OUTROS_CNPJS' => $this->input->post('outros_cnpjs'),
                        'CTT_QTDE_VENDAS' => $this->input->post('qtde_vendas'),
                        'CTT_VENDEDOR' => $this->input->post('vendedor'),
                        'CTT_DATA_CONTRATO' => $this->input->post('data_contrato'),
                        'CTT_PERCENTUAL' => 80
                    );

                    if ($id == null) {
                        $retorno = $this->contratos_model->insert($modulo, $data);

                        if (!$retorno) {
                            echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                        } else {
                            echo json_encode(array('erro' => false, 'message' => false, 'list' => array('id' => $retorno)));
                        }
                    } else {
                        $retorno = $this->contratos_model->edit($modulo, $data, $id);

                        if (!$retorno) {
                            echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                        } else {
                            echo json_encode(array('erro' => false, 'message' => false, 'list' => array('id' => $retorno)));
                        }
                    }
                }
            } else {
                $this->logout();
            }
        }
    }

    function get()
    {
        if ($this->session->userdata('user')) {
            $sess_data = $this->session->userdata('user');

            $token = $sess_data['token'];

            if ($this->contratos_model->validaToken($token)) {

                $modulo = $this->input->post('nomemodulo');
                $id = $this->input->post('id');

                if ($id != null) {
                    echo json_encode(array('erro' => false, 'message' => false, 'list' => json_encode($this->contratos_model->get($modulo, $id))));
                } else {
                    echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                }
            } else {
                $this->logout();
            }
        }
    }

    function duplicar()
    {
        if ($this->session->userdata('user')) {
            $sess_data = $this->session->userdata('user');

            $token = $sess_data['token'];

            if ($this->contratos_model->validaToken($token)) {

                $modulo = $this->input->post('nomemodulo');
                $id = $this->input->post('id');

                if ($this->contratos_model->duplicar($modulo, $id, $token)) {
                    echo json_encode(array('erro' => false, 'message' => false, 'list' => null));
                } else {
                    echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                }
            } else {
                $this->logout();
            }
        }
    }

    function delete()
    {
        if ($this->session->userdata('user')) {
            $sess_data = $this->session->userdata('user');

            $token = $sess_data['token'];

            $modulo = $this->input->post('nomemodulo');
            $id = $this->input->post('id');

            if ($this->contratos_model->validaToken($token)) {

                if ($id != null) {
                    if ($this->contratos_model->delete($modulo, $id)) {
                        echo json_encode(array('erro' => false, 'message' => false, 'list' => null));
                    } else {
                        echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                    }
                } else {
                    echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                }
            } else {
                $this->logout();
            }
        }
    }

    function ativarInativar()
    {
        if ($this->session->userdata('user')) {
            $sess_data = $this->session->userdata('user');

            $token = $sess_data['token'];

            $modulo = $this->input->post('nomemodulo');
            $id = $this->input->post('id');

            if ($this->contratos_model->validaToken($token)) {

                if ($id != null) {
                    if ($this->contratos_model->ativarInativar($modulo, $id)) {
                        echo json_encode(array('erro' => false, 'message' => false, 'list' => null));
                    } else {
                        echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                    }
                } else {
                    echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
                }
            } else {
                $this->logout();
            }
        }
    }

    function logout()
    {

        $sess_data = $this->session->userdata('user');

        $token = $sess_data['token'];

        $acesso = array(
            'CONNECTED' => 0
        );

        if ($this->contratos_model->disconnect($token, $acesso)) {
            $this->session->unset_userdata('user');
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
    }

    function check_default($post_string)
    {
        return check_default($post_string);
    }

    function valid_email($email)
    {
        return validar_email($email);
    }

    function validar_any_email($emails)
    {

        $anyemails = explode(',', $emails);

        $count = 0;

        foreach ($anyemails as $email) {
            if (!validar_email($email)) {
                $count++;
            }
        }

        if ($count > 0) {
            return false;
        } else {
            return true;
        }
    }
}

?>