<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{

    protected $profile;
    protected $key;

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('encrypt');
        $this->load->library('email');
        $this->load->helper("funcoes");
        $this->load->model('login_model', 'login_model', TRUE);

        $this->key = "tgB425oF4fdso51l";
    }

    public function index()
    {
        $this->profile = 'admin';

        $data = array(
            'profile' => $this->profile,
            'formtype' => 1,
            'title' => 'Login',
            'config' => $this->login_model->infoSistema('login'),
            'idmodulo' => null
        );

        $this->load->view('login', $data);
    }

    public function logar()
    {

        $this->load->helper("funcoes");

        $this->load->library('form_validation');

        $this->form_validation->set_rules('senha', 'Senha', 'trim|required|xss_clean|callback_check_database');

        $this->form_validation->set_message('required', ('O campo %s é obrigatório'));

        if ($this->form_validation->run() == FALSE) {

            $data = array(
                'profile' => 'admin',
                'formtype' => 1,
                'title' => 'Login',
                'config' => $this->login_model->infoSistema('login'),
                'idmodulo' => null
            );

            $this->load->view('login', $data);
        } else {
            $sess_data = $this->session->userdata('user');

            $usuario = $this->login_model->infoUser($sess_data['token']);
            $config = $this->login_model->infoSistema('login');

            if ($usuario->tipo == $config[0]->CONF_ADM_PERFIL) {
                $pagina = '/admin/modulo/home';
            } else {
                $pagina = '/admin/modulo/propostas';
            }

            redirect($pagina, 'refresh');
        }
    }

    /**
     * REENVIA A SENHA PARA O LOGIN
     */
    public function reenviar()
    {
        $this->load->helper('string');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|xss_clean|callback_valid_email');

        $this->form_validation->set_message('required', ('O campo %s é obrigatório'));

        if ($this->form_validation->run() == FALSE) {
            $data = array(
                'profile' => 'admin',
                'formtype' => 2,
                'title' => 'Reenviar senha',
                'config' => $this->login_model->infoSistema('login'),
                'idmodulo' => null
            );

            $this->load->view('login', $data);
        } else {

            $sistema = $this->login_model->infoSistema('login');

            $email = $this->input->post('email');
            $selector = bin2hex(random_string('alnum', 8));
            $token = random_string('alnum', 32);

            $expires = new DateTime('tomorrow');

            $dados = $this->login_model->resetsenha($email, $selector, $token, $expires);

            $dadosusuario = $this->login_model->getUsuarioByEmail($email);

            if (!empty($dados)) {

                date_default_timezone_set('Brazil/East');
                $this->email->set_newline("\r\n");
                $this->email->set_mailtype("html");

                $this->email->from($sistema[0]->US_SMTP_USER, $sistema[0]->CONF_TITLE_SYS . " - Reset de Senha");
                $this->email->to($email, "Usuário do Sistema");
                $this->email->subject("Reset de Senha");

                $urlToEmail = base_url() . 'resetsenha?' . http_build_query([
                        'selector' => $selector,
                        'validator' => bin2hex($token)
                ]);

                $infodata = array(
                    'url' => $urlToEmail,
                    'nome' => $dados->USER_NAME_FULL,
                    'login' => $dadosusuario->US_LOGIN,
                    'expira' => $expires->format('d/m/Y')
                );

                $body = $this->load->view('template/resetsenha', $infodata, TRUE);

                $this->email->message($body);

                if ($this->email->send()) {

                    $this->profile = 'admin';

                    $data = array(
                        'profile' => $this->profile,
                        'formtype' => 1,
                        'title' => 'Login',
                        'config' => $this->login_model->infoSistema('login'),
                        'page' => 'admin',
                        'enviado' => true,
                        'idmodulo' => null
                    );

                    $this->load->view('login', $data);
                } else {
                    echo $this->email->print_debugger();
                }
            }
        }
    }
    /* public function testeenvio()
      {

      $email = 'guitarpe@hotmail.com';

      date_default_timezone_set('Brazil/East');
      $this->email->set_newline("\r\n");
      $this->email->set_mailtype("html");

      $this->email->from('alex@m2farma.com', "Propostas e contratos automáticos - Reset de Senha");
      $this->email->to($email, "Usuário do Sistema");
      $this->email->subject("Teste E-mail");

      $this->email->message("teste envio de mensagens smtp gmail");

      if ($this->email->send()) {
      echo "enviado";
      } else {
      echo $this->email->print_debugger();
      }
      } */

    /**
     * FAZ A VERIFICAÇÃO DE USUÁRIO E SENHA NO SISTEMA
     * @param unknown_type $password
     * @return boolean
     */
    function check_database($password)
    {
        $username = $this->input->post('login');

        $result = $this->login_model->login($username, $password);

        if ($result['erro'] == false) {
            $sess_array = array();

            foreach ($result['list'] as $row) {
                $sess_array = array(
                    'token' => $row['AC_TOKEN']
                );

                $sub = array('subfolder' => 'user' . $row['US_ID']);

                $this->session->set_userdata('RF', $sub);
                $this->session->set_userdata('user', $sess_array);
            }

            return true;
        } else {
            $this->form_validation->set_message('check_database', $result['message']);
            return false;
        }
    }

    function valid_email($email)
    {
        if (!validar_email($email)) {
            $this->form_validation->set_message('valid_email', ('O %s digitado não é válido'));
            return false;
        } else {
            $result = $this->login_model->verificaEmail($email);

            if ($result) {
                return true;
            } else {
                $this->form_validation->set_message('check_email_database', ("O e-mail informado não existe!"));
                return false;
            }
        }
    }
}

?>