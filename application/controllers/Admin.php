<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('encrypt');
        $this->load->library('email');
        $this->load->model('admin_model', 'admin_model', TRUE);
    }

    public function index()
    {
        if ($this->session->userdata('user')) {
            $sess_data = $this->session->userdata('user');

            $token = $sess_data['token'];

            if ($this->admin_model->validaToken($token)) {

                $slug = 'admin';
                $modulo = $this->uri->segment(3);

                $titles = array(
                    'admin' => 'Painel Administrativo'
                );

                $titulosmodulos = array(
                    'home' => array('Página Inicial', 'fa fa-home'),
                    'usuarios' => array('Gerenciamento de Usuários', 'fa fa-id-card-o'),
                    'perfis' => array('Gerenciamento de Perfis de Usuário', 'fa fa-eye'),
                    'modelos' => array('Gerenciamento de Modelos', 'fa fa-file'),
                    'propostas' => array('Gerenciamento de Propostas', 'fa fa-check'),
                    'contratos' => array('Gerenciamento de Contratos', 'fa fa-handshake-o'),
                    'configuracoes' => array('Configurações gerais', 'fa fa-cogs'),
                    'profile' => array('Informações do Usuário', 'fa fa-user-o')
                );

                $data = array(
                    'title' => $titles[$slug],
                    'menuativo' => array_search($modulo, array_keys($titulosmodulos)),
                    'profile' => $slug,
                    'titulomodulo' => $titulosmodulos[$modulo][0],
                    'icon' => $titulosmodulos[$modulo][1],
                    'config' => $this->admin_model->infoSistema('configuracoes'),
                    'user' => $this->admin_model->infoUser($token),
                    'modulo' => $this->load->view('adm/modulos/' . $modulo, $this->admin_model->getDataModulo($modulo, $token), true)
                );

                $this->load->view('adm/admin', $data);
            } else {
                redirect('login', 'refresh');
            }
        } else {
            redirect('login', 'refresh');
        }
    }

    function gettags()
    {

        $tag = $this->input->post('tag');

        echo $this->admin_model->getTagsCadastradas($tag);
    }

    //LISTA PROPOSTAS HOME
    function getlistaallpropostas()
    {
        $sess_data = $this->session->userdata('user');
        $token = $sess_data['token'];
        $list = $this->admin_model->getpropostashome($token);
        $data = array();

        $no = $_POST['start'];

        foreach ($list as $customers) {
            $row = array();
            $row[] = $customers->PRP_ID;
            $row[] = '<a href="' . base_url() . 'visualizar?tipo=p&id=' . $customers->PRP_ID . '" title="Clique para Visualizar" class="link_view" target="_blank" style="font-weight:bold">' . $customers->PRP_DESC . '</a>';
            $row[] = $customers->PRP_PERCENTUAL;
            $row[] = $customers->STATUS;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->admin_model->todospropostashome($token),
            "recordsFiltered" => $this->admin_model->filtropropostashome($token),
            "data" => $data,
        );

        echo json_encode($output);
    }

    //LISTA CONTRATOS HOME
    function getlistaallcontratos()
    {
        $sess_data = $this->session->userdata('user');
        $token = $sess_data['token'];
        $list = $this->admin_model->getcontratoshome($token);

        $data = array();

        $no = $_POST['start'];

        foreach ($list as $customers) {
            $row = array();
            $row[] = $customers->CTT_ID;
            $row[] = $customers->CTT_DESC;
            $row[] = $customers->CTT_PERCENTUAL;
            $row[] = $customers->STATUS;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->admin_model->todoscontratoshome($token),
            "recordsFiltered" => $this->admin_model->filtrocontratoshome($token),
            "data" => $data,
        );

        echo json_encode($output);
    }

    function logout()
    {
        $sess_data = $this->session->userdata('user');

        $token = $sess_data['token'];

        $acesso = array(
            'CONNECTED' => 0
        );

        if ($this->admin_model->disconnect($token, $acesso)) {
            $this->session->unset_userdata('user');
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
    }

    function finalizaContrato()
    {
        $id = $this->input->post('id');

        if ($this->admin_model->atualizaStatus(2, 'contratos', $id)) {
            echo json_encode(array('erro' => false, 'message' => false, 'list' => array('id' => $id)));
        } else {
            echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
        }
    }

    function finalizaProposta()
    {
        $id = $this->input->post('id');

        if ($this->admin_model->atualizaStatus(2, 'propostas', $id)) {
            echo json_encode(array('erro' => false, 'message' => false, 'list' => array('id' => $id)));
        } else {
            echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
        }
    }

    function enviarEmailCliente()
    {
        $this->load->helper("funcoes");
        $this->load->library('form_validation');

        $sess_data = $this->session->userdata('user');
        $token = $sess_data['token'];

        $usuariologado = $this->admin_model->infoUser($token);
        $configsis = $this->admin_model->config();

        $emailpadrao = $this->input->post('email_customizado');
        $idmodelo = $this->input->post('modelo_email');
        $modulo = $this->input->post('nomemodulo');
        $titulo = $this->input->post('titulo');
        $textomsg = $this->input->post('textomsg');

        $id = $modulo == "propostas" ? $this->input->post('id_proposta') : $id = $this->input->post('id_contrato');
        $usuario = $usuariologado->email == null ? $configsis->US_SMTP_USER : $usuariologado->email;
        $padrao = $emailpadrao == 'sim' ? true : false;
        $layoutmodelo = $emailpadrao == 'sim' ? $this->admin_model->getmodeloLayout($idmodelo) : null;

        if ($emailpadrao != 'sim') {
            $this->form_validation->set_rules('titulo', ('Título'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('textomsg', ('Mensagem'), 'trim|required|xss_clean');
            $this->form_validation->set_message('required', ('O campo %s é obrigatório'));
        } else {

            $this->form_validation->set_rules('modelo_email', ('Modelo do email'), 'trim|required|xss_clean|callback_check_default');
            $this->form_validation->set_message('required', ('O campo %s é obrigatório'));
            $this->form_validation->set_message('check_default', ('O campo %s é obrigatório'));
        }

        $form_validation = $this->form_validation->run();

        if (!$form_validation) {
            $errors = $this->form_validation->error_array();
            echo json_encode(array('erro' => true, 'message' => false, 'list' => json_encode($errors)));
        } else {

            $dados = [
                'USUARIO' => $usuario,
                'MODULO' => $modulo,
                'CONFIG' => $configsis,
                'LOGADO' => $usuariologado,
                'ID' => $id,
                'TITULO' => $titulo,
                'TEXTOMSG' => $textomsg,
                'PADRAO' => $padrao,
                'LAYOUT' => $layoutmodelo
            ];

            $enviointerno = $this->envioEmailInterno($dados);

            if ($enviointerno['erro']) {

                $envioexterno = $this->envioEmailExterno($dados);

                if ($envioexterno['erro']) {
                    echo json_encode(array('erro' => false, 'message' => null, 'list' => null));
                } else {
                    echo json_encode(array('erro' => true, 'message' => 'Erro ao enviar e-mail externo ' . $envioexterno['message'], 'list' => null));
                }
            } else {
                echo json_encode(array('erro' => true, 'message' => 'Erro ao enviar a notificação interna ' . $enviointerno['message'], 'list' => null));
            }
        }
    }

    function emailCopiaContrato($id)
    {
        $this->load->helper("funcoes");

        $doc = $this->admin_model->getContrato($id);

        if ($doc->status == 3) {

            $dados = [
                'ID' => $id,
            ];

            $enviointerno = $this->envioEmailCopiaInterno($dados);

            if ($enviointerno['erro']) {

                $envioexterno = $this->envioEmailCopiaExterno($dados);

                if ($envioexterno['erro']) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        return true;
    }

    function emailContratoAtualizado($id)
    {
        $this->load->helper("funcoes");

        $now = new DateTime();

        $configsis = $this->admin_model->config();
        $doc = $this->admin_model->getContrato($id);

        if ((int) $configsis->CONF_TROCA_EMAIL == 1) {
            $emailnotificacao = $doc->email_padrao == 'sim' ? $configsis->CONF_MAILS_NOTIFY : $doc->email_notificacao;

            $listemails = explode(",", $emailnotificacao);
        } else {
            $listasis = explode(",", $configsis->CONF_MAILS_NOTIFY);
            $listausu = explode(",", $doc->email_notificacao);

            $listemails = array_merge($listausu, $listasis);
        }

        //pegar os dados do usuário que criou a proposta
        $remetente = $this->admin_model->dadosEmailUsuario($id, 2);

        date_default_timezone_set('Brazil/East');
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");
        $this->email->to($listemails);
        $this->email->from($configsis->US_SMTP_USER, "$configsis->CONF_TITLE_SYS - Notificação");
        $this->email->subject("Contrato Editado");

        $data = array(
            'interno' => true,
            'tipodoc' => 'contrato',
            'infotipo' => 'atualizado',
            'datagerado' => $doc->data_gerado,
            'urlproposta' => 'contrato/' . $doc->urlcontrato,
            'contrato' => $doc->descricao,
            'informacao' => null,
            'cliente' => $doc->nome_contato,
            'servico' => $doc->outro_servicos,
            'dataenvio' => $now->format('d/m/Y H:i:s'),
            'url' => 'visualizar?tipo=c&id=' . $id,
            'layout' => $configsis->TXTMAIL_ADD_DATA_CTT,
            'usuario' => ''
        );

        $body = $this->load->view('template/notificacao', $data, TRUE);

        $this->email->message($body);

        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }

        return true;
    }

    function emailAprovacaoProposta($id)
    {
        $this->load->helper("funcoes");

        $dados = [
            'ID' => $id,
            'TIPO' => 'proposta'
        ];

        $enviointerno = $this->envioEmailAprovacaoInterno($dados);

        if ($enviointerno['erro']) {
            return true;
        } else {
            return false;
        }
    }

    function emailAprovacaoContrato($id)
    {
        $this->load->helper("funcoes");

        $dados = [
            'ID' => $id,
            'TIPO' => 'contrato'
        ];

        $enviointerno = $this->envioEmailAprovacaoInterno($dados);

        if ($enviointerno['erro']) {

            $envioexterno = $this->envioEmailAprovacaoExterno($dados);

            if ($envioexterno['erro']) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function getCidades()
    {
        $this->load->helper("funcoes");

        $uf = $this->input->POST('uf');
        $ret = getCidades($uf);

        echo json_encode($ret);
    }

    public function aprovarproposta()
    {
        $modulo = "propostas";

        $id = $this->input->post('id');
        $oppag = $this->input->post('oppag');

        $ip = $_SERVER['REMOTE_ADDR'];

        $retorno = $this->admin_model->aprovar($modulo, $oppag, $id, $ip);

        if ($retorno) {
            //envia o e-mail de aprovação
            $this->emailAprovacaoProposta($id);

            $proposta = $this->admin_model->getPropostaId($id);
            $contrato_id = $this->gerarContratoautomatico($proposta, 'contratos');

            $modelo_id = $proposta[0]->CTT_ID;
            $this->admin_model->gerarLayoutContrato($modelo_id, $contrato_id);

            $contrato = $this->admin_model->getContratoId($contrato_id);

            echo json_encode(array('erro' => false, 'message' => false, 'list' => array('url' => base_url() . 'contrato/' . $contrato[0]->CTT_URL_INFO)));
        } else {
            echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
        }
    }

    public function vercontrato()
    {
        $id = $this->input->post('id');

        $proposta = $this->admin_model->getPropostaId($id);
        $url_contrato = $this->urlContrato($proposta);

        echo json_encode(array('erro' => false, 'message' => false, 'list' => array('url' => base_url() . 'contrato/' . $url_contrato)));
    }

    public function reprovarproposta()
    {
        $modulo = "propostas";
        $id = $this->input->post('id');
        $ip = $_SERVER['REMOTE_ADDR'];

        $retorno = $this->admin_model->reprova($modulo, $id, $ip);

        if ($retorno) {
            echo json_encode(array('erro' => false, 'message' => false, 'list' => array('url' => base_url() . 'obrigado')));
        } else {
            echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
        }
    }

    public function aprovarcontrato()
    {
        $modulo = "contratos";
        $id = $this->input->post('id');
        $ip = $_SERVER['REMOTE_ADDR'];

        $dadoscompletos = $this->admin_model->dadosCompletos($id);

        if ($dadoscompletos) {
            $retorno = $this->admin_model->aprovar($modulo, null, $id, $ip);
            $sistema = $this->admin_model->infoSistema('contratos');

            if ($retorno) {
                $contrato = $this->admin_model->getContratoId($id);

                $this->emailAprovacaoContrato($id);

                echo json_encode(array('erro' => false, 'message' => false, 'list' => array('contrato' => $contrato[0]->CTT_ID, 'url' => $sistema[0]->CONF_REDIRECT_CONTRATOS)));
            } else {
                echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
            }
        } else {
            echo json_encode(array('erro' => true, 'message' => 'Por favor, insira seus dados ao contrato clicando no botão Adicionar Dados e então confirme a contratação.', 'list' => null));
        }
    }

    public function reprovarcontrato()
    {

        $modulo = "contratos";
        $id = $this->input->post('id');
        $ip = $_SERVER['REMOTE_ADDR'];

        $retorno = $this->admin_model->reprova($modulo, $id, $ip);

        if ($retorno) {
            echo json_encode(array('erro' => false, 'message' => false, 'list' => array('url' => base_url() . 'obrigado')));
        } else {
            echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
        }
    }

    public function atualizardados()
    {
        $this->load->helper("funcoes");
        $this->load->library('form_validation');

        $id = $this->input->post('id');

        $data = array(
            'CLI_RAZAO_SOCIAL' => $this->input->post('razao_social'),
            'CLI_NOME_FANTASIA' => $this->input->post('nome_fantasia'),
            'CLI_CNPJ' => $this->input->post('cnpj'),
            'CLI_LOGRADOURO_EMPRESA' => $this->input->post('logradouro'),
            'CLI_NUMERO_EMPRESA' => $this->input->post('numero'),
            'CLI_COMPLEMENTO_EMPRESA' => $this->input->post('complemento'),
            'CLI_BAIRRO_EMPRESA' => $this->input->post('bairro'),
            'CLI_UF_EMPRESA' => $this->input->post('estado'),
            'CLI_CIDADE_EMPRESA' => $this->input->post('cidade'),
            'CLI_CEP_EMPRESA' => $this->input->post('cep'),
            'CLI_CONTATO_NOME' => $this->input->post('nome_contato'),
            'CLI_CONTATO_CARGO' => $this->input->post('cargo_contato'),
            'CLI_RG_CONTATO' => $this->input->post('rg_contato'),
            'CLI_CPF_CONTATO' => $this->input->post('cpf_contato'),
            'CLI_EMAIL_CONTATO' => $this->input->post('email_contato'),
            'CLI_LOGRADOURO_CONTATO' => $this->input->post('logradouro_contato'),
            'CLI_NUMERO_CONTATO' => $this->input->post('numero_contato'),
            'CLI_COMPLEMENTO_CONTATO' => $this->input->post('complemento_contato'),
            'CLI_BAIRRO_CONTATO' => $this->input->post('bairro_contato'),
            'CLI_UF_CONTATO' => $this->input->post('estado_contato'),
            'CLI_CIDADE_CONTATO' => $this->input->post('cidade_contato'),
            'CLI_CEP_CONTATO' => $this->input->post('cep_contato'),
        );

        $retorno = $this->admin_model->editarContrato("contratos", $data, $id);

        if (!$retorno) {
            echo json_encode(array('erro' => true, 'message' => false, 'list' => null));
        } else {
            $this->emailContratoAtualizado($retorno);
            //$this->emailCopiaContrato($retorno);
            echo json_encode(array('erro' => false, 'message' => false, 'list' => array('id' => $retorno)));
        }
    }

    private function urlContrato($proposta)
    {
        $time = DateTime::createFromFormat('d/m/Y H:i:s', $proposta[0]->PRP_DT_APROV);

        $dia = $time->format("Y-m-d H:i:s");
        $data = new DateTime("$dia");
        $ctt_url_info = "contrato_prop_" . $proposta[0]->PRP_ID . $data->format("d_m_Y");

        return $ctt_url_info;
    }
    /* AÇÕES DOS CONTRATOS E PROPOSTAS VISTOS PELOS CLIENTES */

    private function gerarContratoautomatico($proposta, $modulo)
    {
        $now = new DateTime();

        $infosistema = $this->admin_model->infoSistema($modulo);

        if ((int) $infosistema[0]->CONF_TROCA_EMAIL == 1) {
            $emailpadrao = 'sim';
        } else {
            $emailpadrao = 'nao';
        }

        $urlpadrao = 'sim';

        $servico = $proposta[0]->SERV_ID;

        if ($servico != 'outro') {
            $serv = $this->admin_model->get("servicos", $servico);
            $servico_info = $serv[0]->nome;
        } else {
            $servico_info = $this->input->post('outro_servicos');
        }

        $ctt_desc = ("Contrato automático para o cliente: ") . $proposta[0]->CLI_CONTATO_NOME;

        $ctt_url_info = "contrato_prop_" . $proposta[0]->PRP_ID . $now->format("d_m_Y");

        //soma 3 dias após aprovação da proposta
        $datahoje = new DateTime();
        $datahoje->add(new DateInterval('P3D'));

        $data = array(
            'CTT_EMAIL_DEFAULT' => $emailpadrao,
            'CTT_URL_DEFAULT' => $urlpadrao,
            'CTT_SEND_MAIL' => $proposta[0]->PRP_SEND_MAIL,
            'CTT_URL_INFO' => $ctt_url_info,
            'CTT_SERVICO' => $servico_info,
            'CTT_SERVICO_ID' => $servico,
            'CTT_DESC' => $ctt_desc,
            'MDL_ID' => $proposta[0]->CTT_ID,
            'CTT_STATUS' => 2,
            'CTT_SEND_NOTIFICATION' => 1,
            'CLI_RAZAO_SOCIAL' => null,
            'CLI_NOME_FANTASIA' => null,
            'CLI_CNPJ' => null,
            'CLI_LOGRADOURO_EMPRESA' => null,
            'CLI_NUMERO_EMPRESA' => null,
            'CLI_COMPLEMENTO_EMPRESA' => null,
            'CLI_BAIRRO_EMPRESA' => null,
            'CLI_UF_EMPRESA' => null,
            'CLI_CIDADE_EMPRESA' => null,
            'CLI_CEP_EMPRESA' => null,
            'CLI_CONTATO_NOME' => $proposta[0]->CLI_CONTATO_NOME,
            'CLI_CONTATO_CARGO' => null,
            'CLI_RG_CONTATO' => null,
            'CLI_CPF_CONTATO' => null,
            'CLI_EMAIL_CONTATO' => $proposta[0]->CLI_EMAIL_CONTATO,
            'CLI_LOGRADOURO_CONTATO' => null,
            'CLI_NUMERO_CONTATO' => null,
            'CLI_COMPLEMENTO_CONTATO' => null,
            'CLI_BAIRRO_CONTATO' => null,
            'CLI_UF_CONTATO' => null,
            'CLI_CIDADE_CONTATO' => null,
            'CLI_CEP_CONTATO' => null,
            'SERV_DESC' => $servico_info,
            'CTT_TAGS' => null,
            'CTT_VALOR_TOTAL' => $proposta[0]->PRP_VALOR_TOTAL,
            'CTT_VALOR_PARCELADO' => $proposta[0]->PRP_VALOR_PARCELADO,
            'CTT_PARCELAS' => $proposta[0]->PRP_PARCELAS,
            'CTT_OP_PAG_SLCT' => $proposta[0]->PRP_OP_PAG_SLCT,
            'CTT_INTERVALO_PARCELAS' => $proposta[0]->PRP_INTERVALO_PARCELAS,
            'CTT_DATA_PRIMEIRA_PARCELA' => $datahoje->format('d/m/Y'),
            'CTT_VENCIMENTOS' => null,
            'CTT_CONVENIOS' => $proposta[0]->CTT_CONVENIOS,
            'CTT_OUTROS_CNPJS' => null,
            'CTT_QTDE_VENDAS' => null,
            'CTT_VENDEDOR' => $proposta[0]->USER_NAME_FULL,
            'CTT_USER_CREATED' => $proposta[0]->PRP_USER_CREATED,
            'CTT_DATA_CONTRATO' => $now->format("d/m/Y"),
            'CTT_DT_CREATE' => $now->format('Y-m-d H:i:s'),
            'CTT_PERCENTUAL' => 80,
            'CTT_AUTO' => 1
        );

        return $this->admin_model->insereContratoAutomatico($modulo, $data);
    }

    private function envioEmailAprovacaoInterno($dados)
    {
        $configsis = $this->admin_model->config();
        $id = $dados['ID'];
        $tipo = $dados['TIPO'];

        $erros = 0;
        $message = null;

        $doc = $tipo == 'proposta' ? $this->admin_model->getProposta($id) : $this->admin_model->getContrato($id);
        $now = new DateTime();

        if ((int) $configsis->CONF_TROCA_EMAIL == 1) {
            $emailnotificacao = $doc->email_padrao == 'sim' ? $configsis->CONF_MAILS_NOTIFY : $doc->email_notificacao;

            $listemails = explode(",", $emailnotificacao);
        } else {
            $listasis = explode(",", $configsis->CONF_MAILS_NOTIFY);
            $listausu = explode(",", $doc->email_notificacao);

            $listemails = array_merge($listausu, $listasis);
        }

        $titulo = $tipo == 'proposta' ? 'Proposta Aprovada' : 'Contrato Aprovado';

        $op = $tipo == 'proposta' ? 1 : 2;
        //pegar os dados do usuário que criou a proposta
        $remetente = $this->admin_model->dadosEmailUsuario($id, $op);

        date_default_timezone_set('Brazil/East');
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");
        $this->email->to($listemails);
        $this->email->from($configsis->US_SMTP_USER, "$configsis->CONF_TITLE_SYS - Notificação");
        $this->email->subject($titulo);

        $data = array(
            'interno' => true,
            'tipodoc' => $tipo,
            'infotipo' => 'aprovado',
            'datagerado' => $doc->data_gerado,
            'contrato' => $doc->descricao,
            'informacao' => null,
            'cliente' => $doc->nome_contato,
            'servico' => $doc->outro_servicos,
            'dataenvio' => $now->format('d/m/Y H:i:s'),
            'urlproposta' => $tipo == 'contrato' ? 'contrato/' . $doc->urlcontrato : 'proposta/' . $doc->urlproposta,
            'url' => $tipo == 'contrato' ? 'visualizar?tipo=c&id=' . $id : 'visualizar?tipo=p&id=' . $id,
            'layout' => $tipo == 'contrato' ? $configsis->TXTMAIL_CTT_APROV : $configsis->TXTMAIL_PROP_APROV,
            'usuario' => ''
        );

        $body = $this->load->view('template/notificacao', $data, TRUE);

        $this->email->message($body);

        if (!$this->email->send()) {
            $message .= $this->email->print_debugger() . '\n';
            $erros++;
        }

        return $erros > 0 ? ['erro' => false, 'message' => $message] : ['erro' => true, 'message' => $message];
    }

    private function envioEmailAprovacaoExterno($dados)
    {
        $configsis = $this->admin_model->config();
        $id = $dados['ID'];

        $erros = 0;
        $message = null;

        $now = new DateTime();

        $doc = $this->admin_model->getContrato($id);

        //pegar os dados do usuário que criou a proposta
        $remetente = $this->admin_model->dadosEmailUsuario($id, 2);

        date_default_timezone_set('Brazil/East');
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");

        $listemails = explode(',', $doc->email_contato);
        //$this->email->to($doc->email_contato, $doc->nome_contato);
        $this->email->to($listemails, $doc->nome_contato);
        $this->email->from($remetente->email, $remetente->remetente);
        $this->email->subject('Contrato Aprovado');

        $data = array(
            'interno' => true,
            'modelo' => null,
            'padrao' => true,
            'tipodoc' => 'contrato',
            'infotipo' => 'aprovado',
            'datagerado' => $doc->data_gerado,
            'contrato' => $doc->descricao,
            'informacao' => null,
            'cliente' => $doc->nome_contato,
            'servico' => $doc->outro_servicos,
            'dataenvio' => $now->format('d/m/Y H:i:s'),
            'urlproposta' => 'visualizar?tipo=c&id=' . $id,
            'layout' => $configsis->TXTMAIL_COPY_CTT_EXT,
            'usuario' => ''
        );

        $body = $this->load->view('template/notificacao', $data, TRUE);
        $this->email->message($body);

        if (!$this->email->send()) {
            $message .= $this->email->print_debugger() . '\n';
            $erros++;
        }

        return $erros > 0 ? ['erro' => false, 'message' => $message] : ['erro' => true, 'message' => $message];
    }

    private function envioEmailCopiaInterno($dados)
    {
        $id = $dados['ID'];

        $doc = $this->admin_model->getContrato($id);
        $configsis = $this->admin_model->config();

        $erros = 0;
        $message = null;

        $now = new DateTime();

        if ((int) $configsis->CONF_TROCA_EMAIL == 1) {
            $emailnotificacao = $doc->email_padrao == 'sim' ? $configsis->CONF_MAILS_NOTIFY : $doc->email_notificacao;

            $listemails = explode(",", $emailnotificacao);
        } else {
            $listasis = explode(",", $configsis->CONF_MAILS_NOTIFY);
            $listausu = explode(",", $doc->email_notificacao);

            $listemails = array_merge($listausu, $listasis);
        }

        //pegar os dados do usuário que criou a proposta
        $remetente = $this->admin_model->dadosEmailUsuario($id, 2);

        date_default_timezone_set('Brazil/East');
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");

        $this->email->to($listemails);
        $this->email->from($configsis->US_SMTP_USER, "$configsis->CONF_TITLE_SYS - Notificação");
        $this->email->subject("Cópia de Contrato Aprovado");

        $data = array(
            'interno' => true,
            'tipodoc' => 'contrato',
            'infotipo' => 'aprovado',
            'datagerado' => $doc->data_gerado,
            'urlproposta' => 'contrato/' . $doc->urlcontrato,
            'contrato' => $doc->descricao,
            'informacao' => null,
            'cliente' => $doc->nome_contato,
            'servico' => $doc->outro_servicos,
            'dataenvio' => $now->format('d/m/Y H:i:s'),
            'url' => 'visualizar?tipo=c&id=' . $id,
            'layout' => $configsis->TXTMAIL_CTT_APROV,
            'usuario' => ''
        );

        $body = $this->load->view('template/notificacao', $data, TRUE);

        $this->email->message($body);

        if (!$this->email->send()) {
            $message .= $this->email->print_debugger() . '\n';
            $erros++;
        }

        return $erros > 0 ? ['erro' => false, 'message' => $message] : ['erro' => true, 'message' => $message];
    }

    private function envioEmailCopiaExterno($dados)
    {
        $id = $dados['ID'];
        $doc = $this->admin_model->getContrato($id);
        $configsis = $this->admin_model->config();

        $erros = 0;
        $message = null;

        $now = new DateTime();

        //pegar os dados do usuário que criou a proposta
        $remetente = $this->admin_model->dadosEmailUsuario($id, 2);

        date_default_timezone_set('Brazil/East');
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");
        $listemails = explode(',', $doc->email_contato);
        //$this->email->to($doc->email_contato, $doc->nome_contato);
        $this->email->to($listemails, $doc->nome_contato);
        $this->email->from($remetente->email, $remetente->remetente);
        $this->email->subject('Cópia de Contrato Aprovado');

        $data = array(
            'interno' => true,
            'modelo' => null,
            'padrao' => true,
            'tipodoc' => 'contrato',
            'infotipo' => 'aprovado',
            'datagerado' => $doc->data_gerado,
            'contrato' => $doc->descricao,
            'informacao' => null,
            'cliente' => $doc->nome_contato,
            'servico' => $doc->outro_servicos,
            'dataenvio' => $now->format('d/m/Y H:i:s'),
            'urlproposta' => 'visualizar?tipo=c&id=' . $id,
            'layout' => $configsis->TXTMAIL_COPY_CTT_EXT,
            'usuario' => ''
        );

        $body = $this->load->view('template/notificacao', $data, TRUE);
        $this->email->message($body);

        if (!$this->email->send()) {
            $message .= $this->email->print_debugger() . '\n';
            $erros++;
        }

        return $erros > 0 ? ['erro' => false, 'message' => $message] : ['erro' => true, 'message' => $message];
    }

    private function envioEmailInterno($dados)
    {
        $usuario = $dados['USUARIO'];
        $modulo = $dados['MODULO'];
        $configsis = $dados['CONFIG'];
        $usuariologado = $dados['LOGADO'];
        $id = $dados['ID'];

        $erros = 0;
        $message = null;

        $doc = $modulo == 'propostas' ? $this->admin_model->getProposta($id) : $this->admin_model->getContrato($id);
        $now = new DateTime();

        if ((int) $configsis->CONF_TROCA_EMAIL == 1) {
            $emailnotificacao = $doc->email_padrao == 'sim' ? $configsis->CONF_MAILS_NOTIFY : $doc->email_notificacao;

            $listemails = explode(",", $emailnotificacao);
        } else {
            $listasis = explode(",", $configsis->CONF_MAILS_NOTIFY);
            $listausu = explode(",", $doc->email_notificacao);

            $listemails = array_merge($listausu, $listasis);
        }

        $titulo = $modulo == 'propostas' ? 'Nova Proposta' : 'Novo Contrato';

        date_default_timezone_set('Brazil/East');
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");
        $this->email->to($listemails);
        $this->email->from($usuario, "$configsis->CONF_TITLE_SYS - Notificação");
        $this->email->subject($titulo);

        $data = array(
            'interno' => true,
            'tipodoc' => $modulo == 'propostas' ? 'proposta' : 'contrato',
            'infotipo' => 'gerado',
            'datagerado' => $doc->data_gerado,
            'contrato' => $doc->descricao,
            'informacao' => null,
            'cliente' => $doc->nome_contato,
            'servico' => $doc->outro_servicos,
            'dataenvio' => $now->format('d/m/Y H:i:s'),
            'urlproposta' => $modulo == 'propostas' ? 'proposta/' . $doc->urlproposta : 'contrato/' . $doc->urlcontrato,
            'layout' => $modulo == 'propostas' ? $configsis->TXTMAIL_PROP_SEND : $configsis->TXTMAIL_CTT_SEND,
            'usuario' => $usuariologado->nome_completo
        );

        $body = $this->load->view('template/notificacao', $data, TRUE);

        $this->email->message($body);

        if ($this->email->send()) {
            if (!$this->admin_model->sendNotificacaoInterna($modulo, $id)) {
                $erros++;
            }
        } else {
            $message .= $this->email->print_debugger() . '\n';
            $erros++;
        }

        return $erros > 0 ? ['erro' => false, 'message' => $message] : ['erro' => true, 'message' => $message];
    }

    private function envioEmailExterno($dados)
    {
        $modulo = $dados['MODULO'];
        $configsis = $dados['CONFIG'];
        $usuariologado = $dados['LOGADO'];
        $id = $dados['ID'];
        $titulo = $dados['TITULO'];
        $textomsg = $dados['TEXTOMSG'];
        $padrao = $dados['PADRAO'];
        $layoutmodelo = $dados['LAYOUT'];

        $erros = 0;
        $message = null;

        $now = new DateTime();

        $doc = $modulo == 'propostas' ? $this->admin_model->getProposta($id) : $this->admin_model->getContrato($id);

        $remetente = $modulo == 'propostas' ? $this->admin_model->dadosEmailUsuario($id, 1) : $this->admin_model->dadosEmailUsuario($id, 2);

        date_default_timezone_set('Brazil/East');
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");

        if (!$padrao) {
            $titulofrom = $titulo;
        } else {
            $titulofrom = empty($layoutmodelo[0]->MDL_TITULO) ? "$configsis->CONF_TITLE_SYS" : $layoutmodelo[0]->MDL_TITULO;
        }

        $listaemailcontato = explode(",", $doc->email_contato);
        $this->email->to($listaemailcontato, ($doc->nome_contato));
        //$this->email->to($doc->email_contato, ($doc->nome_contato));
        $this->email->from($remetente->email, $remetente->remetente);
        $this->email->subject($titulofrom);

        $cliente = null;

        if ($modulo == 'contratos') {
            if (empty($doc->razao_social)) {
                $cliente = $doc->nome_contato;
            } else {
                $cliente = $doc->razao_social;
            }
        } else {
            $cliente = $doc->nome_contato;
        }

        $dataext = array(
            'interno' => false,
            'padrao' => $padrao,
            'modelo' => $layoutmodelo,
            'infotipo' => 'gerado',
            'datagerado' => $doc->data_gerado,
            'contrato' => $doc->descricao,
            'informacao' => null,
            'cliente' => $cliente,
            'servico' => $doc->outro_servicos,
            'titulo' => $titulo,
            'descricao' => $textomsg,
            'tipodoc' => $modulo == 'propostas' ? 'proposta' : 'contrato',
            'dataenvio' => $now->format('d/m/Y H:i:s'),
            'urlproposta' => $modulo == 'propostas' ? 'proposta/' . $doc->urlproposta : 'contrato/' . $doc->urlcontrato,
            'layout' => $modulo == 'propostas' ? $configsis->TXTMAIL_PROP_SEND_EXT : $configsis->TXTMAIL_CTT_SEND_EXT,
            'usuario' => $usuariologado->nome_completo
        );

        $body = $this->load->view('template/notificacao', $dataext, TRUE);

        $this->email->message($body);

        if ($this->email->send()) {
            if ($this->admin_model->sendEmailcliente($modulo, $id)) {
                $tipo = $modulo == 'propostas' ? 3 : 2;
                $this->admin_model->atualizaStatus($tipo, $modulo, $id);
            } else {
                $erros++;
            }
        } else {
            $message .= $this->email->print_debugger() . '\n';
            $erros++;
        }

        return $erros > 0 ? ['erro' => false, 'message' => $message] : ['erro' => true, 'message' => $message];
    }

    function valorParcela()
    {
        $valor = $this->input->post('valor');
        $taxa = $this->input->post('taxa');
        $parcelas = $this->input->post('parcelas');

        echo $this->admin_model->valorParcela($valor, $taxa, $parcelas);
    }

    function removeracentos()
    {
        $this->load->helper("funcoes");
        $str = $this->input->post('string');

        echo removeracentos($str);
    }

    function decrypt($q)
    {
        if ($q != null) {
            return $this->admin_model->decrypt($q);
        } else {
            return null;
        }
    }

    function check_default($post_string)
    {
        return check_default($post_string);
    }

    function valida_cpf($cpf)
    {
        return valida_cpf($cpf);
    }

    function valida_cnpj($cnpj)
    {
        return valida_cnpj($cnpj);
    }

    function valid_email($email)
    {
        return validar_email($email);
    }
}

?>