<?php
require_once (substr(realpath(dirname(__FILE__)), 0, -6) . 'models/Comum.php');

$CI = & get_instance();

$CI->load->library('session');

$comum = new Comum();

//INFORMAÇÕES DAS CONFIGURAÇÕES
$CI->db->select('*');
$CI->db->from("TB_CONFIGURACOES");
$CI->db->limit(1);
$cfg = $CI->db->get()->row();

//INFORMAÇÕES DO USUÁRIO
$sess_data = $CI->session->userdata('user');
$token = $sess_data['token'];

$CI->db->select('
        us.US_SMTP_HOST as smtphost,
        us.US_SMTP_PORT as smtpporta,
        us.US_SMTP_USER as smtpusuario,
        us.US_SMTP_PASS as smtpsenha');
$CI->db->from("TB_ACESSO_USUARIO acc");
$CI->db->join("TB_USUARIOS us", 'us.US_ID = acc.US_ID');
$CI->db->join("TB_TIPO_USUARIO tp", 'tp.TYPE_ID = us.TYPE_ID');
$CI->db->where('acc.AC_TOKEN', $token);
$CI->db->limit(1);

$user = $CI->db->get()->row();

if (empty($sess_data)) {
    $host = $cfg->US_SMTP_HOST;
    $porta = $cfg->US_SMTP_PORT;
    $usuario = $cfg->US_SMTP_USER;
    $pass = $cfg->US_SMTP_PASS;
    $senha = $comum->decrypt($pass);
} else {
    $host = !empty($user->smtphost) ? $user->smtphost : $cfg->US_SMTP_HOST;
    $porta = !empty($user->smtpporta) ? $user->smtpporta : $cfg->US_SMTP_PORT;
    $usuario = !empty($user->smtpusuario) ? $user->smtpusuario : $cfg->US_SMTP_USER;
    $pass = !empty($user->smtpsenha) ? $user->smtpsenha : $cfg->US_SMTP_PASS;
    $senha = $comum->decrypt($pass);
}

$config['useragent'] = 'CodeIgniter';
$config['protocol'] = 'smtp';
$config['smtp_crypto'] = 'ssl';
$config['smtp_host'] = $host;
$config['smtp_port'] = $porta;
$config['smtp_user'] = $usuario;
$config['smtp_pass'] = $senha;
$config['smtp_timeout'] = '5';
$config['mailtype'] = 'html';
$config['charset'] = 'utf-8';
$config['wordwrap'] = TRUE;
$config['wrapchars'] = 76;
$config['validate'] = FALSE;
$config['priority'] = 3;
$config['bcc_batch_mode'] = FALSE;
$config['bcc_batch_size'] = 200;
$config['crlf'] = "\r\n";
$config['newline'] = "\r\n";

