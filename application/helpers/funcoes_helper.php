<?php
if (!function_exists('curl_file_create')) {

    function curl_file_create($filename, $mimetype = '', $postname = '')
    {
        //return "@$filename;filename=".($postname ?: basename($filename)).($mimetype ? ";type=$mimetype" : '');
    }
}

/**
 * EXIBE O ANO ATUAL
 */
if (!function_exists('anoAtual')) {

    function anoAtual()
    {
        return date('Y');
    }
}

if (!function_exists('delTree')) {

    function delTree($dir)
    {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }
}

if (!function_exists('getCidades')) {

    function getCidades($uf)
    {

        $cidades = array();

        $data = json_decode(file_get_contents(base_url() . "assets/data/estados-cidades.json"), TRUE);

        foreach ($data as $obj) {
            foreach ($data['estados'] as $estados) {
                if ($estados['sigla'] == $uf) {
                    foreach ($estados['cidades'] as $cidade) {
                        array_push($cidades, array('value' => $cidade));
                    }
                }
            }
        }

        return $cidades;
    }
}

if (!function_exists('calculo_parcelas')) {

    function calculo_parcelas($valor, $parcelas, $juros)
    {

        $juros = bcdiv($juros, 100, 15);
        $e = 1.0;
        $cont = 1.0;

        for ($i = 1; $i <= $parcelas; $i++) {
            $cont = bcmul($cont, bcadd($juros, 1, 15), 15);
            $e = bcadd($e, $cont, 15);
        }

        $e = bcsub($e, $cont, 15);

        $valor = bcmul($valor, $cont, 15);

        return bcdiv($valor, $e, 15);
    }
}

/**
 * EXIBE A DATA ATUAL POR EXTENSO
 */
if (!function_exists('dataInfoExtenso')) {

    function dataInfoExtenso($data)
    {

        if (strlen($data) == 7) {
            if (preg_match("/([0-9]{2})\/([0-9]{4})/", $data, $matches)) {
                $data = "01/" . $data;

                $time = strtotime(str_replace('/', '-', $data));
                $newformat = gmdate('Y-m-d', $time);

                setlocale(LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese');
                date_default_timezone_set('America/Sao_Paulo');

                return strftime('%B de %Y', strtotime($newformat));
            }
        }

        if (strlen($data) == 10) {
            if (preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $data, $matches)) {

                $time = strtotime(str_replace('/', '-', $data));
                $newformat = gmdate('Y-m-d', $time);

                setlocale(LC_TIME, 'portuguese');
                date_default_timezone_set('America/Sao_Paulo');

                return strftime('%A, %d de %B de %Y', strtotime($newformat));
            }
        }
    }
}

if (!function_exists('dataSomenteAno')) {

    function dataSomenteAno($data)
    {
        if (strlen($data) == 7) {
            if (preg_match("/([0-9]{2})\/([0-9]{4})/", $data, $matches)) {
                $data = "01/" . $data;

                $time = strtotime(str_replace('/', '-', $data));
                $newformat = gmdate('Y', $time);

                return $newformat;
            }
        }

        if (strlen($data) == 10) {
            if (preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $data, $matches)) {

                $time = strtotime(str_replace('/', '-', $data));
                $newformat = gmdate('Y', $time);

                return $newformat;
            }
        }
    }
}

/**
 * VERIFICA SE FOI SELECIONADO ZERO NOS COMBOS
 */
if (!function_exists('check_default')) {

    function check_default($post_string)
    {
        return $post_string == '0' ? FALSE : TRUE;
    }
}

/**
 * REMOVE ACENTUA��O NA STRING
 */
if (!function_exists('removeracentos')) {

    function removeracentos($str)
    {

        $str = strtolower($str);

        $str = preg_replace('/[áàãâä]/ui', 'a', $str);
        $str = preg_replace('/[éèêë]/ui', 'e', $str);
        $str = preg_replace('/[íìîï]/ui', 'i', $str);
        $str = preg_replace('/[óòõôö]/ui', 'o', $str);
        $str = preg_replace('/[úùûü]/ui', 'u', $str);
        $str = preg_replace('/[ç]/ui', 'c', $str);
        $str = preg_replace('/[^a-z0-9]/i', '_', $str);
        $str = preg_replace('/_+/', '_', $str); // ideia do Bacco :)
        return $str;
    }
}

/**
 * 	REMOVE ACENTOS E ESPA�OS
 */
if (!function_exists('removeracentosespacos')) {

    function removeracentosespacos($string)
    {

        $string = preg_replace("/[áàâãä]/", "a", $string);
        $string = preg_replace("/[ÁÀÂÃÄ]/", "A", $string);
        $string = preg_replace("/[éèê]/", "e", $string);
        $string = preg_replace("/[ÉÈÊ]/", "E", $string);
        $string = preg_replace("/[íì]/", "i", $string);
        $string = preg_replace("/[ÍÌ]/", "I", $string);
        $string = preg_replace("/[óòôõö]/", "o", $string);
        $string = preg_replace("/[ÓÒÔÕÖ]/", "O", $string);
        $string = preg_replace("/[úùü]/", "u", $string);
        $string = preg_replace("/[ÚÙÜ]/", "U", $string);
        $string = preg_replace("/ç/", "c", $string);
        $string = preg_replace("/Ç/", "C", $string);
        $string = preg_replace("/[][><}{)(:;,!?*%~^`&#@]/", "", $string);
        $string = preg_replace("/ /", "_", $string);

        return $string;
    }
}

/**
 * REMOVE OS ACENTOS E CAIXA ALTA
 */
if (!function_exists('removeraespacoslower')) {

    function removeraespacoslower($string)
    {
        $string = preg_replace('/\s/', '', $string);

        return strtolower($string);
    }
}

/**
 * VALIDA O EMAIL INFORMADO
 */
if (!function_exists('validar_email')) {

    function validar_email($email)
    {
        $er = "/^(([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}){0,1}$/";
        if (preg_match($er, $email)) {
            return true;
        } else {
            return false;
        }
    }
}

/**
 * VALIDA A DATA INFORMADA
 */
if (!function_exists('valida_data')) {

    function valida_data($dat)
    {
        if ($dat != null) {
            if (strlen($dat) == 7) {
                if (preg_match("/([0-9]{2})\/([0-9]{4})/", $dat, $matches)) {

                    $dat = "01/" . $dat;

                    $data = explode("/", "$dat");
                    $d = $data[0];
                    $m = $data[1];
                    $y = $data[2];

                    $res = checkdate($m, $d, $y);
                    if ($res == 1) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } elseif (strlen($dat) == 10) {
                if (preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $dat, $matches)) {
                    $data = explode("/", "$dat");
                    $d = $data[0];
                    $m = $data[1];
                    $y = $data[2];

                    $res = checkdate($m, $d, $y);
                    if ($res == 1) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

/**
 * VALIDA O CPF INFORMADO
 */
if (!function_exists('valida_cpf')) {

    function valida_cpf($post_string)
    {
        // Verifiva se o n�mero digitado cont�m todos os digitos
        $post_string = str_pad(preg_replace('/[^0-9]/', '', $post_string), 11, '0', STR_PAD_LEFT);

        // Verifica se nenhuma das sequ�ncias abaixo foi digitada, caso seja, retorna falso
        if (strlen($post_string) != 11 || $post_string == '00000000000' || $post_string == '11111111111' || $post_string == '22222222222' || $post_string == '33333333333' || $post_string == '44444444444' || $post_string == '55555555555' || $post_string == '66666666666' || $post_string == '77777777777' || $post_string == '88888888888' || $post_string == '99999999999') {
            return FALSE;
        } else {
            // Calcula os n�meros para verificar se o CPF � verdadeiro
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $post_string{$c} * (($t + 1) - $c);
                }

                $d = ((10 * $d) % 11) % 10;

                if ($post_string{$c} != $d) {
                    return FALSE;
                }
            }
            return TRUE;
        }
    }
}

/**
 * VALIDA O CNPJ INFORMADO
 */
if (!function_exists('valida_cnpj')) {

    function valida_cnpj($cnpj)
    {

        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);

        // Valida tamanho
        if (strlen($cnpj) != 14)
            return false;

        // Valida primeiro d�gito verificador
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
            return false;

        // Valida segundo d�gito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }
}

if (!function_exists('jsonCidadesEstados')) {
    $lista = "";
}