<h4><?php echo ("Relação de Contratos Cadastrados") ?></h4>
<div>
    <div class="form-group" style="height: 50px">
        <?php if (in_array(16, $permissoes)) { ?>
            <div class="col-sm-12" style="text-align: left; padding-bottom: 10px;">
                <button type="button" class="btn btn-success btcad" id="btn_cadastrar" data-toggle="modal" data-target="#cadastro-edicao-contratos"><?php echo ("Cadastrar Novo Contrato") ?></button>
            </div>
        <?php } ?>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title"><?php echo ("Filtro") ?><a data-toggle="collapse" href="#collapse1" style="margin-left: 20px"><i class="entypo-down-open"></i></a></h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="form-group busca">
                        <div class="col-sm-9">
                            <label for="busca_titulo" class="control-label"><?php echo ("Título: ") ?></label>
                            <input type="text" class="form-control" id="busca_titulo" name="busca_titulo" placeholder="<?php echo ("Buscar Título") ?>">
                        </div>
                        <div class="col-sm-3">
                            <label for="busca_categoria" class="control-label"><?php echo ("Categoria: ") ?></label>
                            <input type="text" class="form-control" id="busca_categoria" name="busca_categoria" placeholder="<?php echo ("Buscar Categoria") ?>">
                        </div>
                    </div>
                    <div class="col-sm-12 linha-busca">
                        <div class="col-sm-8">
                            <label for="busca_razao" class="control-label"><?php echo ("Razão Social: ") ?></label>
                            <input type="text" class="form-control" id="busca_razao" name="busca_razao" placeholder="<?php echo ("Buscar Cliente") ?>">
                        </div>
                        <div class="col-sm-2">
                            <label for="busca_data" class="control-label"><?php echo ("Período De: ") ?></label>
                            <input type="date" class="form-control" id="busca_data" name="busca_data" placeholder="<?php echo ("Buscar Inicial") ?>">
                        </div>
                        <div class="col-sm-2">
                            <label for="busca_data_ate" class="control-label"><?php echo ("Até: ") ?></label>
                            <input type="date" class="form-control" id="busca_data_ate" name="busca_data_ate" placeholder="<?php echo ("Buscar Final") ?>">
                        </div>
                    </div>
                    <div class="col-sm-12 linha-busca">
                        <div class="col-sm-2">
                            <label for="busca_status" class="control-label"><?php echo ("Status: ") ?></label>
                            <input type="text" class="form-control" id="busca_status" name="busca_status" placeholder="<?php echo ("Buscar Status") ?>">
                        </div>
                        <div class="col-sm-2">
                            <label for="busca_enviado" class="control-label"><?php echo ("Visualizado: ") ?></label>
                            <input type="text" class="form-control" id="busca_enviado" name="busca_enviado" placeholder="<?php echo ("Buscar Situação") ?>">
                        </div>
                        <div class="col-sm-8">
                            <label for="busca_tags" class="control-label"><?php echo ("Tags: ") ?></label>
                            <input type="text" class="form-control" id="busca_tags" name="busca_tags" placeholder="<?php echo ("Tags") ?>">
                        </div>
                    </div>
                    <div class="col-sm-12 linha-busca">
                        <div class="col-sm-9">
                            <label for="busca_vendedor" class="control-label"><?php echo ("Vendedor: ") ?></label>
                            <input type="text" class="form-control" id="busca_vendedor" name="busca_vendedor" placeholder="<?php echo ("Vendedor") ?>">
                        </div>
                        <div class="col-sm-3 FieldPadTop28">
                            <input type="button" class="btn btn-block btn-success" value="Buscar" id="btnbuscar" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <table class="table table-bordered table-striped datatable" id="table-contratos">
                <thead>
                    <tr>
                        <th width="10"><?php echo ("Id") ?></th>
                        <th width="200"><?php echo ("Título") ?></th>
                        <th width="90"><?php echo ("Razão Social") ?></th>
                        <th width="60"><?php echo ("Vendedor") ?></th>
                        <th width="60"><?php echo ("Categoria") ?></th>
                        <th width="20"><?php echo ("Data") ?></th>
                        <th width="20"><?php echo ("Status") ?></th>
                        <th width="10"><?php echo ("Visto") ?></th>
                        <th width="250"><?php echo ("Ações") ?></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php if (in_array(16, $permissoes)) { ?>
    <div class="modal fade" id="cadastro-edicao-contratos" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                    <h4 class="modal-title"><strong><?php echo ("Cadastro e Edição de Contratos") ?></strong></h4>
                </div>
                <div class="modal-body">
                    <?php
                    echo form_open_multipart("contratos/insertedit", array(
                        "id" => "form_cad_contrato",
                        "class" => "form-horizontal form-groups-bordered panel-wizard",
                        "data-toggle" => "validator"
                    ));

                    ?>
                    <ul class="nav nav-justified nav-wizard">
                        <li class="active"><a href="#tab1-4" data-toggle="tab"><?php echo (" Informações Básicas") ?></a></li>
                        <li><a href="#tab2-4" data-toggle="tab"><?php echo (" Dados do Cliente") ?></a></li>
                        <li><a href="#tab3-4" data-toggle="tab"><?php echo (" Tags e Modelo do Contrato") ?></a></li>
                        <li><a href="#tab4-4" data-toggle="tab"><?php echo (" Visualizar Contrato e Finalizar") ?></a></li>
                    </ul>
                    <ul class="list-unstyled wizard">
                        <li class="pull-right finish hide">
                            <button type="submit" class="btn btn-info"><?php echo ("Gerar") ?></button>
                            <button type="button" class="btn btn-success" id="finalizar_enviar"><?php echo ("Finalizar e Enviar") ?></button>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1-4">
                            <input type="hidden" id="nomemodulo" name="nomemodulo" value="<?php echo $idmodulo; ?>"/>
                            <input type="hidden" id="id" name="id"/>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Título do Contrato") ?></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="descricao" name="descricao" placeholder="<?php echo ("Descrição do Contrato") ?>" maxlength="120">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Modelo do Contrato") ?></label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="modelo_contrato" name="modelo_contrato">
                                        <option value="">Selecione</option>
                                        <?php foreach ($listamodeloscontratos as $modelo) { ?>
                                            <option value="<?php echo $modelo->MDL_ID ?>"><?php echo $modelo->MDL_DESC ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Selecione a Categoria") ?></label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="servico" name="servico">
                                        <option value="">Selecione</option>
                                        <option value="outro">Outro</option>
                                        <?php foreach ($listaservicos as $serv) { ?>
                                            <option value="<?php echo $serv->SERV_ID ?>"><?php echo $serv->SERV_DESC ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-8 col-sm-offset-3" id="outro_serv">
                                    <input type="text" class="form-control" id="outro_servicos" name="outro_servicos" placeholder="<?php echo ("Digite a Categoria") ?>" maxlength="120">
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2-4">
                            <div class="form-group">
                                <label for="field-1" class="col-sm-2 control-label"><?php echo ("Razão Social") ?></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="razao_social" name="razao_social" placeholder="<?php echo ("Razão Social") ?>" maxlength="200">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-2 control-label"><?php echo ("Nome Fantasia") ?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="nome_fantasia" name="nome_fantasia" placeholder="Nome Fantasia" maxlength="200">
                                </div>
                                <label for="field-1" class="col-sm-1 control-label"><?php echo ("CNPJ") ?></label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="cnpj" name="cnpj" placeholder="CNPJ" maxlength="25" data-mask="cnpj">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3 control-label"><h5><strong><?php echo ("Endereço Comercial") ?></strong></h5></div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-2 control-label"><?php echo ("Logradouro") ?></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="logradouro" name="logradouro" placeholder="Logradouro" maxlength="200"/>
                                </div>
                                <label for="field-1" class="col-sm-1 control-label"><?php echo ("Número") ?></label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="numero" name="numero" data-mask="decimal" placeholder="<?php echo ("Número") ?>" maxlength="10" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-2 control-label"><?php echo ("Complemento") ?></label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="complemento" name="complemento" placeholder="Complemento" maxlength="200"/>
                                </div>
                                <label for="field-1" class="col-sm-1 control-label"><?php echo ("Bairro") ?></label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Bairro" maxlength="100"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-2 control-label"><?php echo ("Estado") ?></label>
                                <div class="col-sm-2">
                                    <select class="form-control estado" id="estado" name="estado">
                                        <option value="0">UF</option>
                                        <?php
                                        foreach ($listaestados as $estado) {

                                            ?>
                                            <option value="<?php echo $estado->id ?>"><?php echo $estado->sigla ?></option>
                                            <?php
                                        }

                                        ?>
                                    </select>
                                </div>
                                <label for="field-1" class="col-sm-1 control-label"><?php echo ("Cidade") ?></label>
                                <div class="col-sm-3">
                                    <input list="cidades_list" class="form-control" id="cidade" name="cidade" placeholder="Cidade" maxlength="100"/>
                                    <datalist id="cidades_list">
                                    </datalist>
                                </div>
                                <label for="field-1" class="col-sm-1 control-label"><?php echo ("CEP") ?></label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="cep" name="cep" data-mask="cep" placeholder="CEP" maxlength="9"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3 control-label"><h5><strong><?php echo ("Informações do Contato") ?></strong></h5></div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-2 control-label"><?php echo ("Responsável") ?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="nome_contato" name="nome_contato" placeholder="Responsável da Empresa" maxlength="200"/>
                                </div>
                                <label for="field-1" class="col-sm-1 control-label"><?php echo ("RG") ?></label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="rg_contato" name="rg_contato" placeholder="RG" data-mask="rg"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-2 control-label"><?php echo ("CPF") ?></label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="cpf_contato" name="cpf_contato" placeholder="CPF" data-mask="cpf"/>
                                </div>
                                <label for="field-1" class="col-sm-1 control-label"><?php echo ("Cargo") ?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="cargo_contato" name="cargo_contato" placeholder="Cargo" maxlength="200"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo ("E-mail") ?></label>
                                <div class="col-sm-9">
                                    <div class="input-group minimal">
                                        <span class="input-group-addon"><i class="entypo-mail"></i></span>
                                        <input type="text" id="email_contato" name="email_contato" class="form-control" data-mask="email" placeholder="E-mail" maxlength="200">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3 control-label"><h5><strong><?php echo ("Endereço do Responsável") ?></strong></h5></div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-2 control-label"><?php echo ("Logradouro") ?></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="logradouro_contato" name="logradouro_contato" placeholder="Logradouro" maxlength="200"/>
                                </div>
                                <label for="field-1" class="col-sm-1 control-label"><?php echo ("Número") ?></label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="numero_contato" name="numero_contato" data-mask="decimal" placeholder="<?php echo ("Número") ?>" maxlength="10" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-2 control-label"><?php echo ("Complemento") ?></label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="complemento_contato" name="complemento_contato" placeholder="Complemento" maxlength="200"/>
                                </div>
                                <label for="field-1" class="col-sm-1 control-label"><?php echo ("Bairro") ?></label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="bairro_contato" name="bairro_contato" placeholder="Bairro" maxlength="100"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-2 control-label"><?php echo ("Estado") ?></label>
                                <div class="col-sm-2">
                                    <select class="form-control estado" id="estado_contato" name="estado_contato">
                                        <option value="0">UF</option>
                                        <?php
                                        foreach ($listaestados as $estado) {

                                            ?>
                                            <option value="<?php echo $estado->id ?>"><?php echo $estado->sigla ?></option>
                                            <?php
                                        }

                                        ?>
                                    </select>
                                </div>
                                <label for="field-1" class="col-sm-1 control-label"><?php echo ("Cidade") ?></label>
                                <div class="col-sm-3">
                                    <input list="cidades_list" class="form-control" id="cidade_contato" name="cidade_contato" placeholder="Cidade" maxlength="100"/>
                                    <datalist id="cidades_list">
                                    </datalist>
                                </div>
                                <label for="field-1" class="col-sm-1 control-label"><?php echo ("CEP") ?></label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="cep_contato" name="cep_contato" data-mask="cep" placeholder="CEP" maxlength="9"/>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab3-4">
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo ("E-mail interno para notificação") ?></label>
                                <div class="col-sm-8">
                                    <input type="text" id="email_notificacao" name="email_notificacao" class="form-control" placeholder="E-mail" value="<?php echo $user->email; ?>"/>
                                </div>
                                <?php if ((int) $config[0]->CONF_TROCA_EMAIL == 1) { ?>
                                    <div class="col-sm-4">
                                        <label><input type="checkbox" name="email_padrao" id="email_padrao" value="sim">
                                            <?php echo ("Enviar para e-mail padrão do sistema") ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Tags") ?></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tags" name="tags">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="login" class="col-sm-3 control-label"><?php echo ("URL do contrato") ?></label>
                                <div class="col-sm-8">
                                    <?php echo base_url() . 'contrato/'; ?><input type="text" class="form-control" id="urlcontrato" name="urlcontrato" placeholder="URL do contrato" maxlength="150">
                                </div>
                                <div class="col-sm-3 col-sm-offset-3" id="ckb_url">
                                    <label class="checkbox-inline"><input type="checkbox" id="url_padrao" name="url_padrao" value="sim"><?php echo ("Gerar url padrão do sistema") ?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Forma de Pagamento") ?></label>
                                <div class="col-sm-3">
                                    <select class="form-control" id="forma_pagamento" name="forma_pagamento">
                                        <option value="">Selecione</option>
                                        <option value="1">Pagamento à vista</option>
                                        <option value="2">Pagamento parcelado</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Valor Total") ?></label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="valor_total" name="valor_total" placeholder="Valor Total">
                                </div>
                                <label for="field-1" class="col-sm-2 control-label"><?php echo ("Valor Parcelado") ?></label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="valor_parcelado" name="valor_parcelado" placeholder="<?php echo ("Valor Parcelado") ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Qtde. de Parcelas") ?></label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control numbers" id="parcelas" name="parcelas" placeholder="Qtde. de Parcelas">
                                </div>
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Intervalo entre Parcelas (dias)") ?></label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control numbers" id="intervalo_parcelas" name="intervalo_parcelas" placeholder="Intervalo" maxlength="2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Vencimento da Primeira Parcela") ?></label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="primeira_parcela" name="primeira_parcela" data-mask="date" placeholder="Vencimento">
                                </div>
                                <label for="field-1" class="col-sm-2 control-label"><?php echo ("Data do Contrato") ?></label>
                                <div class="col-sm-3">
                                    <?php $hoje = new DateTime(); ?>
                                    <input type="text" class="form-control" id="data_contrato" name="data_contrato" data-mask="date" placeholder="<?php echo ("Data do Contrato") ?>" value="<?php echo $hoje->format('d/m/Y'); ?>" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Vendedor") ?></label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="vendedor" name="vendedor" placeholder="<?php echo ("Vendedor") ?>"/>
                                </div>
                                <label for="field-1" class="col-sm-2 control-label"><?php echo ("Qtde. Vendas") ?></label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control numbers" id="qtde_vendas" name="qtde_vendas" placeholder="<?php echo ("Qtde. Vendas") ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Convênios") ?></label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="convenios" name="convenios" placeholder="<?php echo ("Convênios") ?>" style="height: 150px;overflow-x: hidden;"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Outros CNPJ's") ?></label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="outros_cnpjs" name="outros_cnpjs" placeholder="<?php echo ("Outros CNPJ's") ?>" style="height: 150px;overflow-x: hidden;"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab4-4">
                            <input type="hidden" id="idcontrato"/>
                            <div id="loadpdf" class="loadpdf"></div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <div class="modal-body" id="form_envio_email">
                    <?php
                    echo form_open_multipart("admin/enviarEmailCliente", array(
                        "id" => "form_cad_send",
                        "class" => "form-horizontal validate form-groups-bordered",
                        "data-toggle" => "validator"
                    ));

                    ?>
                    <?php echo form_hidden('nomemodulo', $idmodulo) ?>
                    <input type="hidden" id="id_contrato" name="id_contrato"/>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div style="float: left;"><h4><?php echo ("Enviar Contrato ao Cliente") ?></h4></div><div class="divinfo"><a href="#" class="left" id="info"><i class="fa fa-info-circle" aria-hidden="true"></i></a></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-1">
                            <label class="checkbox-inline"><input type="checkbox" name="email_customizado" id="email_customizado" value="sim"><?php echo ("Enviar e-mail modelo"); ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-1 control-label"><?php echo ("Modelo") ?></label>
                        <div class="col-sm-6">
                            <select class="form-control" id="modelo_email" name="modelo_email">
                                <option value="">Selecione</option>
                                <?php foreach ($listamodelosemail as $modelo) { ?>
                                    <option value="<?php echo $modelo->MDL_ID ?>"><?php echo $modelo->MDL_DESC ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="login" class="col-sm-1 control-label"><?php echo ("Título") ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="titulo" name="titulo" placeholder="<?php echo ("Título") ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="login" class="col-sm-1 control-label"><?php echo ("Mensagem") ?></label>
                        <div class="col-sm-10">
                            <textarea class="form-control editor" id="textomsg" name="textomsg" maxlength="500" style="height: 300px"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5">
                            <button type="submit" class="btn btn-success"><?php echo ("Enviar") ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<script>
    jQuery(document).ready(function ($) {
        $("#info").on('click', function (e) {
            e.preventDefault();
            $.notify.addStyle('foo', {
                html:
                        "<div>" +
                        "<div class='clearfix'>" +
                        "<div class='title' data-notify-html='title'/>" +
                        "<div class='buttons'>" +
                        "<a href='#' class='btn btn-info btn-xs no'>Fechar</a>" +
                        "</div>" +
                        "<div class='info'>" +
                        "<div>URL do contrato/proposta<span>[valor-url]</span></div>" +
                        "<div>Tipo (contrato ou proposta)<span>[valor-tipo]</span></div>" +
                        "<div><?php echo ("Data de geração do documento") ?><span>[valor-data]</span></div>" +
                        "<div><?php echo ("Descrição do documento") ?><span>[valor-titulo]</span></div>" +
                        "<div>Cliente<span>[valor-cliente]</span></div>" +
                        "<div>Nome do Usuário do Sistema<span>[usuario]</span></div>" +
                        "<div><?php echo ("Descrição do servico") ?><span>[valor-servico]</span></div>" +
                        "<div><?php echo ("Informações básicas do documento") ?><span>[valor-informacao]</span></div>" +
                        "<div>Data de envio do e-mail<span>[data-envio]</span></div><br/>" +
                        "<div><strong><?php echo ("Para adicionar a url do documento não é necessário utilizar a opção link do editor, basta informar a tag. Caso queira adicionar um link diretamente para o documento utilize a opção link do editor infromando a tag de url ([valor-url]) no campo 'Url'.") ?></strong></div>" +
                        "</div>" +
                        "</div>" +
                        "</div>"
            });
            $(this).notify({
                title: '<?php echo ("Utilização das tags no texto:") ?>',
                button: 'Confirm'
            }, {
                style: 'foo',
                elementPosition: 'right top',
                autoHide: false,
                clickToHide: false
            });
        });
        $(document).on('click', '.notifyjs-foo-base .no', function (e) {
            e.preventDefault();
            $(this).trigger('notify-hide');
        });
        var table = $('#table-contratos').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [
                [0, "desc"]
            ],
            "dom": '<"top"i>rt<"bottom"lp><"clear">',
            "ajax": {
                "url": "<?php echo base_url() ?>listacontratos",
                "type": "POST",
                "data": {
                    "modulo": '<?php echo $idmodulo; ?>'
                }
            },
            columns: [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                {orderable: false},
                {orderable: false, visible: false},
                {orderable: false, visible: false}
            ],
            "columnDefs": [
                {
                    "targets": [8],
                    "orderable": false,
                    "render": function (data, type, row) {

                        var btnstatus = null;
                        if (row[6] != 'Inativo') {
                            btnstatus = '<a href="#" class="btn btn-danger btn-xs btn_ativainativa" title="Inativar"><i class="fa fa-ban"></i> <?php echo ("Inativar") ?></a>'
                        } else {
                            btnstatus = '<a href="#" class="btn btn-primary btn-xs btn_ativainativa" title="Ativar"><i class="fa fa-ban"></i> <?php echo ("Ativar") ?></a>'
                        }

                        if (row[6] != 'Aprovado') {
                            return '<a href="#" class="btn btn-info btn-xs btn_duplicar" title="Duplicar"><i class="fa fa fa-copy"></i> <?php echo ("Duplicar") ?></a> <?php if (in_array(17, $permissoes)) { ?><a href="#" class="btn btn-warning btn-xs btn_editar" title="Editar"><i class="fa fa-pencil-square-o"></i><?php } ?> <?php echo ("Editar") ?></a> <?php if (in_array(18, $permissoes)) { ?><a href="#" class="btn btn-danger btn-xs btn_excluir" title="Excluir"><i class="fa fa-trash-o"></i><?php echo ("Excluir") ?></a><?php } ?> ' + btnstatus
                        } else {
                            return '<a href="#" class="btn btn-info btn-xs btn_duplicar" title="Duplicar"><i class="fa fa fa-copy"></i> <?php echo ("Duplicar") ?></a> <a href="#" class="btn btn-success btn-xs" title="Aprovado"><i class="fa fa-check"></i> <?php echo ("Aprovado") ?></a> ' + btnstatus;
                        }
                    }
                },
            ],
            select: true,
            'fnRowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", aData[0]);
                return nRow;
            },
            "stripeClasses": ['', '']
        });
        $('#btnbuscar').click(function () {

            table.column(1).search($('#busca_titulo').val().trim());
            table.column(2).search($('#busca_razao').val().trim());
            table.column(3).search($('#busca_vendedor').val().trim());
            table.column(4).search($('#busca_categoria').val().trim());
            table.column(5).search($('#busca_data').val().trim());
            table.column(6).search($('#busca_enviado').val().trim());
            table.column(7).search($('#busca_status').val().trim());
            table.column(9).search($('#busca_tags').val().trim());
            table.column(10).search($('#busca_data_ate').val().trim());
            table.draw();
        });
        $("#valor_total").maskMoney({prefix: 'R$ ', allowNegative: true, thousands: '.', decimal: ',', affixesStay: false});
        $("#valor_parcelado").maskMoney({prefix: 'R$ ', allowNegative: true, thousands: '.', decimal: ',', affixesStay: false});

        $('#email_contato').tagsInput({
            width: 'auto',
            defaultText: 'Adicionar'
        });

<?php if (in_array(16, $permissoes)) { ?>
            var tagsInput = $('#tags').tagsInput({
                width: 'auto',
                defaultText: 'Adicionar',
                onAddTag: function (tag) {

                    $.ajax({
                        url: '<?php echo base_url() ?>tags',
                        type: "POST",
                        data: {
                            tag: tag
                        },
                        success: function (data) {
                            if (data == false) {
                                tagsInput.removeTag(escape(tag));
                            }
                        },
                        error: function () {}
                    });
                }
            });
            $('#email_notificacao').tagsInput({
                width: 'auto',
                defaultText: 'Adicionar'
            });
            $("#btn_cadastrar").on("click", function (e) {
                e.preventDefault();
                $('#form_cad_contrato').each(function () {
                    this.reset();
                    $(this).find('#id').val('');
                }).not('.nomemodulo');
                $("#outro_serv").hide();
                $("#form_envio_email").hide();
                $("#loadpdf").html('');
            });
<?php } ?>
<?php if (in_array(16, $permissoes) || in_array(17, $permissoes)) { ?>
            $("#outro_serv").hide();
            $("#form_envio_email").hide();
            $("#servico").on('change', function (e) {
                var value = $(this).val();
                if (value != 'outro') {
                    $("#outro_serv").hide();
                } else {
                    $("#outro_serv").show();
                }
            });
            $("#modelo_email").prop("disabled", true);
            $("#email_customizado").click(function (e) {
                if ($(this).is(':checked')) {
                    $("#titulo").prop("disabled", true);
                    $("#modelo_email").prop("disabled", false);
                    $("#modelo_email").val('');
                    tinymce.get('textomsg').getBody().setAttribute('contenteditable', 'false');
                } else {
                    $("#titulo").prop("disabled", false);
                    $("#modelo_email").prop("disabled", true);
                    $("#modelo_email").val('');
                    tinymce.get('textomsg').getBody().setAttribute('contenteditable', 'true');
                }
            });
    <?php if ((int) $config[0]->CONF_TROCA_EMAIL == 1) { ?>
                $("#email_padrao").click(function (e) {
                    if ($(this).is(':checked')) {
                        $("#email_notificacao").prop("readonly", true);
                        $("#email_notificacao").val('<?php echo $config[0]->CONF_MAILS_NOTIFY; ?>')
                    } else {
                        $("#email_notificacao").prop("readonly", false);
                        $("#email_notificacao").val("");
                    }
                });
    <?php } ?>
            $("#url_padrao").click(function (e) {
                if ($(this).is(':checked')) {
                    $("#urlcontrato").prop("readonly", true);
                    $.ajax({
                        url: '<?php echo base_url() ?>contratos/removeracentosctt',
                        type: "POST",
                        data: {
                            string: $("#descricao").val()
                        },
                        success: function (data) {
                            $("#urlcontrato").val(data);
                        },
                        error: function () {}
                    });
                } else {
                    $("#urlcontrato").prop("readonly", false);
                    $("#urlcontrato").val("");
                }
            });
            $('#urlcontrato').on('keypress', function () {
                var regex = new RegExp("^[-0-9a-zA-Zàèìòùáéíóúâêîôûãõ\b]+$");
                var _this = this;
                setTimeout(function () {
                    var texto = $(_this).val();
                    if (!regex.test(texto)) {
                        $(_this).val(texto.substring(0, (texto.length - 1)))
                    }
                }, 100);
            });
            jQuery('#form_cad_contrato').bootstrapWizard({
                onTabShow: function (tab, navigation, index) {
                    tab.prevAll().addClass('done');
                    tab.nextAll().removeClass('done');
                    tab.removeClass('done');
                    var $total = navigation.find('li').length;
                    var $current = index + 1;
                    if ($current >= $total) {
                        $('#form_cad_contrato').find('.wizard .next').addClass('hide');
                        $('#form_cad_contrato').find('.wizard .finish').removeClass('hide');
                    } else {
                        $('#form_cad_contrato').find('.wizard .next').removeClass('hide');
                        $('#form_cad_contrato').find('.wizard .finish').addClass('hide');
                    }
                }
            });
            $(".estado").on('change', function (e) {
                var value = $(this).val();
                var uf = $('#estado option:selected').text();
                if (value != 0) {

                    $("#cidades_list").html("");
                    //popula a lista de cidades
                    $.ajax({
                        url: '<?php echo base_url() ?>admin/getCidades/',
                        type: "POST",
                        data: {
                            uf: uf
                        },
                        success: function (data) {
                            var info = $.parseJSON(data);
                            for (var i = 0; i < info.length; i++) {
                                $("#cidades_list").append('<option value="' + info[i].value + '">');
                            }
                        },
                        error: function () {}
                    });
                    $("#cidade").prop("readonly", false);
                } else {
                    $("#cidade").prop("readonly", true);
                }
            });
            $("#finalizar_enviar").on('click', (function (e) {
                e.preventDefault();
                var id = $("#idcontrato").val();
                var formData = new FormData();
                formData.append('id', id);
                $.ajax({
                    url: '<?php echo base_url() ?>admin/finalizaContrato',
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        data = JSON.parse(data);
                        console.log(data);

                        if (data.erro == false) {
                            $("#form_envio_email").show("slow", function () {
                                $('html, body, #cadastro-edicao-contratos').animate({scrollTop: $("#form_envio_email").height() + 200}, 'slow');
                            });
                        } else {
                            msgerror("<?php echo ('Cadastro Edição de Contratos') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                        }
                    },
                    error: function () {}
                });
            }));
<?php } ?>
<?php if (in_array(16, $permissoes) || in_array(17, $permissoes)) { ?>
            $("#form_cad_send").on('submit', (function (e) {
                e.preventDefault();
                var form = $("#form_cad_send");
                $('.form-horizontal').waitMe({
                    effect: 'ios',
                    text: '',
                    bg: 'rgba(255,255,255,0.7)',
                    color: '#000000',
                    sizeW: '',
                    sizeH: '',
                    source: '',
                    onClose: function () {}
                });
                //remove as mensagens
                $('#form_cad_send .error-message').each(function () {
                    $(this).remove();
                });
                var formData = new FormData(this);
                formData.append('titulo', $('input[name=titulo]').val());
                formData.append('textomsg', tinyMCE.get('textomsg').getContent());
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        data = JSON.parse(data);
                        if (data.erro == false) {
                            msgsuccess("<?php echo ('Cadastro Edição de Contratos') ?>", "<?php echo ('Realizado com Sucesso') ?>");
                        } else {
                            if (data.list != null) {

                                $('.form-horizontal').waitMe('hide');
                                var data1 = JSON.parse(data.list);
                                $('#form_cad_send :input').each(function () {
                                    var elementName = $(this).attr('name');
                                    var message = data1[elementName];
                                    if (message) {
                                        var element = $('<span>' + message + '</span>')
                                                .attr({
                                                    'class': 'error-message'
                                                })
                                                .css({
                                                    display: 'none'
                                                });
                                        $(this).after(element);
                                        $(element).fadeIn();
                                    }
                                }).not(':button, :submit, :reset, :hidden');
                            } else {
                                msgerror("<?php echo ('Cadastro Edição de Propostas') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorMessage) {
                        console.log(errorMessage);
                    }
                });
            }));
<?php } ?>
<?php if (in_array(16, $permissoes)) { ?>
            $("#form_cad_contrato").on('submit', (function (e) {
                e.preventDefault();
                var form = $("#form_cad_contrato");
                $('.form-horizontal').waitMe({
                    effect: 'ios',
                    text: '',
                    bg: 'rgba(255,255,255,0.7)',
                    color: '#000000',
                    sizeW: '',
                    sizeH: '',
                    source: '',
                    onClose: function () {}
                });
                //remove as mensagens
                $('#form_cad_contrato .error-message').each(function () {
                    $(this).remove();
                });
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        data = JSON.parse(data);
                        if (data.erro == false) {

                            $("#id").val(data.list.id);
                            $("#idcontrato").val(data.list.id);
                            $("#id_contrato").val(data.list.id);
                            var url = "<?php echo base_url() . 'visualizar?tipo=c&id=' ?>";
                            $("#loadpdf").html('<embed src="' + url + data.list.id + '" width="100%" height="500px">').promise().done(function () {
                                $('.form-horizontal').waitMe('hide');
                            });
                        } else {
                            if (data.list != null) {

                                $('.form-horizontal').waitMe('hide');
                                var data1 = JSON.parse(data.list);
                                $('form :input').each(function () {
                                    var elementName = $(this).attr('name');
                                    var message = data1[elementName];
                                    if (message) {
                                        var element = $('<span>' + message + '</span>')
                                                .attr({
                                                    'class': 'error-message'
                                                })
                                                .css({
                                                    display: 'none'
                                                });
                                        $(this).after(element);
                                        $(element).fadeIn();
                                    }
                                }).not(':button, :submit, :reset, :hidden');
                                alert("Verifique as etapas pois existem campos obrigatorios nao preenchidos!")
                            } else {
                                msgerror("<?php echo ('Cadastro Edição de Contratos') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorMessage) {
                        console.log(errorMessage);
                    }
                });
            }));
<?php } ?>
        $("#table-contratos").on('click', '.btn_duplicar', function (e) {
            e.preventDefault();
            var id = $(this).closest('tr').attr('id');
            $.ajax({
                url: '<?php echo base_url() ?>contratos/duplicar/',
                type: "POST",
                data: {
                    id: id,
                    nomemodulo: '<?php echo $idmodulo; ?>'
                },
                success: function (data) {
                    data = JSON.parse(data);
                    if (data.erro == false) {
                        msgsuccess("<?php echo ('Cadastro Edição de Contratos') ?>", "<?php echo ('Duplicado com sucesso!') ?>");
                    } else {
                        msgerror("<?php echo ('Cadastro Edição de Contratos') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                    }
                }
            });
        });
<?php if (in_array(17, $permissoes)) { ?>
            $("#table-contratos").on('click', '.btn_editar', function (e) {
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                $("#btn_submit").html('Salvar');
                jQuery('#cadastro-edicao-contratos').modal('show', {backdrop: 'static'});
                $("#id_contrato").val(id);
                //remove as mensagens
                $('#form_cad_contrato .error-message').each(function () {
                    $(this).remove();
                });
                //resgata os dados
                $.ajax({
                    url: '<?php echo base_url() ?>contratos/get/',
                    type: "POST",
                    data: {
                        id: id,
                        nomemodulo: '<?php echo $idmodulo; ?>'
                    },
                    success: function (data) {
                        data = JSON.parse(data);
                        var frm = $('#form_cad_contrato');
                        var info = JSON.parse(data.list);
                        var automatico = false;
                        $.each(info[0], function (key, value) {

                            if (key != 'automatico') {
                                if (key != 'email_padrao' && key != 'url_padrao' && key != 'valor_parcelado' && key != 'servico') {
                                    $('[name=' + key + ']', frm).val(value);
                                } else if (key == 'email_padrao') {
    <?php if ((int) $config[0]->CONF_TROCA_EMAIL == 1) { ?>
                                        if (value == 'sim') {
                                            $(this).prop('checked', true);
                                            $("#email_notificacao").prop("readonly", true);
                                        } else {
                                            $(this).prop('checked', false);
                                            $("#email_notificacao").prop("readonly", false);
                                        }
    <?php } ?>
                                } else if (key == 'valor_parcelado') {
                                    $('[name=' + key + ']', frm).val(value);
                                } else if (key == 'url_padrao') {
                                    if (value == 'sim') {
                                        $('[name=' + key + ']', frm).prop('checked', true);
                                        $("#urlcontrato").prop("readonly", true);
                                    } else {
                                        $('[name=' + key + ']', frm).prop('checked', false);
                                        $("#urlcontrato").prop("readonly", false);
                                    }
                                } else if (key == 'servico') {
                                    $('[name=' + key + ']', frm).val(value);
                                    if (value != 'outro') {
                                        $("#outro_serv").hide();
                                    } else {
                                        $("#outro_serv").show();
                                    }
                                }
                            } else {
                                automatico = value;
                            }
                        });
                        if (automatico == '1') {
                            $("#email_notificacao").prop("readonly", true);
                            $("#urlcontrato").prop("readonly", true);
                            $("#ckb_url").hide();
                        } else {
                            $("#email_notificacao").prop("readonly", false);
                            $("#urlcontrato").prop("readonly", false);
                            $("#ckb_url").show();
                        }

                        $('#tags').tagsInput({
                            width: 'auto',
                            defaultText: 'Adicionar',
                        });
                    },
                    error: function (jqXHR, textStatus, errorMessage) {
                        console.log(errorMessage);
                    }
                });
            });
<?php } ?>
<?php if (in_array(18, $permissoes)) { ?>
            $("#table-contratos").on('click', '.btn_excluir', function (e) {
                e.preventDefault();
                var c = confirm("Deseja realmente excluir o registro?");
                if (c) {
                    var id = $(this).closest('tr').attr('id');
                    $(this).closest('tr').fadeOut(function () {
                        $(this).remove();
                    });
                    $.ajax({
                        url: '<?php echo base_url() ?>contratos/delete',
                        type: "POST",
                        data: {
                            id: id,
                            nomemodulo: '<?php echo $idmodulo; ?>'
                        },
                        success: function (data) {

                            data = JSON.parse(data);
                            if (data.erro == false) {
                                msgsuccess("<?php echo ('Cadastro Edição de Contratos') ?>", "<?php echo ('Deletado com sucesso!') ?>");
                            } else {
                                msgerror("<?php echo ('Cadastro Edição de Contratos') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                            }

                            $('#form_cad_contrato').each(function () {
                                this.reset();
                            });
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            });
<?php } ?>
        $("#table-contratos").on('click', '.btn_ativainativa', function (e) {
            e.preventDefault();
            var c = confirm("Deseja realmente alterar o status o registro?");
            if (c) {
                var id = $(this).closest('tr').attr('id');
                $(this).closest('tr').fadeOut(function () {
                    $(this).remove();
                });
                $.ajax({
                    url: '<?php echo base_url() ?>contratos/ativarInativar',
                    type: "POST",
                    data: {
                        id: id,
                        nomemodulo: '<?php echo $idmodulo; ?>'
                    },
                    success: function (data) {

                        data = JSON.parse(data);
                        if (data.erro == false) {
                            msgsuccess("<?php echo ('Cadastro Edição de Contratos') ?>", "<?php echo ('Alterado o Status com sucesso!') ?>");
                        } else {
                            msgerror("<?php echo ('Cadastro Edição de Contratos') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                        }

                        $('#form_cad_contrato').each(function () {
                            this.reset();
                        });
                    },
                    error: function (jqXHR, textStatus, errorMessage) {
                        console.log(errorMessage);
                    }
                });
            }
        });
        $(window).scroll(function () {
            if ($(this).scrollTop() > 600) {
                $('.scrollup').fadeIn('slow');
            } else {
                $('.scrollup').fadeOut('slow');
            }
        });
    });
</script>