<h4><?php echo ("Relação de Usuários Cadastrados") ?></h4>
<div>
    <div class="form-group">
        <?php if (in_array(1, $permissoes)) { ?>
            <div class="col-sm-12" style="text-align: left; padding-bottom: 10px;">
                <button type="button" class="btn btn-success btcad" id="btn_cadastrar" data-toggle="modal" data-target="#cadastro-edicao-usuario"><?php echo ("Cadastrar Novo Usuário") ?></button>
            </div>
        <?php } ?>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <table class="table table-bordered table-striped datatable" id="table-usuarios">
                <thead>
                    <tr>
                        <th width="25"><?php echo ("Id") ?></th>
                        <th><?php echo ("Login") ?></th>
                        <th><?php echo ("E-mail") ?></th>
                        <th><?php echo ("Nome") ?></th>
                        <th><?php echo ("Perfil") ?></th>
                        <th><?php echo ("Status") ?></th>
                        <th width="210"><?php echo ("Ações") ?></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php if (in_array(1, $permissoes) || in_array(2, $permissoes)) { ?>
    <div class="modal fade" id="cadastro-edicao-usuario" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                    <h4 class="modal-title"><strong><?php echo ("Cadastro e Edição de Usuários") ?></strong></h4>
                </div>
                <div class="modal-body">
                    <?php
                    echo form_open_multipart("usuarios/insertedit", array(
                        "id" => "form_cad_usuario",
                        "class" => "form-horizontal validate form-groups-bordered",
                        "data-toggle" => "validator"
                    ));

                    ?>
                    <input type="hidden" id="nomemodulo" name="nomemodulo" value="<?php echo $idmodulo; ?>"/>
                    <div class="form-group">
                        <input type="hidden" id="id" name="id">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo ("Perfil de Usuário") ?></label>
                        <div class="col-sm-5">
                            <select class="form-control" id="perfil" name="perfil">
                                <option value="0">Selecione</option>
                                <?php foreach ($listperfis as $perfil) { ?>
                                    <option value="<?php echo $perfil->TYPE_ID ?>"><?php echo $perfil->TYPE_NAME ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo ("Nome Completo") ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome Completo" maxlength="120">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo ("E-mail") ?></label>
                        <div class="col-sm-5">
                            <div class="input-group minimal">
                                <span class="input-group-addon"><i class="entypo-mail"></i></span>
                                <input type="text" id="email" name="email" class="form-control" data-mask="email" placeholder="E-mail" maxlength="100">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="login" class="col-sm-3 control-label"><?php echo ("Login") ?></label>
                        <div class="col-sm-5">
                            <div class="input-group minimal">
                                <span class="input-group-addon"><i class="entypo-user"></i></span>
                                <input type="text" class="form-control" id="login" name="login" placeholder="Login" maxlength="8">
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="ckb_troca_senha">
                        <div class="col-sm-5 col-sm-offset-3">
                            <div class="input-group minimal">
                                <label><input type="checkbox" name="trocar_senha" id="trocar_senha"><?php echo ("Realizar troca de senha"); ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="senha" class="col-sm-3 control-label"><?php echo ("Senha") ?></label>
                        <div class="col-sm-5">
                            <div class="input-group minimal">
                                <span class="input-group-addon"><i class="entypo-lock"></i></span>
                                <input type="password" class="form-control senha" id="senha" name="senha" placeholder="Senha" maxlength="8" disabled="disabled">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="senha" class="col-sm-3 control-label"><?php echo ("Confirmar senha") ?></label>
                        <div class="col-sm-5">
                            <div class="input-group minimal">
                                <span class="input-group-addon"><i class="entypo-lock"></i></span>
                                <input type="password" class="form-control senha" id="confisenha" name="confisenha" placeholder="Confirmar Senha" maxlength="8" disabled="disabled">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo ("Status") ?></label>
                        <div class="col-sm-5">
                            <select class="form-control" id="status" name="status">
                                <option value="0">Selecione</option>
                                <option value="1"><?php echo ("Liberado") ?></option>
                                <option value="2"><?php echo ("Bloqueado") ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5">
                            <h4><?php echo ("Remetente Padrão para mensagens") ?></h4>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo ("Texto Remetente") ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="textoremetente" name="textoremetente" placeholder="Texto Remetente" maxlength="250">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5">
                            <h4><?php echo ("Configurações de SMTP do usuário") ?></h4>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="senha" class="col-sm-3 control-label"><?php echo ("Host") ?></label>
                        <div class="col-sm-5">
                            <div class="input-group minimal">
                                <span class="input-group-addon"><i class="entypo-lock"></i></span>
                                <input type="text" class="form-control" id="smtphost" name="smtphost" placeholder="<?php echo ("Host") ?>" maxlength="150">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="senha" class="col-sm-3 control-label"><?php echo ("Porta") ?></label>
                        <div class="col-sm-5">
                            <div class="input-group minimal">
                                <span class="input-group-addon"><i class="entypo-lock"></i></span>
                                <input type="text" class="form-control numbers" id="smtpporta" name="smtpporta" placeholder="<?php echo ("Porta") ?>" maxlength="3">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="senha" class="col-sm-3 control-label"><?php echo ("Usuário") ?></label>
                        <div class="col-sm-5">
                            <div class="input-group minimal">
                                <span class="input-group-addon"><i class="entypo-lock"></i></span>
                                <input type="text" class="form-control" id="smtpusuario" name="smtpusuario" placeholder="<?php echo ("Usuário") ?>" maxlength="150">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="senha" class="col-sm-3 control-label"><?php echo ("Senha") ?></label>
                        <div class="col-sm-5">
                            <div class="input-group minimal">
                                <span class="input-group-addon"><i class="entypo-lock"></i></span>
                                <input type="password" class="form-control" id="smtpsenha" name="smtpsenha" placeholder="Insira a senha aqui">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5">
                            <button type="submit" class="btn btn-success" id="btn_submit"><?php echo ("Cadastrar") ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<script>
    jQuery(document).ready(function ($) {

        $(".senha").on('click', function (e) {
            e.preventDefault();

            $(this).notify(
                    '<?php echo ("A senha deve conter 8 caracteres") ?>',
                    {
                        position: 'right',
                        autoHide: true,
                        autoHideDelay: 3000,
                        className: "info"
                    }
            );
        });

        $('#table-usuarios').DataTable({
            dom: "Bfrtip",
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, "desc"]
            ],
            "ajax": {
                "url": "<?php echo base_url() ?>listausuarios",
                "type": "POST",
                "data": {
                    "modulo": '<?php echo $idmodulo; ?>'
                }
            },
            columns: [
                null,
                null,
                null,
                null,
                null,
                null,
                {orderable: false},
            ],
            "columnDefs": [
                {
                    "targets": [6],
                    "orderable": false,
                    "render": function (data, type, row) {

                        if (row[5] == 'Liberado') {
                            return '<a href="#" class="btn btn-warning btn-xs btn_editar" title="Editar"><i class="fa fa-pencil-square-o"></i><?php echo ("Editar") ?></a> <a href="#" class="btn btn-danger btn-xs btn_excluir" title="Excluir"><i class="fa fa-trash-o"></i><?php echo ("Excluir") ?></a> <a href="#" class="btn btn-info btn-xs btn_bloquear" title="Bloquear"><i class="fa fa-lock"></i><?php echo ("Bloquear") ?></a>'
                        } else {
                            return '<a href="#" class="btn btn-warning btn-xs btn_editar" title="Editar"><i class="fa fa-pencil-square-o"></i><?php echo ("Editar") ?></a> <a href="#" class="btn btn-danger btn-xs btn_excluir" title="Excluir"><i class="fa fa-trash-o"></i><?php echo ("Excluir") ?></a> <a href="#" class="btn btn-success btn-xs btn_desbloquear" title="Desbloquear"><i class="fa fa-unlock"></i><?php echo ("Desbloquear") ?></a>'
                        }
                    }
                },
            ],
            select: true,
            'fnRowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", aData[0]);
                return nRow;
            }
        });

<?php if (in_array(1, $permissoes)) { ?>
            $("#btn_cadastrar").on('click', (function (e) {
                e.preventDefault();

                $('#form_cad_usuario').each(function () {
                    this.reset();
                    $(this).find('#id').val('');
                }).not('.nomemodulo');

                $("#btn_submit").html('Cadastrar');

                $("#ckb_troca_senha").hide();

                $("#senha").prop("disabled", false);
                $("#confisenha").prop("disabled", false);
            }));
<?php } ?>

        //submit form
        $("#form_cad_usuario").on('submit', (function (e) {
            e.preventDefault();

            var form = $("#form_cad_usuario");

            $('.form-horizontal').waitMe({
                effect: 'ios',
                text: '',
                bg: 'rgba(255,255,255,0.7)',
                color: '#000000',
                sizeW: '',
                sizeH: '',
                source: '',
                onClose: function () {}
            });

            //remove as mensagens
            $('#form_cad_usuario .error-message').each(function () {
                $(this).remove();
            });

            var formData = new FormData(this);

            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {

                    data = JSON.parse(data);

                    if (data.erro == false) {
                        msgsuccess("<?php echo ('Cadastro Edição de Usuários') ?>", "<?php echo ('Realizado com sucesso!') ?>");

                        $("#senha").prop("disabled", "disabled");
                        $("#confisenha").prop("disabled", "disabled");

                    } else {
                        if (data.list != null) {

                            $('.form-horizontal').waitMe('hide');

                            var data1 = JSON.parse(data.list);

                            var count = 0;

                            $('form :input').each(function () {
                                var elementName = $(this).attr('name');
                                var message = data1[elementName];
                                if (message) {
                                    var element = $('<span>' + message + '</span>')
                                            .attr({
                                                'class': 'error-message'
                                            })
                                            .css({
                                                display: 'none'
                                            });
                                    $(this).after(element);
                                    $(element).fadeIn();
                                }
                            }).not(':button, :submit, :reset, :hidden');
                        } else {
                            msgerror("<?php echo ('Cadastro Edição de Usuários') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorMessage) {
                    console.log(errorMessage);
                }
            });
        }));

<?php if (in_array(2, $permissoes)) { ?>
            $("#table-usuarios").on('click', '.btn_editar', function (e) {
                e.preventDefault();

                var id = $(this).closest('tr').attr('id');

                $("#btn_submit").html('Salvar');

                $("#ckb_troca_senha").show();

                $("#senha").prop("disabled", "disabled");
                $("#confisenha").prop("disabled", "disabled");

                jQuery('#cadastro-edicao-usuario').modal('show', {backdrop: 'static'});

                //remove as mensagens
                $('#form_cad_usuario .error-message').each(function () {
                    $(this).remove();
                });

                //resgata os dados
                $.ajax({
                    url: '<?php echo base_url() ?>usuarios/get/',
                    type: "POST",
                    data: {
                        id: id,
                        nomemodulo: '<?php echo $idmodulo; ?>'
                    },
                    success: function (data) {
                        data = JSON.parse(data);

                        var frm = $('#form_cad_usuario');
                        var info = JSON.parse(data.list);

                        $.each(info[0], function (key, value) {
                            $('[name=' + key + ']', frm).val(value);
                        });
                    },
                    error: function (jqXHR, textStatus, errorMessage) {
                        console.log(errorMessage);
                    }
                });
            });
<?php } ?>
<?php if (in_array(3, $permissoes)) { ?>
            $("#table-usuarios").on('click', '.btn_excluir', function (e) {
                e.preventDefault();

                var c = confirm("Deseja realmente excluir o registro?");

                if (c) {
                    var id = $(this).closest('tr').attr('id');

                    $(this).closest('tr').fadeOut(function () {
                        $(this).remove();
                    });

                    $.ajax({
                        url: '<?php echo base_url() ?>usuarios/delete',
                        type: "POST",
                        data: {
                            id: id,
                            nomemodulo: '<?php echo $idmodulo; ?>'
                        },
                        success: function (data) {

                            data = JSON.parse(data);

                            if (data.erro == false) {
                                msgsuccess("<?php echo ('Cadastro Edição de Usuários') ?>", "<?php echo ('Deletado com sucesso!') ?>");
                            } else {
                                msgerror("<?php echo ('Cadastro Edição de Usuários') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                            }

                            $('#form_cad_usuario').each(function () {
                                this.reset();
                            });
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            });
<?php } ?>
<?php if (in_array(1, $permissoes) || in_array(2, $permissoes) || in_array(3, $permissoes)) { ?>
            $("#table-usuarios").on('click', '.btn_bloquear', function (e) {
                e.preventDefault();

                var c = confirm("Deseja realmente bloquear o registro?");

                if (c) {
                    var id = $(this).closest('tr').attr('id');

                    $.ajax({
                        url: '<?php echo base_url() ?>usuarios/block',
                        type: "POST",
                        data: {
                            id: id,
                            nomemodulo: '<?php echo $idmodulo; ?>'
                        },
                        success: function (data) {

                            data = JSON.parse(data);

                            if (data.erro == false) {
                                msgsuccess("<?php echo ('Cadastro Edição de Usuários') ?>", "<?php echo ('Bloqueado com sucesso!') ?>");
                            } else {
                                msgerror("<?php echo ('Cadastro Edição de Usuários') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                            }

                            $('#form_cad_usuario').each(function () {
                                this.reset();
                            });
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            });

            $("#table-usuarios").on('click', '.btn_desbloquear', function (e) {
                e.preventDefault();

                var c = confirm("Deseja realmente desbloquear o registro?");

                if (c) {
                    var id = $(this).closest('tr').attr('id');

                    $.ajax({
                        url: '<?php echo base_url() ?>usuarios/desblock',
                        type: "POST",
                        data: {
                            id: id,
                            nomemodulo: '<?php echo $idmodulo; ?>'
                        },
                        success: function (data) {

                            data = JSON.parse(data);

                            if (data.erro == false) {
                                msgsuccess("<?php echo ('Cadastro Edição de Usuários') ?>", "<?php echo ('Desbloqueado com sucesso!') ?>");
                            } else {
                                msgerror("<?php echo ('Cadastro Edição de Usuários') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                            }

                            $('#form_cad_usuario').each(function () {
                                this.reset();
                            });
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            });
<?php } ?>
<?php if (in_array(1, $permissoes) || in_array(2, $permissoes)) { ?>
            $("#trocar_senha").click(function (e) {
                if ($(this).is(':checked')) {
                    $("#senha").prop("disabled", false);
                    $("#confisenha").prop("disabled", false);
                } else {
                    $("#senha").prop("disabled", "disabled");
                    $("#confisenha").prop("disabled", "disabled");
                }
            });
<?php } ?>
    });
</script>