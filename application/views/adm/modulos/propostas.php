<h4><?php echo ("Relação de Propostas Cadastradas") ?></h4>
<div>
    <div class="form-group" style="height: 50px">
        <?php if (in_array(10, $permissoes)) { ?>
            <div class="col-sm-12" style="text-align: left; padding-bottom: 10px;">
                <button type="button" class="btn btn-success btcad" id="btn_cadastrar" data-toggle="modal" data-target="#cadastro-edicao-propostas"><?php echo ("Cadastrar Nova Proposta") ?></button>
            </div>
        <?php } ?>
    </div>

    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title"><?php echo ("Filtro") ?><a data-toggle="collapse" href="#collapse1" style="margin-left: 20px"><i class="entypo-down-open"></i></a></h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="form-group busca">
                        <div class="col-sm-12 linha-busca">
                            <div class="col-sm-9">
                                <label for="busca_titulo" class="control-label"><?php echo ("Título: ") ?></label>
                                <input type="text" class="form-control" id="busca_titulo" name="busca_titulo" placeholder="<?php echo ("Buscar Título") ?>">
                            </div>
                            <div class="col-sm-3">
                                <label for="busca_categoria" class="control-label"><?php echo ("Categoria: ") ?></label>
                                <input type="text" class="form-control" id="busca_categoria" name="busca_categoria" placeholder="<?php echo ("Buscar Categoria") ?>">
                            </div>
                        </div>
                        <div class="col-sm-12 linha-busca">
                            <div class="col-sm-2">
                                <label for="busca_status" class="control-label"><?php echo ("Status: ") ?></label>
                                <input type="text" class="form-control" id="busca_status" name="busca_status" placeholder="<?php echo ("Buscar Status") ?>">
                            </div>
                            <div class="col-sm-4">
                                <label for="busca_cliente" class="control-label"><?php echo ("Cliente: ") ?></label>
                                <input type="text" class="form-control" id="busca_cliente" name="busca_cliente" placeholder="<?php echo ("Buscar Cliente") ?>">
                            </div>
                            <div class="col-sm-2">
                                <label for="busca_data" class="control-label"><?php echo ("Período De: ") ?></label>
                                <input type="date" class="form-control" id="busca_data" name="busca_data"  placeholder="<?php echo ("Buscar Inicial") ?>">
                            </div>
                            <div class="col-sm-2">
                                <label for="busca_data" class="control-label"><?php echo ("Até: ") ?></label>
                                <input type="date" class="form-control" id="busca_data_ate" name="busca_data_ate" placeholder="<?php echo ("Buscar Final") ?>">
                            </div>
                            <div class="col-sm-2">
                                <label for="busca_situacao" class="control-label"><?php echo ("Situação: ") ?></label>
                                <input type="text" class="form-control" id="busca_situacao" name="busca_situacao" placeholder="<?php echo ("Buscar Situação") ?>">
                            </div>
                        </div>
                        <div class="col-sm-12 linha-busca">
                            <div class="col-sm-9">
                                <label for="busca_vendedor" class="control-label"><?php echo ("Vendedor: ") ?></label>
                                <input type="text" class="form-control" id="busca_vendedor" name="busca_vendedor" placeholder="<?php echo ("Vendedor") ?>">
                            </div>
                            <div class="col-sm-3 FieldPadTop28">
                                <input type="button" class="btn form-control btn-block btn-success" value="Buscar" id="btnbuscar" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <table class="table table-bordered table-striped datatable" id="table-propostas">
                <thead>
                    <tr>
                        <th width="10"><?php echo ("Id") ?></th>
                        <th width="160"><?php echo ("Título da Proposta") ?></th>
                        <th width="60"><?php echo ("Cliente") ?></th>
                        <th width="60"><?php echo ("Vendedor") ?></th>
                        <th width="90"><?php echo ("Categoria") ?></th>
                        <th width="20"><?php echo ("Data") ?></th>
                        <th width="20"><?php echo ("Situação") ?></th>
                        <th width="50"><?php echo ("Status") ?></th>
                        <th width="270"><?php echo ("Ações") ?></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php if (in_array(10, $permissoes) || in_array(11, $permissoes)) { ?>
    <div class="modal fade" id="cadastro-edicao-propostas" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                    <h4 class="modal-title"><strong><?php echo ("Cadastro e Edição de Propostas") ?></strong></h4>
                </div>
                <div class="modal-body">
                    <?php
                    echo form_open_multipart("propostas/insertedit", array(
                        "id" => "form_cad_proposta",
                        "class" => "form-horizontal form-groups-bordered panel-wizard",
                        "data-toggle" => "validator"
                    ));

                    ?>
                    <ul class="nav nav-justified nav-wizard">
                        <li class="active"><a href="#tab1-4" data-toggle="tab"><?php echo (" Informações Básicas") ?></a></li>
                        <li><a href="#tab2-4" data-toggle="tab"><?php echo (" Contato da Empresa") ?></a></li>
                        <li><a href="#tab3-4" data-toggle="tab"><?php echo (" Modelo do Contrato, Pagamento") ?></a></li>
                        <li><a href="#tab4-4" data-toggle="tab"><?php echo (" Visualizar Proposta e Finalizar") ?></a></li>
                    </ul>
                    <ul class="list-unstyled wizard">
                        <li class="pull-right finish hide">
                            <button type="submit" class="btn btn-info"><?php echo ("Gerar") ?></button>
                            <button type="button" class="btn btn-success" id="finalizar_enviar"><?php echo ("Finalizar e Enviar") ?></button>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1-4">
                            <input type="hidden" id="nomemodulo" name="nomemodulo" value="<?php echo $idmodulo; ?>"/>
                            <input type="hidden" id="id" name="id"/>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Título da Proposta") ?></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="descricao" name="descricao" placeholder="<?php echo ("Descrição da Proposta") ?>" maxlength="120">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Modelo da Proposta") ?></label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="modelo" name="modelo">
                                        <option value="">Selecione</option>
                                        <?php foreach ($listamodelos as $modelo) { ?>
                                            <option value="<?php echo $modelo->MDL_ID ?>"><?php echo $modelo->MDL_DESC ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Categoria") ?></label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="servico" name="servico">
                                        <option value="">Selecione</option>
                                        <option value="outro">Outro</option>
                                        <?php foreach ($listaservicos as $serv) { ?>
                                            <option value="<?php echo $serv->SERV_ID ?>"><?php echo $serv->SERV_DESC ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-8 col-sm-offset-3" id="outro_serv">
                                    <input type="text" class="form-control" id="outro_servicos" name="outro_servicos" placeholder="<?php echo ("Digite a Categoria") ?>" maxlength="120">
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab2-4">
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Responsável pela Proposta") ?></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="nome_contato" name="nome_contato" placeholder="Contato da Empresa" maxlength="200"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo ("E-mail do Contato") ?></label>
                                <div class="col-sm-8">
                                    <div class="input-group minimal">
                                        <span class="input-group-addon"><i class="entypo-mail"></i></span>
                                        <input type="text" id="email_contato" name="email_contato" class="form-control" data-mask="email" placeholder="E-mail" maxlength="200">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab3-4">
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Modelo do Contrato") ?></label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="modelo_contrato" name="modelo_contrato">
                                        <option value="">Selecione</option>
                                        <?php foreach ($listamodeloscontratos as $modelo) { ?>
                                            <option value="<?php echo $modelo->MDL_ID ?>"><?php echo $modelo->MDL_DESC ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo ("E-mail interno para notificação") ?></label>
                                <div class="col-sm-8">
                                    <input type="text" id="email" name="email" class="form-control" placeholder="E-mail" value="<?php echo $user->email; ?>">
                                </div>
                                <?php if ((int) $config[0]->CONF_TROCA_EMAIL == 1) { ?>
                                    <div class="col-sm-3 col-sm-offset-3">
                                        <label class="checkbox-inline"><input type="checkbox" name="email_padrao" id="email_padrao" value="sim" checked="checked"><?php echo ("E-mail padrão do sistema") ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label for="login" class="col-sm-3 control-label"><?php echo ("URL da proposta") ?></label>
                                <div class="col-sm-8">
                                    <?php echo base_url() . 'proposta/'; ?><input type="text" class="form-control" id="urlproposta" name="urlproposta" placeholder="URL da proposta" maxlength="150">
                                </div>
                                <div class="col-sm-2 col-sm-offset-3">
                                    <label class="checkbox-inline"><input type="checkbox" id="url_padrao" name="url_padrao" value="sim"><?php echo ("Url padrão") ?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Qtde. de Drogarias/Farmacias") ?></label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control numbers" id="qde_farmacias" name="qde_farmacias" placeholder="Qtde. de Drogarias/Farmacias">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Valor Total") ?></label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="valor_total" name="valor_total" placeholder="<?php echo ("Valor Total") ?>">
                                </div>
                                <label for="field-1" class="col-sm-2 control-label"><?php echo ("Valor Parcelado") ?></label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="valor_parcelado" name="valor_parcelado" placeholder="<?php echo ("Valor Parcelado") ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Qtde. de Parcelas") ?></label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control numbers" id="parcelas" name="parcelas" placeholder="Qtde. de Parcelas">
                                </div>
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Intervalo entre Parcelas (dias)") ?></label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control numbers" id="intervalo_parcelas" name="intervalo_parcelas" placeholder="Intervalo" maxlength="2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><?php echo ("Convênios") ?></label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="convenios" name="convenios" placeholder="<?php echo ("Convênios") ?>" style="height: 150px;overflow-x: hidden;"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab4-4">
                            <input type="hidden" id="idproposta"/>
                            <div id="loadpdf" class="loadpdf"></div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <div class="modal-body" id="form_envio_email">
                    <?php
                    echo form_open_multipart("admin/enviarEmailCliente", array(
                        "id" => "form_cad_send",
                        "class" => "form-horizontal validate form-groups-bordered",
                        "data-toggle" => "validator"
                    ));

                    ?>
                    <?php echo form_hidden('nomemodulo', $idmodulo) ?>
                    <input type="hidden" id="id_proposta" name="id_proposta"/>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div style="float: left;"><h4><?php echo ("Enviar Proposta ao Cliente") ?></h4></div><div class="divinfo"><a href="#" class="left" id="info"><i class="fa fa-info-circle" aria-hidden="true"></i></a></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-1">
                            <label class="checkbox-inline"><input type="checkbox" name="email_customizado" id="email_customizado" value="sim"/><?php echo ("Enviar e-mail modelo"); ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-1 control-label"><?php echo ("Modelo") ?></label>
                        <div class="col-sm-6">
                            <select class="form-control" id="modelo_email" name="modelo_email">
                                <option value="">Selecione</option>
                                <?php foreach ($listamodelosemail as $modelo) { ?>
                                    <option value="<?php echo $modelo->MDL_ID ?>"><?php echo $modelo->MDL_DESC ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="login" class="col-sm-1 control-label"><?php echo ("Título") ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="titulo" name="titulo" placeholder="<?php echo ("Título") ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="login" class="col-sm-1 control-label"><?php echo ("Mensagem") ?></label>
                        <div class="col-sm-10">
                            <textarea class="form-control editor" id="textomsg" name="textomsg" maxlength="500" style="height: 300px"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5">
                            <button type="submit" class="btn btn-success"><?php echo ("Enviar") ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<script>
    jQuery(document).ready(function ($) {

        $("#info").on('click', function (e) {
            e.preventDefault();

            $.notify.addStyle('foo', {
                html:
                        "<div>" +
                        "<div class='clearfix'>" +
                        "<div class='title' data-notify-html='title'/>" +
                        "<div class='buttons'>" +
                        "<a href='#' class='btn btn-info btn-xs no'>Fechar</a>" +
                        "</div>" +
                        "<div class='info'>" +
                        "<div>URL do contrato/proposta<span>[valor-url]</span></div>" +
                        "<div>Tipo (contrato ou proposta)<span>[valor-tipo]</span></div>" +
                        "<div><?php echo ("Data de geração do documento") ?><span>[valor-data]</span></div>" +
                        "<div><?php echo ("Descrição do documento") ?><span>[valor-titulo]</span></div>" +
                        "<div>Cliente<span>[valor-cliente]</span></div>" +
                        "<div>Nome do Usuário do Sistema<span>[usuario]</span></div>" +
                        "<div><?php echo ("Descrição do servico") ?><span>[valor-servico]</span></div>" +
                        "<div><?php echo ("Informações básicas do documento") ?><span>[valor-informacao]</span></div>" +
                        "<div>Data de envio do e-mail<span>[data-envio]</span></div><br/>" +
                        "<div><strong><?php echo ("Para adicionar a url do documento não é necessário utilizar a opção link do editor, basta informar a tag. Caso queira adicionar um link diretamente para o documento utilize a opção link do editor infromando a tag de url ([valor-url]) no campo 'Url'.") ?></strong></div>" +
                        "</div>" +
                        "</div>" +
                        "</div>"
            });

            $(this).notify({
                title: '<?php echo ("Utilização das tags no texto:") ?>',
                button: 'Confirm'
            }, {
                style: 'foo',
                elementPosition: 'right top',
                autoHide: false,
                clickToHide: false
            });
        });

        $(document).on('click', '.notifyjs-foo-base .no', function (e) {
            e.preventDefault();
            $(this).trigger('notify-hide');
        });

        var table = $('#table-propostas').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [
                [0, "desc"]
            ],
            "dom": '<"top"i>rt<"bottom"lp><"clear">',
            "ajax": {
                "url": "<?php echo base_url() ?>listapropostas",
                "type": "POST",
                "data": {
                    "modulo": '<?php echo $idmodulo; ?>'
                }
            },
            columns: [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                {orderable: false},
                {orderable: false, visible: false}
            ],
            "columnDefs": [
                {
                    "targets": [8],
                    "orderable": false,
                    "render": function (data, type, row) {

                        var btnstatus = null;

                        if (row[6] != 'Inativo') {
                            btnstatus = '<a href="#" class="btn btn-danger btn-xs btn_ativainativa" title="Inativar"><i class="fa fa-ban"></i> <?php echo ("Inativar") ?></a>'
                        } else {
                            btnstatus = '<a href="#" class="btn btn-primary btn-xs btn_ativainativa" title="Ativar"><i class="fa fa-ban"></i> <?php echo ("Ativar") ?></a>'
                        }
                        if (row[5] != 'Aprovada') {
                            return '<a href="#" class="btn btn-info btn-xs btn_duplicar" title="Duplicar"><i class="fa fa fa-copy"></i> <?php echo ("Duplicar") ?></a> <?php if (in_array(11, $permissoes)) { ?><a href="#" class="btn btn-warning btn-xs btn_editar" title="Editar"><i class="fa fa-pencil-square-o"></i><?php } ?> <?php echo ("Editar") ?></a> <?php if (in_array(12, $permissoes)) { ?><a href="#" class="btn btn-danger btn-xs btn_excluir" title="Excluir"><i class="fa fa-trash-o"></i> <?php echo ("Excluir") ?></a><?php } ?> ' + btnstatus;
                        } else {
                            return '<a href="#" class="btn btn-info btn-xs btn_duplicar" title="Duplicar"><i class="fa fa fa-copy"></i> <?php echo ("Duplicar") ?></a> <a href="#" class="btn btn-success btn-xs" title="Aprovada"><i class="fa fa-check"></i> <?php echo ("Aprovada") ?></a> ' + btnstatus;
                            ;
                        }
                    }
                },
            ],
            select: true,
            'fnRowCallback': function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", aData[0]);
                return nRow;
            },
            "stripeClasses": ['', '']
        });

        $('#btnbuscar').click(function () {

            table.column(1).search($('#busca_titulo').val().trim());
            table.column(2).search($('#busca_cliente').val().trim());
            table.column(3).search($('#busca_vendedor').val().trim());
            table.column(4).search($('#busca_categoria').val().trim());
            table.column(5).search($('#busca_data').val().trim());
            table.column(6).search($('#busca_situacao').val().trim());
            table.column(7).search($('#busca_status').val().trim());
            table.column(9).search($('#busca_data_ate').val().trim());

            table.draw();
        });

        $('#email').tagsInput({
            width: 'auto',
            defaultText: 'Adicionar E-mail'
        });
		
		$('#email_contato').tagsInput({
            width: 'auto',
            defaultText: 'Adicionar E-mail'
        });

<?php if (in_array(10, $permissoes) || in_array(11, $permissoes)) { ?>
            $("#valor_total").maskMoney({prefix: 'R$ ', allowNegative: true, thousands: '.', decimal: ',', affixesStay: false});
            $("#valor_parcelado").maskMoney({prefix: 'R$ ', allowNegative: true, thousands: '.', decimal: ',', affixesStay: false});
<?php } ?>

<?php if (in_array(10, $permissoes)) { ?>
            $("#btn_cadastrar").on("click", function (e) {
                e.preventDefault();

                $('#form_cad_proposta').each(function () {
                    this.reset();
                    $(this).find('#id').val('');
                }).not('.nomemodulo');

                $("#outro_serv").hide();
                $("#form_envio_email").hide();
                $("#loadpdf").html('');
            });
<?php } ?>
<?php if (in_array(10, $permissoes) || in_array(11, $permissoes)) { ?>

            $("#outro_serv").hide();
            $("#form_envio_email").hide();

            $("#servico").on('change', function (e) {
                var value = $(this).val();
                if (value != 'outro') {
                    $("#outro_serv").hide();
                } else {
                    $("#outro_serv").show();
                }
            });

            $("#modelo_email").prop("disabled", true);

            $("#email_customizado").click(function (e) {
                if ($(this).is(':checked')) {
                    $("#titulo").prop("disabled", true);
                    $("#modelo_email").prop("disabled", false);
                    $("#modelo_email").val('');
                    tinymce.get('textomsg').getBody().setAttribute('contenteditable', 'false');
                } else {
                    $("#titulo").prop("disabled", false);
                    $("#modelo_email").prop("disabled", true);
                    $("#modelo_email").val('');
                    tinymce.get('textomsg').getBody().setAttribute('contenteditable', 'true');
                }
            });

    <?php if ((int) $config[0]->CONF_TROCA_EMAIL == 1) { ?>
                $("#email_padrao").click(function (e) {
                    if ($(this).is(':checked')) {
                        $("#email").prop("readonly", true);

                        $("#email").val('<?php echo $config[0]->CONF_MAILS_NOTIFY; ?>')
                    } else {
                        $("#email").prop("readonly", false);
                        $("#email").val("");
                    }
                });
    <?php } ?>

            $("#url_padrao").click(function (e) {
                if ($(this).is(':checked')) {
                    $("#urlproposta").prop("readonly", true);

                    $.ajax({
                        url: '<?php echo base_url() ?>propostas/removeacentosprop',
                        type: "POST",
                        data: {
                            string: $("#descricao").val()
                        },
                        success: function (data) {
                            $("#urlproposta").val(data);
                        },
                        error: function () {}
                    });
                } else {
                    $("#urlproposta").prop("readonly", false);
                    $("#urlproposta").val("");
                }
            });

            $('#urlproposta').on('keypress', function () {
                var regex = new RegExp("^[-0-9a-zA-Zàèìòùáéíóúâêîôûãõ\b]+$");
                var _this = this;

                setTimeout(function () {
                    var texto = $(_this).val();
                    if (!regex.test(texto)) {
                        $(_this).val(texto.substring(0, (texto.length - 1)))
                    }
                }, 100);
            });

            jQuery('#form_cad_proposta').bootstrapWizard({
                onTabShow: function (tab, navigation, index) {
                    tab.prevAll().addClass('done');
                    tab.nextAll().removeClass('done');
                    tab.removeClass('done');

                    var $total = navigation.find('li').length;
                    var $current = index + 1;

                    if ($current >= $total) {
                        $('#form_cad_proposta').find('.wizard .next').addClass('hide');
                        $('#form_cad_proposta').find('.wizard .finish').removeClass('hide');
                    } else {
                        $('#form_cad_proposta').find('.wizard .next').removeClass('hide');
                        $('#form_cad_proposta').find('.wizard .finish').addClass('hide');
                    }
                }
            });

            $("#finalizar_enviar").on('click', (function (e) {
                e.preventDefault();

                var id = $("#idproposta").val();
                var formData = new FormData();
                formData.append('id', id);

                $.ajax({
                    url: '<?php echo base_url() ?>admin/finalizaProposta',
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        data = JSON.parse(data);

                        if (data.erro == false) {
                            $("#form_envio_email").show("slow", function () {
                                $('html, body, #cadastro-edicao-propostas').animate({scrollTop: $("#form_envio_email").height() + 200}, 'slow');
                            });
                        } else {
                            msgerror("<?php echo ('Cadastro Edição de Propostas') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                        }
                    },
                    error: function () {}
                });

            }));

            $("#form_cad_send").on('submit', (function (e) {
                e.preventDefault();

                var form = $("#form_cad_send");

                $('.form-horizontal').waitMe({
                    effect: 'ios',
                    text: '',
                    bg: 'rgba(255,255,255,0.7)',
                    color: '#000000',
                    sizeW: '',
                    sizeH: '',
                    source: '',
                    onClose: function () {}
                });

                //remove as mensagens
                $('#form_cad_send .error-message').each(function () {
                    $(this).remove();
                });

                var formData = new FormData(this);
                formData.append('titulo', $('input[name=titulo]').val());
                formData.append('textomsg', tinyMCE.get('textomsg').getContent());

                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        data = JSON.parse(data);

                        if (data.erro == false) {
                            msgsuccess("<?php echo ('Cadastro Edição de Propostas') ?>", "<?php echo ('Realizado com Sucesso') ?>");
                        } else {
                            if (data.list != null) {

                                $('.form-horizontal').waitMe('hide');

                                var data1 = JSON.parse(data.list);

                                $('#form_cad_send :input').each(function () {
                                    var elementName = $(this).attr('name');
                                    var message = data1[elementName];
                                    if (message) {
                                        var element = $('<span>' + message + '</span>')
                                                .attr({
                                                    'class': 'error-message'
                                                })
                                                .css({
                                                    display: 'none'
                                                });

                                        $(this).after(element);
                                        $(element).fadeIn();
                                    }
                                }).not(':button, :submit, :reset, :hidden');
                            } else {
                                msgerror("<?php echo ('Cadastro Edição de Propostas') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorMessage) {
                        console.log(errorMessage);
                    }
                });
            }));

            //submit form
            $("#form_cad_proposta").on('submit', (function (e) {
                e.preventDefault();

                var form = $("#form_cad_proposta");

                $('.form-horizontal').waitMe({
                    effect: 'ios',
                    text: '',
                    bg: 'rgba(255,255,255,0.7)',
                    color: '#000000',
                    sizeW: '',
                    sizeH: '',
                    source: '',
                    onClose: function () {}
                });

                //remove as mensagens
                $('#form_cad_proposta .error-message').each(function () {
                    $(this).remove();
                });

                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        data = JSON.parse(data);

                        if (data.erro == false) {

                            $("#id").val(data.list.id);
                            $("#idproposta").val(data.list.id);
                            $("#id_proposta").val(data.list.id);

                            var url = "<?php echo base_url() . 'visualizar?tipo=p&id=' ?>";

                            $("#loadpdf").html('<embed src="' + url + data.list.id + '" width="100%" height="500px">').promise().done(function () {
                                $('.form-horizontal').waitMe('hide');
                            });
                        } else {
                            if (data.list != null) {
                                $('.form-horizontal').waitMe('hide');
                                var data1 = JSON.parse(data.list);

                                $('form :input').each(function () {
                                    var elementName = $(this).attr('name');
                                    var message = data1[elementName];
                                    if (message) {
                                        var element = $('<span>' + message + '</span>')
                                                .attr({
                                                    'class': 'error-message'
                                                })
                                                .css({
                                                    display: 'block'
                                                });
                                        $(this).after(element);
                                        $(element).fadeIn();
                                    }
                                }).not(':button, :submit, :reset, :hidden');
                                alert("Verifique as etapas pois existem campos obrigatorios nao preenchidos!")
                            } else {
                                msgerror("<?php echo ('Cadastro Edição de Propostas') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorMessage) {
                        console.log(errorMessage);
                    }
                });
            }));
<?php } ?>

        $("#table-propostas").on('click', '.btn_duplicar', function (e) {
            e.preventDefault();

            var id = $(this).closest('tr').attr('id');

            $.ajax({
                url: '<?php echo base_url() ?>propostas/duplicar/',
                type: "POST",
                data: {
                    id: id,
                    nomemodulo: '<?php echo $idmodulo; ?>'
                },
                success: function (data) {
                    data = JSON.parse(data);
                    if (data.erro == false) {
                        msgsuccess("<?php echo ('Cadastro Edição de Propostas') ?>", "<?php echo ('Duplicado com sucesso!') ?>");
                    } else {
                        msgerror("<?php echo ('Cadastro Edição de Propostas') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                    }
                }
            });
        });

<?php if (in_array(11, $permissoes)) { ?>
            $("#table-propostas").on('click', '.btn_editar', function (e) {
                e.preventDefault();

                var id = $(this).closest('tr').attr('id');

                jQuery('#cadastro-edicao-propostas').modal('show', {backdrop: 'static'});

                $("#id_proposta").val(id);

                //remove as mensagens
                $('#form_cad_proposta .error-message').each(function () {
                    $(this).remove();
                });

                //resgata os dados
                $.ajax({
                    url: '<?php echo base_url() ?>propostas/get/',
                    type: "POST",
                    data: {
                        id: id,
                        nomemodulo: '<?php echo $idmodulo; ?>'
                    },
                    success: function (data) {
                        data = JSON.parse(data);

                        var frm = $('#form_cad_proposta');
                        var info = JSON.parse(data.list);

                        $.each(info[0], function (key, value) {
                            if (key != 'email_padrao' && key != 'url_padrao' && key != 'valor_parcelado' && key != 'servico') {
                                $('[name=' + key + ']', frm).val(value);
                            } else if (key == 'email_padrao') {
    <?php if ((int) $config[0]->CONF_TROCA_EMAIL == 1) { ?>
                                    if (value == 'sim') {
                                        $(this).prop('checked', true);
                                        $("#email").prop("readonly", true);
                                    } else {
                                        $(this).prop('checked', false);
                                        $("#email").prop("readonly", false);
                                    }
    <?php } ?>
                            } else if (key == 'valor_parcelado') {
                                $('[name=' + key + ']', frm).val(value);
                            } else if (key == 'url_padrao') {
                                if (value == 'sim') {
                                    $('[name=' + key + ']', frm).prop('checked', true);
                                    $("#urlproposta").prop("readonly", true);
                                } else {
                                    $('[name=' + key + ']', frm).prop('checked', false);
                                    $("#urlproposta").prop("readonly", false);
                                }
                            } else if (key == 'servico') {
                                $('[name=' + key + ']', frm).val(value);
                                if (value != 'outro') {
                                    $("#outro_serv").hide();
                                } else {
                                    $("#outro_serv").show();
                                }
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorMessage) {
                        console.log(errorMessage);
                    }
                });
            });
<?php } ?>
<?php if (in_array(12, $permissoes)) { ?>
            $("#table-propostas").on('click', '.btn_excluir', function (e) {
                e.preventDefault();

                var c = confirm("Deseja realmente excluir o registro?");

                if (c) {
                    var id = $(this).closest('tr').attr('id');

                    $(this).closest('tr').fadeOut(function () {
                        $(this).remove();
                    });

                    $.ajax({
                        url: '<?php echo base_url() ?>propostas/delete',
                        type: "POST",
                        data: {
                            id: id,
                            nomemodulo: '<?php echo $idmodulo; ?>'
                        },
                        success: function (data) {

                            data = JSON.parse(data);

                            if (data.erro == false) {
                                msgsuccess("<?php echo ('Cadastro Edição de Propostas') ?>", "<?php echo ('Deletado com sucesso!') ?>");
                            } else {
                                msgerror("<?php echo ('Cadastro Edição de Propostas') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                            }

                            $('#form_cad_proposta').each(function () {
                                this.reset();
                            });
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            });
<?php } ?>
        $("#table-propostas").on('click', '.btn_ativainativa', function (e) {
            e.preventDefault();

            var c = confirm("Deseja realmente alterar o status o registro?");

            if (c) {
                var id = $(this).closest('tr').attr('id');

                $.ajax({
                    url: '<?php echo base_url() ?>propostas/ativarInativar',
                    type: "POST",
                    data: {
                        id: id,
                        nomemodulo: '<?php echo $idmodulo; ?>'
                    },
                    success: function (data) {

                        data = JSON.parse(data);

                        if (data.erro == false) {
                            msgsuccess("<?php echo ('Cadastro Edição de Propostas') ?>", "<?php echo ('Alterado o Status com sucesso!') ?>");
                        } else {
                            msgerror("<?php echo ('Cadastro Edição de Propostas') ?>", "<?php echo ('Ocorreu algum erro, tente mais tarde') ?>");
                        }

                        $('#form_cad_proposta').each(function () {
                            this.reset();
                        });
                    },
                    error: function (jqXHR, textStatus, errorMessage) {
                        console.log(errorMessage);
                    }
                });
            }
        });
    });
</script>