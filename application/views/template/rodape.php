<div class="row rodape">
    <div class="col-xs-10 col-sm-10 col-md-12 mapa">
        <div class="col-md-4">
            <ol>
                <li><a href="#"><?php echo ("acesso cliente") ?></a></li>
                <li><a href="#"><?php echo ("acesso provedor") ?></a></li>
                <li><a href="#"><?php echo ("quem somos") ?></a></li>
                <li><a href="#"><?php echo ("fale conosco") ?></a></li>
            </ol>
        </div>
        <div class="col-md-3 col-md-offset-5 social">
            <ul>	
                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-flickr" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="col-xs-10 col-sm-10 col-md-12 txtcenter copyright">
        <div><?php echo ('&copy; 2017 ASSINE INTERNET.com.br - Todos os direitos reservados') ?></div>
    </div>
</div>