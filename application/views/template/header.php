<!DOCTYPE html>
<html id="adm-login" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <?php if (isset($tipo)) { ?>
            <title></title>
        <?php } else { ?>
            <title><?php echo (isset($config)) ? $config[0]->CONF_TITLE_SYS . " - " . $title : ("Propostas e Contratos") . " - " . $title; ?></title>
        <?php } ?>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="Sistema de propostas e contratos automáticos">
        <meta name="author" content="Flavio Santos Sousa">

        <?php if (isset($config)) { ?>
            <?php if ($config[0]->CONF_FAVICON_SYS != null) { ?>
                <link rel="icon" href="<?php echo base_url() . $config[0]->CONF_FAVICON_SYS; ?>"/>
            <?php } else { ?>
                <link rel="icon" href="<?php echo base_url() . 'assets/img/favicon.ico'; ?>"/>
            <?php } ?>
        <?php } ?>

        <link href="<?php echo base_url() . 'assets/css/style.default.css' ?>" rel="stylesheet">
        <link href="<?php echo base_url() . 'assets/css/jquery.gritter.css' ?>" rel="stylesheet">

        <!-- CSS -->
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/font-icons/entypo/css/entypo.css' ?>">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/font-icons/font-awesome/css/font-awesome.min.css' ?>">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">

        <?php if (isset($title)) { ?>
            <?php if ($title == "Login" || $title == "Reset Senha") { ?>
                <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/form-elements.css' ?>">
            <?php } ?>
        <?php } ?>

        <link rel="stylesheet" href="<?php echo base_url() . 'assets/datatables/css/dataTables.bootstrap.css' ?>">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/datatables/css/jquery.dataTables.min.css' ?>">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/datatables/responsive/css/datatables.responsive.css' ?>">

        <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/style.css' ?>">

        <link rel="stylesheet" href="<?php echo base_url() . 'assets/datatables/responsive/css/datatables.responsive.css' ?>">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/js/select2/select2-bootstrap.css' ?>">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/js/select2/select2.css' ?>">

        <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/jquery.tagsinput.css' ?>">

        <link type="text/css" rel="stylesheet" href="<?php echo base_url() . 'assets/css/waitMe.css' ?>"/>

        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/tooltipster.bundle.min.css' ?>" />
        <script src="<?php echo base_url() . 'assets/js/jquery/jquery-1.11.1.min.js' ?>"></script>

        <?php if (isset($bootbox)) { ?>
            <script src="<?php echo base_url() . 'assets/js/bootbox.min.js' ?>"></script>
        <?php } ?>

    </head>