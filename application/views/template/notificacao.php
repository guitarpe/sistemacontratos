<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

        <title><?php echo ("Propostas e contratos automáticos | Notificação") ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <style type="text/css">
            p{
                margin-top: 0;
            }

            body{
                padding-top: 50px;
                font-family: arial;
                background: #ffffff;
            }

            .corpo{
            }

            .content {
                margin-top: 20px;
            }
        </style>
    </head>
    <body>
        <div class="corpo">
            <div class="logo"></div>
            <div class="content">
                <?php
                if ($interno) {

                    $chaves = array(
                        'url' => '[valor-url]',
                        'tipo' => '[valor-tipo]',
                        'data' => '[valor-data]',
                        'contrato' => '[valor-contrato]',
                        'cliente' => '[valor-cliente]',
                        'servico' => '[valor-servico]',
                        'informacao' => '[valor-informacao]',
                        'enviado' => '[data-envio]',
                        'usuario' => '[usuario]'
                    );

                    $troca = array(
                        'url' => $urlproposta,
                        'tipo' => $tipodoc,
                        'data' => $datagerado,
                        'contrato' => $contrato,
                        'cliente' => $cliente,
                        'servico' => $servico,
                        'informacao' => $informacao,
                        'enviado' => $dataenvio,
                        'usuario' => $usuario
                    );

                    $layoutemail = str_replace($chaves, $troca, $layout);

                    echo $layoutemail;
                } else {
                    if ($modelo == null) {
                        if ((bool) $padrao) {

                            $chaves = array(
                                'url' => '[valor-url]',
                                'tipo' => '[valor-tipo]',
                                'data' => '[valor-data]',
                                'contrato' => '[valor-contrato]',
                                'cliente' => '[valor-cliente]',
                                'servico' => '[valor-servico]',
                                'informacao' => '[valor-informacao]',
                                'enviado' => '[data-envio]',
                                'usuario' => '[usuario]'
                            );

                            $troca = array(
                                'url' => $urlproposta,
                                'tipo' => $tipodoc,
                                'data' => $datagerado,
                                'contrato' => $contrato,
                                'cliente' => $cliente,
                                'servico' => $servico,
                                'informacao' => $informacao,
                                'enviado' => $dataenvio,
                                'usuario' => $usuario
                            );

                            $layoutemail = str_replace($chaves, $troca, $layout);

                            echo $layoutemail;
                        } else {

                            $chaves = array(
                                'url' => '[valor-url]',
                                'tipo' => '[valor-tipo]',
                                'data' => '[valor-data]',
                                'contrato' => '[valor-contrato]',
                                'cliente' => '[valor-cliente]',
                                'servico' => '[valor-servico]',
                                'informacao' => '[valor-informacao]',
                                'enviado' => '[data-envio]',
                                'usuario' => '[usuario]'
                            );

                            $troca = array(
                                'url' => $urlproposta,
                                'tipo' => $tipodoc,
                                'data' => $datagerado,
                                'contrato' => $contrato,
                                'cliente' => $cliente,
                                'servico' => $servico,
                                'informacao' => $informacao,
                                'enviado' => $dataenvio,
                                'usuario' => $usuario
                            );

                            $layout = str_replace($chaves, $troca, $descricao);

                            echo $layout;
                        }
                    } else {

                        $chaves = array(
                            'url' => '[valor-url]',
                            'tipo' => '[valor-tipo]',
                            'data' => '[valor-data]',
                            'contrato' => '[valor-contrato]',
                            'cliente' => '[valor-cliente]',
                            'servico' => '[valor-servico]',
                            'informacao' => '[valor-informacao]',
                            'enviado' => '[data-envio]',
                            'usuario' => '[usuario]'
                        );

                        $troca = array(
                            'url' => $urlproposta,
                            'tipo' => $tipodoc,
                            'data' => $datagerado,
                            'contrato' => $contrato,
                            'cliente' => $cliente,
                            'servico' => $servico,
                            'informacao' => $informacao,
                            'enviado' => $dataenvio,
                            'usuario' => $usuario
                        );

                        $layout = str_replace($chaves, $troca, $modelo[0]->MDL_LAYOUT);

                        echo $layout;
                    }
                }

                ?>
            </div>
        </div>
    </body>
</html>