<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

        <title><?php echo ("Propostas e contratos automáticos | Notificação") ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <style type="text/css">
            p{
                margin-top: 0;
            }

            body{
                padding-top: 50px;
                font-family: arial;
                font-size: 16px;
                background: #ffffff;
                color: #000;
            }

            .corpo{
                margin: 0 auto;
            }

            .content {
                margin-top: 20px;
            }
        </style>
    </head>
    <body>
        <div class="corpo">
            <div class="logo"></div>
            <div class="content">
                <p><?php echo ("Olá, você esté recebendo uma notificação de " . $infotipo . " no dia <br/>" . $datagerado . ".") ?></p>
                <p><?php echo ("Referente a um contrato aprovado no dia:  " . $dataaprovado); ?></p>
                <p><?php echo ("Clique ") ?><a href="<?php echo $url ?>" target="_blank">aqui</a><?php echo (" para acessar o contrato"); ?></p>
                <p><?php echo ("Email enviado em: " . $dataenvio) ?></p>
            </div>
        </div>
    </body>
</html>