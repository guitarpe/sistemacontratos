<script src="<?php echo base_url() . 'assets/js/jquery.dataTables.min.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/datatables/TableTools.min.js' ?>"></script>

<script src="<?php echo base_url() . 'assets/js/dataTables.bootstrap.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/datatables/jquery.dataTables.columnFilter.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/datatables/lodash.min.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/datatables/responsive/js/datatables.responsive.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/js/select2/select2.min.js' ?>"></script>

<script src="<?php echo base_url() . 'assets/js/jquery/jquery.gritter.min.js' ?>"></script>

<script type="text/javascript" src="<?php echo base_url() . 'assets/js/jquery.tagsinput.js' ?>"></script>

<script src="<?php echo base_url() . 'assets/js/jquery/jquery-migrate-1.2.1.min.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.js' ?>"></script>

<script src="<?php echo base_url() . 'assets/js/jquery/jquery-ui-1.10.3.min.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/js/jquery/jquery-ui.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/js/jquery/jquery-ui-1.10.3.minimal.min.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/js/script.js' ?>"></script>

<script src="<?php echo base_url() . 'assets/js/notify.js' ?>"></script>

<script src="<?php echo base_url() . 'assets/js/bootstrap-wizard.min.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/js/jquery.validate.min.js' ?>"></script>
<script src="<?php echo base_url() . 'assets/js/jquery.inputmask.bundle.min.js' ?>"></script>

<script src="<?php echo base_url() . 'assets/js/jquery.maskMoney.js' ?>" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url() . 'assets/js/tooltipster.bundle.min.js' ?>"></script>

<?php if ($title != 'Visualizar Proposta' && $title != 'Visualizar Contrato') { ?>
    <script src="<?php echo base_url() . 'assets/js/tinymce/tinymce.min.js' ?>"></script>
<?php } ?>

<script>$.noConflict();</script>

<?php if ($title != 'Visualizar Proposta' && $title != 'Visualizar Contrato') { ?>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            //DEFINE O TIPO DO EDITOR
            tinymce.init({
                selector: '.editor',
                language: 'pt_BR',
                theme: 'modern',
                editor_selector: "htmlField",
                code_dialog_width: 1000,
                plugin_preview_height: 650,
                plugin_preview_width: 1000,
                paste_data_images: true,
                automatic_uploads: false,
                relative_urls: false,
                remove_script_host: false,
                image_advtab: true,
                document_base_url: "<?php echo base_url(); ?>",
                imagetools_cors_hosts: ['www.tinymce.com', 'codepen.io'],
                plugins: [
                    "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking powerpaste",
                    "table contextmenu directionality emoticons template textcolor fullpage textcolor colorpicker responsivefilemanager textpattern"
                ],
                toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor | table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | visualchars visualblocks nonbreaking template pagebreak restoredraft",
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css'
                ],
                menubar: true,
                toolbar_items_size: 'small',
                fullpage_default_doctype: "<!DOCTYPE html>",
                external_filemanager_path: "<?php echo base_url() . "filemanager/" ?>",
                filemanager_title: "Gerenciador de arquivos",
                external_plugins: {"filemanager": "<?php echo base_url() . "filemanager/plugin.min.js" ?>"},
                powerpaste_allow_local_images: true,
                powerpaste_word_import: 'prompt',
                powerpaste_html_import: 'prompt',
                style_formats: [{
                        title: 'Bold text',
                        inline: 'b'
                    }, {
                        title: 'Red text',
                        inline: 'span',
                        styles: {
                            color: '#ff0000'
                        }
                    }, {
                        title: 'Red header',
                        block: 'h1',
                        styles: {
                            color: '#ff0000'
                        }
                    }, {
                        title: 'Example 1',
                        inline: 'span',
                        classes: 'example1'
                    }, {
                        title: 'Example 2',
                        inline: 'span',
                        classes: 'example2'
                    }, {
                        title: 'Table styles'
                    }, {
                        title: 'Table row 1',
                        selector: 'tr',
                        classes: 'tablerow1'
                    }],

                templates: [{
                        title: 'Test template 1',
                        content: 'Test 1'
                    }, {
                        title: 'Test template 2',
                        content: 'Test 2'
                    }],

                init_instance_callback: function () {
                    window.setTimeout(function () {
                        $("#div").show();
                    }, 1000);
                }
            });

            $(document).on('focusin', function (event) {
                if ($(event.target).closest(".mce-window").length) {
                    event.stopImmediatePropagation();
                }
            });
        });
    </script>
<?php } ?>

<script type="text/javascript">
    function msgsuccess(tit, info) {
        jQuery.gritter.add({
            title: tit,
            text: info,
            class_name: 'growl-success',
            image: '<?php echo base_url() . "assets/img/screen.png" ?>',
            sticky: false,
            time: '2000',
            after_close: function () {
                location.reload();
            }
        });
    }

    function msgerror(tit, info) {
        jQuery.gritter.add({
            title: tit,
            text: info,
            class_name: 'growl-danger',
            image: '<?php echo base_url() . "assets/img/screen.png" ?>',
            sticky: false,
            time: '2000',
            after_close: function () {
                //location.reload();
            }
        });
    }
</script>

<script src="<?php echo base_url() . 'assets/js/waitMe.js' ?>"></script>
</body>
</html>