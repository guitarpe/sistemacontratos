<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$this->load->view('template/header');

?>
<body>
    <!-- Top content -->
    <div class="top-content">
        <div class="inner-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4 txtcenter">
                        <h1>
                            <strong><?php echo $config[0]->CONF_TITLE_SYS ?> </strong>
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div <?php echo $formtype == 1 ? 'style="display:none"' : 'style="display:block"' ?> class="col-sm-4 col-sm-offset-4 form-box form_login_reset">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Reenviar senha</h3>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-lock"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <div class="erro_form">
                                <?= validation_errors(); ?>
                            </div>
                            <?=
                            form_open("login/reenviar", array(
                                "role" => "form",
                                "class" => "login-form"
                                )
                            );

                            ?>
                            <div class="form-group">
                                <h4><?php echo ("Informe o e-mail realizado no cadastro") ?></h4>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-username"><?php echo ("E-mail"); ?></label>
                                <input type="text" name="email" placeholder="<?php echo ("E-mail"); ?>" class="form-username form-control" id="form-username">
                            </div>
                            <button type="submit" class="btn">Reenviar</button>
                            <?= form_close(); ?>
                            <div>
                                <a href="#" class="btn_login_reenvio">Acessar Sistema</a>
                            </div>
                        </div>
                    </div>
                    <div <?php echo $formtype == 2 ? 'style="display:none"' : 'style="display:block"' ?> class="col-sm-4 col-sm-offset-4 form-box form_login_reset">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Login - <?= $profile; ?></h3>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-lock"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <div class="erro_form">
                                <?= validation_errors(); ?>
                            </div>
                            <?=
                            form_open("login/logar", array(
                                "role" => "form",
                                "class" => "login-form"
                                )
                            );

                            if (isset($enviado)) {
                                if ($enviado) {

                                    ?>
                                    <div class="form-group text-reenvio">
                                        <h4><?php echo ("Você receberá por e-mail um link para a redefinição da senha.") ?></h4>
                                    </div>
                                    <?php
                                }
                            }

                            if (isset($resetado)) {
                                if ($resetado) {

                                    ?>
                                    <div class="form-group text-reenvio">
                                        <h4><?php echo ("Parabéns, você já pode realizar o login!") ?></h4>
                                    </div>
                                    <?php
                                } else {

                                    ?>
                                    <div class="form-group text-reenvio">
                                        <h4><?php echo ("Ocorreu algum erro, tente novamente mais tarde.") ?></h4>
                                    </div>
                                    <?php
                                }
                            }

                            ?>
                            <div class="form-group">
                                <label class="sr-only" for="form-username"><?php echo ("Login"); ?></label>
                                <input type="text" name="login" placeholder="<?php echo ("Login"); ?>" class="form-username form-control" id="form-username">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-password">Senha</label>
                                <input type="password" id="senha" name="senha" placeholder="Senha" class="form-password form-control" id="form-password" maxlength="8"/>
                            </div>
                            <button type="submit" class="btn">Entrar</button>
                            <?= form_close(); ?>
                            <div>
                                <a href="#" class="btn_login_reenvio">Reenviar senha</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url() . 'assets/js/jquery.backstretch.min.js' ?>"></script>

    <script>
        jQuery(document).ready(function ($) {

            $.backstretch("<?php echo base_url() . 'assets/img/backgrounds/3.jpg'; ?>");

            $(".btn_login_reenvio").on('click', function (e) {
                e.preventDefault();

                $(".erro_form").html("");
                $(".form_login_reset").slideToggle("slow");
            });

            $("#senha").on('click', function (e) {
                e.preventDefault();

                $(this).notify(
                        '<?php echo ("A senha deve conter 8 caracteres") ?>',
                        {
                            position: 'right',
                            autoHide: true,
                            autoHideDelay: 3000,
                            className: "info"
                        }
                );
            });
        });
    </script>
    <?php $this->load->view('template/footer'); ?>