<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
$this->load->view('template/header');

?>
<body>
    <!-- Top content -->
    <div class="top-content">
        <div class="inner-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4 txtcenter">
                        <h1>
                            <strong><?php echo $config[0]->CONF_TITLE_SYS ?> </strong>
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4 form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Reset de Senha</h3>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-lock"></i>
                            </div>
                        </div>
                        <div class="form-bottom">                        	
                            <div class="erro_form"> <?php echo validation_errors(); ?></div>
                            <?php
                            echo form_open("resetsenha/resetarsenha", array("role" => "form", "class" => "login-form"));

                            ?>
                            <div class="form-group">
                                <input type="hidden" id="iduser" name="iduser" value="<?php echo $id; ?>" /> 
                                <label class="sr-only" for="form-password">Senha</label> 
                                <input type="password" name="senha" id="senha" placeholder="Senha" class="form-password form-control senha" maxlength="8">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-password">Confirmar Senha</label>
                                <input type="password" name="confsenha" id="confsenha" placeholder="Confirmar Senha" class="form-password form-control senha" maxlength="8">
                            </div>
                            <button type="submit" class="btn">Salvar Senha</button>
<?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url() . 'assets/js/jquery.backstretch.min.js' ?>"></script>

    <script>
        jQuery(document).ready(function ($) {

            $.backstretch("<?php echo base_url() . 'assets/img/backgrounds/3.jpg'; ?>");

            $(".senha").on('click', function (e) {
                e.preventDefault();

                $(this).notify(
                        '<?php echo ("A senha deve conter 8 caracteres") ?>',
                        {
                            position: 'right',
                            autoHide: true,
                            autoHideDelay: 3000,
                            className: "info"
                        }
                );
            });
        });
    </script>	
<?php $this->load->view('template/footer'); ?>