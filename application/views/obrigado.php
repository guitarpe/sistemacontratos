<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
$this->load->view('template/header');
?>
<body>
    <!-- Top content -->
    <div class="top-content">
        <div class="inner-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4 txtcenter">
                        <h1><strong><?php echo "Proposta/Contrato Reprovada!" ?></strong></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2><?php echo "Entre em contato conosco!" ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url() . 'assets/js/jquery.backstretch.min.js' ?>"></script>

    <script>
        jQuery(document).ready(function ($) {

            $.backstretch("<?php echo base_url() . 'assets/img/backgrounds/3.jpg'; ?>");
        });
    </script>
<?php $this->load->view('template/footer'); ?>