<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');



$this->load->view('template/header');
header("Content-type:application/pdf");

?>


<body>

    <div class="page">

        <?php
        echo $tipo == 'c' ? $infodoc->CTT_LAYOUT : $infodoc->PRP_LAYOUT;

        ?>

    </div>

</body>

</html>