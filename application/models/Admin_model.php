﻿<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Comum.php';

class Admin_model extends CI_Model
{

    protected $tabelas;

    function __construct()
    {
        parent::__construct();
        $this->tabelas = new Comum();
    }

    function getDataModulo($modulo, $token)
    {

        $user = $this->infoUser($token);

        $data = array(
            'idmodulo' => $modulo,
            'list' => $this->listRegistros($modulo, $token),
            'listperfis' => $this->listaPerfis($modulo, $token),
            'listamodelos' => $this->listaModelos($modulo, 1, $token),
            'listamodeloscontratos' => $this->listaModelos($modulo, 2, $token),
            'listamodelosemail' => $this->listaModelos($modulo, 3, $token),
            'listaservicos' => $this->listaServicos($modulo, $token),
            'config' => $this->infoSistema($modulo),
            'listaestados' => $this->listaEstados($modulo),
            'user' => $user,
            'metodostela' => $this,
            'permissoes' => explode(', ', $user->permissoes),
            'infohome' => $this->infoHome($modulo, $token),
            'teste'
        );

        return $data;
    }

    function verificarPermissoes($permissoes, $valores)
    {
        foreach ($permissoes as $v1) {
            foreach ($valores as $v2) {
                if ($v1 == $v2) {
                    return true;
                    break;
                }
            }
        }

        return false;
    }

    function infoHome($modulo, $token)
    {

        $tbmodulos = array('home');

        if (in_array($modulo, $tbmodulos)) {

            $data = array();

            /* PROPOSTAS */
            $this->db->select('*');
            $this->db->from($this->tabelas->tb_propostas);

            $queryprp = $this->db->get();
            $allpropostas = $queryprp->result();

            $data['allpropostas'] = $allpropostas;

            //propostas geradas
            $this->db->select('count(PRP_ID) as prop_geradas');
            $this->db->from($this->tabelas->tb_propostas);

            $queryprpg = $this->db->get();
            $propgeradas = $queryprpg->row();

            $data['prop_geradas'] = $propgeradas->prop_geradas;

            //propostas enviadas
            $this->db->select('count(PRP_ID) as prop_enviadas');
            $this->db->from($this->tabelas->tb_propostas);
            $this->db->where('PRP_SEND_MAIL_CLIENTE', 1);
            $this->db->where('PRP_STATUS_VIEW', 'S');

            $queryprpe = $this->db->get();
            $propenviadas = $queryprpe->row();

            $data['prop_enviadas'] = $propenviadas->prop_enviadas;

            //propostas aprovadas
            $this->db->select('count(PRP_ID) as prop_aprovadas');
            $this->db->from($this->tabelas->tb_propostas);
            $this->db->where('PRP_STATUS', 4);
            $this->db->where('PRP_STATUS_VIEW', 'S');

            $queryprpp = $this->db->get();
            $proppendentes = $queryprpp->row();

            $data['prop_aprovadas'] = $proppendentes->prop_aprovadas;

            /* CONTRATOS */
            //contratos gerados
            $this->db->select('*');
            $this->db->from($this->tabelas->tb_contratos);

            $queryctt = $this->db->get();
            $allcontratos = $queryctt->row();

            $data['allcontratos'] = $allcontratos;

            $this->db->select('count(CTT_ID) ctt_geradas');
            $this->db->from($this->tabelas->tb_contratos);

            $querycttg = $this->db->get();
            $cttgeradas = $querycttg->row();

            $data['ctt_geradas'] = $cttgeradas->ctt_geradas;

            //contratos enviados
            $this->db->select('count(CTT_ID) as ctt_enviadas');
            $this->db->from($this->tabelas->tb_contratos);
            $this->db->where('CTT_SEND_MAIL_CLIENTE', 1);
            $this->db->where('CTT_STATUS_VIEW', 'S');

            $queryctte = $this->db->get();
            $cttenviadas = $queryctte->row();

            $data['ctt_enviadas'] = $cttenviadas->ctt_enviadas;

            //contratos assinados
            $this->db->select('count(CTT_ID) as ctt_assinados');
            $this->db->from($this->tabelas->tb_contratos);
            $this->db->where('CTT_STATUS', 3);
            $this->db->where('CTT_STATUS_VIEW', 'S');

            $query = $this->db->get();
            $cttpendentes = $query->row();

            $data['ctt_assinados'] = $cttpendentes->ctt_assinados;

            return $data;
        } else {
            return null;
        }
    }

    function listaEstados($modulo)
    {
        $tbmodulos = array('propostas', 'contratos');

        if (in_array($modulo, $tbmodulos)) {
            $this->db->select('id_estado as id, est_nome as sigla');
            $this->db->from($this->tabelas->tb_estados);

            $query = $this->db->get();
            return $query->result();
        }
    }

    function getUF($id)
    {

        if ($id != null || $id != 0) {
            $this->db->select('id_estado as id, est_nome as sigla');
            $this->db->from($this->tabelas->tb_estados);
            $this->db->where('id_estado', $id);

            $query = $this->db->get();

            return $query->row()->sigla;
        } else {

            return null;
        }
    }

    function listaPerfis($modulo, $token)
    {
        if ($this->validaToken($token)) {
            $tbmodulos = array('usuarios', 'perfis', 'profile', 'configuracoes');

            if (in_array($modulo, $tbmodulos)) {
                $this->db->select('*');
                $this->db->from($this->tabelas->tb_tipos_usuarios);
                $this->db->order_by("TYPE_ID", "desc");

                $query = $this->db->get();

                return $query->result();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    function listaModelos($modulo, $tipo, $token)
    {
        $tbmodulos = array('propostas', 'contratos');

        if ($this->validaToken($token)) {
            if ((in_array($modulo, $tbmodulos))) {
                $this->db->select('*');
                $this->db->from($this->tabelas->tb_modelos);
                $this->db->where('MDL_TYPE', $tipo);
                $this->db->order_by("MDL_ID", "desc");

                $query = $this->db->get();

                return $query->result();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    function listaServicos($modulo, $token)
    {

        $user = $this->infoUser($token);

        $permissoes = explode(',', $user->permissoes);

        $tbmodulos = array('propostas', 'configuracoes', 'contratos');

        if (in_array(13, $permissoes) || in_array(14, $permissoes) || in_array(15, $permissoes)) {
            if (in_array($modulo, $tbmodulos)) {
                $this->db->select('*');
                $this->db->from($this->tabelas->tb_servicos);
                $this->db->order_by("SERV_ID", "desc");

                $query = $this->db->get();

                return $query->result();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    function listRegistros($modulo, $token)
    {

        $user = $this->infoUser($token);

        $permissoes = explode(',', $user->permissoes);

        $tbmodulos = array('usuarios', 'perfis', 'contratos', 'modelos', 'propostas');

        if (in_array($modulo, $tbmodulos) && $modulo == "usuarios") {

            if (in_array(1, $permissoes) || in_array(2, $permissoes) || in_array(3, $permissoes)) {
                $this->db->select('*');
                $this->db->from($this->tabelas->tb_usuario . " us");
                $this->db->join($this->tabelas->tb_tipos_usuarios . " typ", 'typ.TYPE_ID = us.TYPE_ID');

                $this->db->order_by("US_ID", "desc");

                $query = $this->db->get();

                return $query->result();
            } else {
                return null;
            }
        }

        if (in_array($modulo, $tbmodulos) && $modulo == "perfis") {

            if (in_array(4, $permissoes) || in_array(5, $permissoes) || in_array(6, $permissoes)) {
                $this->db->select('*');
                $this->db->from($this->tabelas->tb_tipos_usuarios);
                $this->db->order_by("TYPE_ID", "desc");

                $query = $this->db->get();

                return $query->result();
            } else {
                return null;
            }
        }

        if (in_array($modulo, $tbmodulos) && $modulo == "contratos") {

            if ($user->tipo == $this->config()->CONF_ADM_PERFIL) {
                if (in_array(16, $permissoes) || in_array(17, $permissoes) || in_array(18, $permissoes)) {
                    $this->db->select('*');
                    $this->db->from($this->tabelas->tb_contratos);
                    $this->db->order_by("CTT_ID", "desc");

                    $query = $this->db->get();

                    return $query->result();
                } else {
                    return null;
                }
            } else {
                if (in_array(16, $permissoes) || in_array(17, $permissoes) || in_array(18, $permissoes)) {
                    $this->db->select('*');
                    $this->db->from($this->tabelas->tb_contratos);
                    $this->db->where('CTT_USER_CREATED', $user->id);
                    $this->db->order_by("CTT_ID", "desc");

                    $query = $this->db->get();

                    return $query->result();
                } else {
                    return null;
                }
            }
        }

        if (in_array($modulo, $tbmodulos) && $modulo == "modelos") {

            if (in_array(7, $permissoes) || in_array(8, $permissoes) || in_array(9, $permissoes)) {
                $this->db->select('*');
                $this->db->from($this->tabelas->tb_modelos);
                $this->db->order_by("MDL_ID", "desc");

                $query = $this->db->get();

                return $query->result();
            } else {
                return null;
            }
        }

        if (in_array($modulo, $tbmodulos) && $modulo == "propostas") {

            if ($user->tipo == $this->config()->CONF_ADM_PERFIL) {
                if (in_array(10, $permissoes) || in_array(11, $permissoes) || in_array(12, $permissoes)) {
                    $this->db->select('*');
                    $this->db->from($this->tabelas->tb_propostas);
                    $this->db->order_by("PRP_ID", "desc");

                    $query = $this->db->get();

                    return $query->result();
                } else {
                    return null;
                }
            } else {
                if (in_array(10, $permissoes) || in_array(11, $permissoes) || in_array(12, $permissoes)) {
                    $this->db->select('*');
                    $this->db->from($this->tabelas->tb_propostas);
                    $this->db->where('PRP_USER_CREATED', $user->id);
                    $this->db->order_by("PRP_ID", "desc");

                    $query = $this->db->get();

                    return $query->result();
                } else {
                    return null;
                }
            }
        }
    }
    /* PROPOSTAS HOME */

    var $column_order_ph = array('PRP_ID', 'PRP_DESC', 'PRP_PERCENTUAL', 'STATUS');
    var $column_search_ph = array('PRP_ID', 'PRP_DESC', 'PRP_PERCENTUAL', 'cst.DESCRICAO');
    var $order_ph = array('PRP_ID' => 'desc');

    private function _getpropostashome($per, $user)
    {

        $this->db->select("
				prop.PRP_ID as PRP_ID,
				prop.PRP_DESC as PRP_DESC,
				prop.PRP_PERCENTUAL as PRP_PERCENTUAL,
				cst.DESCRICAO as STATUS
		");
        $this->db->from($this->tabelas->tb_propostas . " prop");
        $this->db->join($this->tabelas->tb_config_status . " cst", 'cst.ID = prop.PRP_STATUS', 'left');
        if (in_array(20, $per)) {
            $this->db->where('PRP_USER_CREATED', $user->id);
        }

        $i = 0;

        foreach ($this->column_search_ph as $item) {
            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search_ph) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order_ph[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_ph)) {
            $order = $this->order_ph;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function getpropostashome($token)
    {
        $user = $this->infoUser($token);

        $permissoes = explode(', ', $user->permissoes);

        $this->_getpropostashome($permissoes, $user);

        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    function filtropropostashome($token)
    {
        $user = $this->infoUser($token);
        $permissoes = explode(', ', $user->permissoes);

        $this->_getpropostashome($permissoes, $user);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function todospropostashome($token)
    {
        //$user = $this->infoUser($token);
        //$permissoes = explode(', ', $user->permissoes);

        $this->db->from($this->tabelas->tb_servicos);
        return $this->db->count_all_results();
    }
    /* PROPOSTAS HOME */

    /* CONTRATOS HOME */

    var $column_order_ch = array('CTT_ID', 'CTT_DESC', 'CTT_PERCENTUAL', 'STATUS');
    var $column_search_ch = array('CTT_ID', 'CTT_DESC', 'CTT_PERCENTUAL', 'stt.DESCRICAO');
    var $order_ch = array('CTT_ID' => 'desc');

    private function _getcontratoshome($per, $user)
    {

        $this->db->select("
				ctt.CTT_ID as CTT_ID,
				ctt.CTT_DESC as CTT_DESC,
				ctt.CTT_PERCENTUAL as CTT_PERCENTUAL,
				stt.DESCRICAO as STATUS");
        $this->db->from($this->tabelas->tb_contratos . " ctt");
        $this->db->join($this->tabelas->tb_config_ct_status . " stt", 'stt.ID = ctt.CTT_STATUS', 'left');

        if (in_array(20, $per)) {
            $this->db->where('CTT_USER_CREATED', $user->id);
        }

        $i = 0;

        foreach ($this->column_search_ch as $item) {
            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search_ch) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order_ch[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_ch)) {
            $order = $this->order_ch;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function getcontratoshome($token)
    {
        $user = $this->infoUser($token);
        $permissoes = explode(', ', $user->permissoes);

        $this->_getcontratoshome($permissoes, $user);

        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    function filtrocontratoshome($token)
    {
        $user = $this->infoUser($token);
        $permissoes = explode(', ', $user->permissoes);

        $this->_getcontratoshome($permissoes, $user);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function todoscontratoshome($token)
    {
        //$user = $this->infoUser($token);
        //$permissoes = explode(', ', $user->permissoes);

        $this->db->from($this->tabelas->tb_servicos);
        return $this->db->count_all_results();
    }
    /* CONTRATOS HOME */

    function infoUser($token)
    {
        if ($this->validaToken($token)) {
            $this->db->select('*');
            $this->db->from($this->tabelas->tb_acesso);
            $this->db->where('AC_TOKEN', $token);

            $query = $this->db->get();

            $result = $query->row();

            if (!empty($result)) {

                $this->db->select('
						us.US_ID as id,
					us.USER_NAME_FULL as nome_completo,
						us.US_MAIL as email,
						us.US_LOGIN as login,
						us.US_STATUS as status,
						us.TYPE_ID as tipo,
						tp.TYPE_PERMISSIONS as permissoes,
                        us.US_TEXT_REMETENTE as textoremetente,
                        us.US_SMTP_HOST as smtphost,
                        us.US_SMTP_PORT as smtpporta,
                        us.US_SMTP_USER as smtpusuario,
                        us.US_SMTP_PASS as smtpsenha');
                $this->db->from($this->tabelas->tb_acesso . " acc");
                $this->db->join($this->tabelas->tb_usuario . " us", 'us.US_ID = acc.US_ID');
                $this->db->join($this->tabelas->tb_tipos_usuarios . " tp", 'tp.TYPE_ID = us.TYPE_ID');
                $this->db->where('acc.AC_TOKEN', $token);
                $this->db->limit(1);

                $query = $this->db->get();
                $array = $query->row();

                return $array;
            }
        } else {
            return null;
        }
    }

    function dadosEmailUsuario($id, $tipo)
    {
        $this->db->select('us.US_ID as id,
					us.US_MAIL as email,
					us.US_TEXT_REMETENTE as remetente,
                    us.US_SMTP_HOST as smtphost,
                    us.US_SMTP_PORT as smtpporta,
                    us.US_SMTP_USER as smtpusuario,
                    us.US_SMTP_PASS as smtpsenha');
        $this->db->from($this->tabelas->tb_usuario . " us");
        if ($tipo == 1) {
            $this->db->join($this->tabelas->tb_propostas . " prp", 'prp.PRP_USER_CREATED = us.US_ID');
            $this->db->where('prp.PRP_ID', $id);
        } else {
            $this->db->join($this->tabelas->tb_contratos . " ctt", 'ctt.CTT_USER_CREATED = us.US_ID');
            $this->db->where('ctt.CTT_ID', $id);
        }
        $this->db->limit(1);

        $queryus = $this->db->get();

        $usuario = $queryus->row();

        //config
        $this->db->select("
            US_SMTP_USER as email,
            'Propostas e Contratos Automáticos - Notificação' as remetente,
            US_SMTP_HOST as smtphost,
            US_SMTP_PORT as smtpporta,
            US_SMTP_USER as smtpusuario,
            US_SMTP_PASS as smtpsenha
        ");
        $this->db->from($this->tabelas->tb_config);
        $this->db->limit(1);

        $querycf = $this->db->get();

        $config = $querycf->row();

        if (!empty($usuario->remetente)) {
            return $usuario;
        } else {
            return $config;
        }
    }

    function infoSistema($modulo)
    {
        $tbmodulos = array('configuracoes', 'propostas', 'contratos', 'home');

        if ((in_array($modulo, $tbmodulos))) {
            $this->db->select('*');
            $this->db->from($this->tabelas->tb_config);
            $this->db->limit(1);

            $query = $this->db->get();

            return $query->result();
        } else {
            return null;
        }
    }

    function get($modulo, $id)
    {
        $tbmodulos = array('propostas', 'servicos', 'contratos');

        if (in_array($modulo, $tbmodulos) && $modulo == "propostas") {
            $this->db->select('PRP_ID as id,
					PRP_DESC as descricao,
					MDL_ID as modelo,
					CLI_CONTATO_NOME as nome_contato,
					CLI_EMAIL_CONTATO as email_contato,
					PRP_EMAIL_DEFAULT as email_padrao,
					PRP_SEND_MAIL as email,
					PRP_URL_DEFAULT as url_padrao,
					PRP_URL_INFO as urlproposta,
					SERV_ID as servico,
					SERV_DESC as outro_servicos,
				CTT_ID as modelo_contrato,
					PRP_VALOR_TOTAL as valor_total,
					PRP_VALOR_PARCELADO as valor_parcelado,
					PRP_INTERVALO_PARCELAS as intervalo_parcelas,
                    CTT_CONVENIOS as convenios,
					PRP_PARCELAS as parcelas,
                    PRP_QTDE_FARMACIAS as qde_farmacias,
					PRP_DT_CREATED as data_gerado');

            $this->db->from($this->tabelas->tb_propostas);
            $this->db->where('PRP_ID', $id);
            $this->db->limit(1);

            $query = $this->db->get();

            return $query->result();
        }

        if (in_array($modulo, $tbmodulos) && $modulo == "servicos") {
            $this->db->select('SERV_ID as id,
					SERV_DESC as nome,
					SERV_INFO as descricao');
            $this->db->from($this->tabelas->tb_servicos);
            $this->db->where('SERV_ID', $id);
            $this->db->limit(1);

            $query = $this->db->get();

            return $query->result();
        }

        if (in_array($modulo, $tbmodulos) && $modulo == "contratos") {
            $this->db->select('CTT_ID as id,
					MDL_ID as modelo_contrato,
					CTT_STATUS as status,
					CTT_DESC as descricao,
					CTT_SEND_MAIL as email_notificacao,
					CTT_EMAIL_DEFAULT as email_padrao,
					CTT_URL_INFO as urlcontrato,
					CTT_URL_DEFAULT as url_padrao,
					CTT_SERVICO as outro_servicos,
					CTT_SERVICO_ID as servico,
					CTT_TAGS as tags,
					CLI_RAZAO_SOCIAL as razao_social,
					CLI_NOME_FANTASIA as nome_fantasia,
					CLI_CNPJ as cnpj,
					CLI_LOGRADOURO_EMPRESA as logradouro,
					CLI_NUMERO_EMPRESA as numero,
					CLI_COMPLEMENTO_EMPRESA as complemento,
					CLI_BAIRRO_EMPRESA as bairro,
					CLI_UF_EMPRESA as estado,
					CLI_CIDADE_EMPRESA as cidade,
					CLI_CEP_EMPRESA as cep,
					CLI_CONTATO_NOME as nome_contato,
					CLI_CONTATO_CARGO as cargo_contato,
					CLI_RG_CONTATO as rg_contato,
					CLI_CPF_CONTATO as cpf_contato,
					CLI_EMAIL_CONTATO as email_contato,
                    CLI_LOGRADOURO_CONTATO as logradouro_contato,
                    CLI_NUMERO_CONTATO as numero_contato,
					CLI_COMPLEMENTO_CONTATO as complemento_contato,
					CLI_BAIRRO_CONTATO as bairro_contato,
					CLI_UF_CONTATO as estado_contato,
					CLI_CIDADE_CONTATO as cidade_contato,
					CLI_CEP_CONTATO as cep_contato,
					CTT_VALOR_TOTAL as valor_total,
					CTT_VALOR_PARCELADO as valor_parcelado,
					CTT_PARCELAS as parcelas,
					CTT_CONVENIOS as convenios,
					CTT_OUTROS_CNPJS as outros_cnpjs,
					CTT_QTDE_VENDAS as qtde_vendas,
					CTT_VENDEDOR as vendedor,
					CTT_DATA_CONTRATO as data_contrato,
					CTT_DT_CREATE as data_gerado,
					CTT_LAYOUT as texto,
					CTT_AUTO as automatico,
                    CTT_IP_CLIENTE as ip_cliente,
                    CTT_DT_APROV as data_aprovacao,
                    CTT_STATUS_VIEW as status_view');
            $this->db->from($this->tabelas->tb_contratos);
            $this->db->where('CTT_ID', $id);
            $this->db->limit(1);

            $query = $this->db->get();

            return $query->result();
        }
    }

    function config()
    {
        $this->db->select('*');
        $this->db->from($this->tabelas->tb_config);
        $this->db->limit(1);

        $query = $this->db->get();

        return $query->row();
    }

    function insereContratoAutomatico($modulo, $data)
    {
        $this->db->insert($this->tabelas->tb_contratos, $data);

        //adiciona as informações no layout
        $contrato_id = $this->db->insert_id();
        $modelo_id = $data['MDL_ID'];

        $this->gerarLayoutContrato($modelo_id, $contrato_id);

        return $contrato_id;
    }

    function editarContrato($modulo, $data, $id)
    {

        $contrato = $this->admin_model->getContratoId($id);

        if ($this->upddata($data, $this->tabelas->tb_contratos, $id, 'CTT_ID')) {

            $modelo_id = $contrato[0]->MDL_ID;

            $this->gerarLayoutContrato($modelo_id, $id);

            return $id;
        } else {
            return null;
        }
    }

    function getProposta($id)
    {
        $this->db->select('PRP_ID as id,
					PRP_DESC as descricao,
					MDL_ID as modelo,
					CLI_CONTATO_NOME as nome_contato,
					CLI_EMAIL_CONTATO as email_contato,
					PRP_EMAIL_DEFAULT as email_padrao,
					PRP_SEND_MAIL as email_notificacao,
					PRP_URL_DEFAULT as urlpadrao,
					PRP_URL_INFO as urlproposta,
					USER_NAME_FULL as vendedor,
					SERV_ID as servico,
					SERV_DESC as outro_servicos,
					CTT_ID as modelo_contrato,
					PRP_VALOR_TOTAL as valor_total,
					PRP_VALOR_PARCELADO as valor_parcelado,
					PRP_QTDE_FARMACIAS as qde_farmacias,
					PRP_PARCELAS as parcelas,
					PRP_DT_CREATED as data_gerado');
        $this->db->from($this->tabelas->tb_propostas . " prop");
        $this->db->join($this->tabelas->tb_usuario . " us", 'us.US_ID = prop.PRP_USER_CREATED');
        $this->db->where('PRP_ID', $id);
        $this->db->limit(1);

        $query = $this->db->get();

        return $query->row();
    }

    function getContrato($id)
    {
        $this->db->select('CTT_ID as id,
					MDL_ID as modelo_contrato,
					CTT_STATUS as status,
					CTT_DESC as descricao,
					CTT_SEND_MAIL as email_notificacao,
					CTT_EMAIL_DEFAULT as email_padrao,
					CTT_URL_INFO as urlcontrato,
					CTT_URL_DEFAULT as url_padrao,
					CTT_SERVICO as outro_servicos,
					CTT_SERVICO_ID as servico,
                    CTT_TAGS as tags,
					CLI_RAZAO_SOCIAL as razao_social,
					CLI_NOME_FANTASIA as nome_fantasia,
					CLI_CNPJ as cnpj,
					CLI_LOGRADOURO_EMPRESA as logradouro,
					CLI_NUMERO_EMPRESA as numero,
					CLI_COMPLEMENTO_EMPRESA as complemento,
					CLI_BAIRRO_EMPRESA as bairro,
                    CLI_UF_EMPRESA as estado,
					CLI_CIDADE_EMPRESA as cidade,
					CLI_CEP_EMPRESA as cep,
					CLI_CONTATO_NOME as nome_contato,
					CLI_CONTATO_CARGO as cargo_contato,
					CLI_RG_CONTATO as rg_contato,
					CLI_CPF_CONTATO as cpf_contato,
                    CLI_EMAIL_CONTATO as email_contato,
					CLI_LOGRADOURO_CONTATO as logradouro_contato,
					CLI_NUMERO_CONTATO as numero_contato,
					CLI_COMPLEMENTO_CONTATO as complemento_contato,
                    CLI_BAIRRO_CONTATO as bairro_contato,
					CLI_UF_CONTATO as estado_contato,
                    CLI_CIDADE_CONTATO as cidade_contato,
					CLI_CEP_CONTATO as cep_contato,
					CTT_VALOR_TOTAL as valor_total,
					CTT_VALOR_PARCELADO as valor_parcelado,
					CTT_PARCELAS as parcelas,
					CTT_VENCIMENTOS as vencimentos,
					CTT_CONVENIOS as convenios,
					CTT_OUTROS_CNPJS as outros_cnpjs,
					CTT_QTDE_VENDAS as qtde_vendas,
					CTT_VENDEDOR as vendedor,
					CTT_DATA_CONTRATO as data_contrato,
					CTT_DT_CREATE as data_gerado,
					CTT_LAYOUT as texto,
					CTT_AUTO as automatico');
        $this->db->from($this->tabelas->tb_contratos);
        $this->db->where('CTT_ID', $id);
        $this->db->limit(1);

        $query = $this->db->get();

        return $query->row();
    }

    function upddata($data, $table, $id, $field)
    {
        $this->db->where($field, $id);
        $this->db->update($table, $data);

        return true;
    }

    function atualizaStatus($value, $table, $id)
    {

        $percent = 0;

        switch ($value) {
            case 1:
                $percent = 25;
                break;
            case 2:
                $percent = 50;
                break;
            case 3:
                $percent = 75;
                break;
            case 4:
                $percent = 100;
                break;
            default:
                $percent = 0;
        }

        if ($table == "propostas") {

            $proposta = array(
                'PRP_STATUS' => $value,
                'PRP_PERCENTUAL' => $percent
            );

            return $this->upddata($proposta, $this->tabelas->tb_propostas, $id, 'PRP_ID');
        }

        if ($table == "contratos") {

            $proposta = array(
                'CTT_STATUS' => $value,
                'CTT_PERCENTUAL' => $percent
            );

            return $this->upddata($proposta, $this->tabelas->tb_contratos, $id, 'CTT_ID');
        }
    }

    function getLayoutProposta($id)
    {
        $this->db->select('*');
        $this->db->from($this->tabelas->tb_propostas);
        $this->db->where('PRP_ID', $id);
        $this->db->limit(1);

        $query = $this->db->get();

        $proposta = $query->row("PRP_LAYOUT");
    }

    function getLayoutContrato($id)
    {
        $this->db->select('*');
        $this->db->from($this->tabelas->tb_contratos);
        $this->db->where('CTT_ID', $id);
        $this->db->limit(1);

        $query = $this->db->get();

        $proposta = $query->row("CTT_LAYOUT");
    }

    function gerarLayoutProposta($modulo_id, $proposta_id)
    {

        //busca o modelo
        $this->db->select('*');
        $this->db->from($this->tabelas->tb_modelos);
        $this->db->where('MDL_ID', $modulo_id);
        $this->db->limit(1);

        $querymdl = $this->db->get();

        $layout_modelo = $querymdl->row()->MDL_LAYOUT;

        //busca a proposta
        $this->db->select('*');
        $this->db->from($this->tabelas->tb_propostas);
        $this->db->where('PRP_ID', $proposta_id);
        $this->db->limit(1);

        $queryprp = $this->db->get();

        $proposta = $queryprp->row();

        $chaves = array(
            'nome_contato' => '[nome-rl]',
            'valor_total' => '[valor-t]',
            'valor_parcelado' => '[valor-p]',
            'parcelas' => '[qtde-parcelas]',
            'detalhe_parcelamento' => '[DETALHE-PARCELAMENTO]',
            'convenios' => '[CONV-PBM]',
            'qde_farmacias' => '[qtd-farm]',
            'data_proposta' => '[dt-proposta]',
            'intervalo_parcelas' => '[intervalo-parcelas]'
        );

        $valor_parcelado = str_replace(".", "", $proposta->PRP_VALOR_PARCELADO);
        $valor = (float) str_replace(",", ".", $valor_parcelado);

        $par = (int) $proposta->PRP_PARCELAS;

        $valparcela = $valor / $par;

        $detalhe_parcelamento = $par . " parcelas de R$ " . number_format($valparcela, 2, ',', '.');
        $intervalo_parcelas = $proposta->PRP_INTERVALO_PARCELAS . " dias";

        $dt_proposta = new DateTime($proposta->PRP_DT_CREATED);

        $troca = array(
            'nome_contato' => $proposta->CLI_CONTATO_NOME,
            'valor_total' => "R$ " . $proposta->PRP_VALOR_TOTAL,
            'valor_parcelado' => "R$ " . $proposta->PRP_VALOR_PARCELADO,
            'parcelas' => $proposta->PRP_PARCELAS,
            'detalhe_parcelamento' => $detalhe_parcelamento,
            'convenios' => $proposta->CTT_CONVENIOS,
            'qde_farmacias' => $proposta->PRP_QTDE_FARMACIAS,
            'data_proposta' => $dt_proposta->format('d/m/Y H:m:s'),
            'intervalo_parcelas' => $intervalo_parcelas,
        );

        $data = array(
            'PRP_LAYOUT' => str_replace($chaves, $troca, $layout_modelo)
        );

        if ($this->upddata($data, $this->tabelas->tb_propostas, $proposta_id, 'PRP_ID')) {
            return true;
        } else {
            return false;
        }
    }

    function dadosCompletos($contrato_id)
    {

        $campos = array('CLI_RAZAO_SOCIAL',
            'CLI_NOME_FANTASIA',
            'CLI_CNPJ',
            'CLI_LOGRADOURO_EMPRESA',
            'CLI_NUMERO_EMPRESA',
            'CLI_COMPLEMENTO_EMPRESA',
            'CLI_BAIRRO_EMPRESA',
            'CLI_UF_EMPRESA',
            'CLI_CIDADE_EMPRESA',
            'CLI_CEP_EMPRESA',
            'CLI_CONTATO_NOME',
            'CLI_CONTATO_CARGO',
            'CLI_RG_CONTATO',
            'CLI_CPF_CONTATO',
            'CLI_EMAIL_CONTATO',
            'CLI_LOGRADOURO_CONTATO',
            'CLI_NUMERO_CONTATO',
            'CLI_BAIRRO_CONTATO',
            'CLI_UF_CONTATO',
            'CLI_CIDADE_CONTATO',
            'CLI_CEP_CONTATO');

        $this->db->select('
            CLI_RAZAO_SOCIAL,
            CLI_NOME_FANTASIA,
            CLI_CNPJ,
            CLI_LOGRADOURO_EMPRESA,
            CLI_NUMERO_EMPRESA,
            CLI_COMPLEMENTO_EMPRESA,
            CLI_BAIRRO_EMPRESA,
            CLI_UF_EMPRESA,
            CLI_CIDADE_EMPRESA,
            CLI_CEP_EMPRESA,
            CLI_CONTATO_NOME,
            CLI_CONTATO_CARGO,
            CLI_RG_CONTATO,
            CLI_CPF_CONTATO,
            CLI_EMAIL_CONTATO,
            CLI_LOGRADOURO_CONTATO,
            CLI_NUMERO_CONTATO,
            CLI_BAIRRO_CONTATO,
            CLI_UF_CONTATO,
            CLI_CIDADE_CONTATO,
            CLI_CEP_CONTATO');
        $this->db->from($this->tabelas->tb_contratos);
        $this->db->where('CTT_ID', $contrato_id);
        $this->db->limit(1);

        $query = $this->db->get();

        $informacoes = $query->result_array();

        foreach ($campos as $value) {
            if (empty($informacoes[0][$value])) {
                return false;
                break;
            }
        }

        return true;
    }

    function gerarLayoutContrato($modelo_id, $contrato_id)
    {

        //busca o modelo
        $this->db->select('*');
        $this->db->from($this->tabelas->tb_modelos);
        $this->db->where('MDL_ID', $modelo_id);
        $this->db->limit(1);

        $querymdl = $this->db->get();

        $layout_modelo = $querymdl->row()->MDL_LAYOUT;

        //busca a proposta
        $this->db->select('*');
        $this->db->from($this->tabelas->tb_contratos);
        $this->db->where('CTT_ID', $contrato_id);
        $this->db->limit(1);

        $queryprp = $this->db->get();

        $contrato = $queryprp->row();

        $chaves = array(
            'razao_social' => '[razao]',
            'nome_fantasia' => '[nome-fant]',
            'cnpj_cliente' => '[CNPJ]',
            'end_cliente' => '[ENDERECO-CLIENTE]',
            'nome_contato' => '[nome-rl]',
            'cargo_contato' => '[carg]',
            'rg_contato' => '[identid]',
            'cpf_contato' => '[CPF-RL-CLIENTE]',
            'end_contato' => '[END-RL-CLIENTE]',
            'valor_total' => '[valor-t]',
            'valor_parcelado' => '[valor-p]',
            'parcelas' => '[qtde-parcelas]',
            'detalhe_parcelamento' => '[DETALHE-PARCELAMENTO]',
            'convenios' => '[CONV-PBM]',
            'outros_cnpjs' => '[mCNPJ]',
            'qtde_vendas' => '[numero-vendas]',
            'vendedor' => '[vendedor]',
            'dta_contrato' => '[data-contrato]',
            'ip_usuario' => '[ip-usuario]',
            'data_assinatura' => '[data-assinatura]',
            'chave_gerada' => '[chave-gerada]',
            'intervalo_parcelas' => '[intervalo-parcelas]',
            'primeira_parcela' => '[primeira-parcela]'
        );

        $detalhe_parcelamento = "";
        $intervalo_parcelas = "";
        $primeiraparcela = "";

        $valor_parcelado = str_replace(".", "", $contrato->CTT_VALOR_PARCELADO);
        $valor = (float) str_replace(",", ".", $valor_parcelado);

        $oppag = $contrato->CTT_OP_PAG_SLCT;

        $par = (int) $contrato->CTT_PARCELAS;

        if ((int) $oppag == 1) {
            $detalhe_parcelamento .= "R$ " . $contrato->CTT_VALOR_TOTAL . ' à vista';

            $dataassinatura = $this->atualizaData($contrato->CTT_DATA_PRIMEIRA_PARCELA, $contrato_id);

            if (!empty($contrato->CTT_DT_APROV)) {
                $time = DateTime::createFromFormat('d/m/Y H:i:s', $contrato->CTT_DT_APROV);

                $dataaprovacao = $time->format("Y-m-d H:i:s");

                $datapripag = new DateTime("$dataaprovacao");
                $datapripag->add(new DateInterval("P3D"));

                $primeiraparcela = $datapripag->format('d/m/Y');
            } else {
                $primeiraparcela = null;
            }
        } else {
            $valparcela = $valor / $par;

            $dataassinatura = $this->atualizaData($contrato->CTT_DATA_PRIMEIRA_PARCELA, $contrato_id);
            $intervaloparcelas = (int) $contrato->CTT_INTERVALO_PARCELAS;

            $datas = array();

            for ($i = 1; $i <= $par; $i++) {
                if ($i == 1) {
                    array_push($datas, $dataassinatura);
                } else {
                    $intervalo = $i * $intervaloparcelas;
                    $strinterval = "P" . $intervalo . "D";

                    $datapag = DateTime::createFromFormat('d/m/Y', $dataassinatura);

                    $datapag->add(new DateInterval($strinterval));
                    array_push($datas, $datapag->format('d/m/Y'));
                }
            }

            $detalhe_parcelamento = $par . " parcelas de R$ " . number_format($valparcela, 2, ',', '.');
            $intervalo_parcelas = $contrato->CTT_INTERVALO_PARCELAS . " dias";

            $time = DateTime::createFromFormat('d/m/Y H:i:s', $contrato->CTT_DT_APROV);

            //$dataaprovacao = $time->format("Y-m-d H:i:s");

            $datapripag = new DateTime("$time");
            $datapripag->add(new DateInterval("P3D"));

            $primeiraparcela = $datapripag->format('d/m/Y');
        }

        $uf = $this->getUF((int) $contrato->CLI_UF_EMPRESA);
        $ufcontato = $this->getUF((int) $contrato->CLI_UF_CONTATO);

        $troca = array(
            'razao_social' => $contrato->CLI_RAZAO_SOCIAL,
            'nome_fantasia' => $contrato->CLI_NOME_FANTASIA,
            'cnpj_cliente' => $contrato->CLI_CNPJ,
            'end_cliente' => $contrato->CLI_LOGRADOURO_EMPRESA . ", " . $contrato->CLI_NUMERO_EMPRESA . " " . $contrato->CLI_COMPLEMENTO_EMPRESA . " - " . $contrato->CLI_BAIRRO_EMPRESA . " - " . $contrato->CLI_CIDADE_EMPRESA . " - " . $uf . ", CEP: " . $contrato->CLI_CEP_EMPRESA,
            'nome_contato' => $contrato->CLI_CONTATO_NOME,
            'cargo_contato' => $contrato->CLI_CONTATO_CARGO,
            'rg_contato' => $contrato->CLI_RG_CONTATO,
            'cpf_contato' => $contrato->CLI_CPF_CONTATO,
            'end_contato' => $contrato->CLI_LOGRADOURO_CONTATO . ", " . $contrato->CLI_NUMERO_CONTATO . " " . $contrato->CLI_COMPLEMENTO_CONTATO . " - " . $contrato->CLI_BAIRRO_CONTATO . " - " . $contrato->CLI_CIDADE_CONTATO . " - " . $ufcontato . ", CEP: " . $contrato->CLI_CEP_CONTATO,
            'valor_total' => 'R$ ' . $contrato->CTT_VALOR_TOTAL,
            'valor_parcelado' => 'R$ ' . $contrato->CTT_VALOR_PARCELADO,
            'parcelas' => $contrato->CTT_PARCELAS,
            'detalhe_parcelamento' => $detalhe_parcelamento,
            'convenios' => $contrato->CTT_CONVENIOS,
            'outros_cnpjs' => $contrato->CTT_OUTROS_CNPJS,
            'qtde_vendas' => $contrato->CTT_QTDE_VENDAS,
            'vendedor' => $contrato->CTT_VENDEDOR,
            'dta_contrato' => $contrato->CTT_DATA_CONTRATO,
            'ip_usuario' => $contrato->CTT_IP_CLIENTE,
            'data_assinatura' => $contrato->CTT_DT_APROV,
            'chave_gerada' => $contrato->CTT_KEY_ASSINATURA,
            'intervalo_parcelas' => $intervalo_parcelas,
            'primeira_parcela' => $primeiraparcela,
        );

        $data = array(
            'CTT_LAYOUT' => str_replace($chaves, $troca, $layout_modelo)
        );

        if ($this->upddata($data, $this->tabelas->tb_contratos, $contrato_id, 'CTT_ID')) {
            return true;
        } else {
            return false;
        }
    }

    function atualizaData($data, $id)
    {
        $now = new DateTime();
        $hoje = $now->format("d/m/Y");

        $dthoje = DateTime::createFromFormat('d/m/Y', $hoje);
        $dthoje->add(new DateInterval("P3D"));

        if (strtotime($data) <= strtotime($dthoje->format("d/m/Y"))) {

            //atualiza data da primeira parcela do contrato
            $datactt = array(
                'CTT_DATA_PRIMEIRA_PARCELA' => $dthoje->format("d/m/Y")
            );

            $this->upddata($datactt, $this->tabelas->tb_contratos, $id, 'CTT_ID');

            //atualiza data da primeira parcela da proposta
            $dataprp = array(
                'PRP_DATA_PRIMEIRA_PARCELA' => $dthoje->format("d/m/Y")
            );

            $this->upddata($dataprp, $this->tabelas->tb_propostas, $id, 'PRP_ID');

            return $dthoje->format("d/m/Y");
        } else {
            return $data;
        }
    }

    function sendNotificacaoInterna($modulo, $d)
    {

        if ($modulo == "propostas") {
            $data = array(
                'PRP_SEND_NOTIFICATION' => 1, 'PRP_STATUS' => 3
            );

            if ($this->upddata($data, $this->tabelas->tb_propostas, $d, 'PRP_ID')) {
                return true;
            } else {
                return false;
            }
        } elseif ($modulo == "contratos") {
            $data = array(
                'CTT_SEND_NOTIFICATION' => 1, 'CTT_STATUS' => 2
            );

            if ($this->upddata($data, $this->tabelas->tb_contratos, $d, 'CTT_ID')) {
                return true;
            } else {
                return false;
            }
        }
    }

    function sendEmailcliente($modulo, $d)
    {
        if ($modulo == "propostas") {
            $data = array(
                'PRP_SEND_MAIL_CLIENTE' => 1,
                'PRP_PERCENTUAL' => 60
            );

            if ($this->upddata($data, $this->tabelas->tb_propostas, $d, 'PRP_ID')) {
                return true;
            } else {
                return false;
            }
        } elseif ($modulo == "contratos") {
            $data = array(
                'CTT_SEND_MAIL_CLIENTE' => 1,
                'CTT_PERCENTUAL' => 60
            );

            if ($this->upddata($data, $this->tabelas->tb_contratos, $d, 'CTT_ID')) {
                return true;
            } else {
                return false;
            }
        }
    }

    function getTagsCadastradas($tag)
    {

        $query = $this->db->query('SELECT * FROM ' . $this->tabelas->tb_contratos . ' WHERE CTT_TAGS LIKE "%' . $tag . '%" COLLATE utf8_general_ci');

        $count = $query->num_rows();

        if ($count > 0) {
            return false;
        } else {
            return true;
        }
    }

    function getModelo($id)
    {
        $this->db->select('*');
        $this->db->from($this->tabelas->tb_modelos);
        $this->db->where('MDL_ID', $id);
        $this->db->limit(1);

        $query = $this->db->get();

        return $query->row();
    }

    function getmodeloLayout($id)
    {
        $this->db->select('*');
        $this->db->from($this->tabelas->tb_modelos);
        $this->db->where('MDL_ID', $id);
        $this->db->limit(1);

        $query = $this->db->get();

        return $query->result();
    }

    function getPropostaUrl($url)
    {
        /* seta a proposta como visualizada */
        $data = array(
            'PRP_PERCENTUAL' => 80
        );

        $this->upddata($data, $this->tabelas->tb_propostas, $url, 'PRP_URL_INFO');

        $this->db->select('*');
        $this->db->from($this->tabelas->tb_propostas);
        $this->db->where('PRP_URL_INFO="' . $url . '"');
        $this->db->limit(1);

        $query = $this->db->get();

        return $query->result();
    }

    function getPropostaId($id)
    {

        $this->db->select('*');
        $this->db->from($this->tabelas->tb_propostas . " prop");
        $this->db->join($this->tabelas->tb_usuario . " us", 'us.US_ID = prop.PRP_USER_CREATED');
        $this->db->where('prop.PRP_ID', $id);
        $this->db->limit(1);

        $query = $this->db->get();

        return $query->result();
    }

    function getModeloPropostaId($id)
    {

        $this->db->select('*');
        $this->db->from($this->tabelas->tb_propostas);
        $this->db->where('PRP_ID', $id);
        $this->db->limit(1);

        $query = $this->db->get();

        return $query->row();
    }

    function getContratoUrl($url)
    {
        /* seta o contrato como visualizada */
        $data = array(
            'CTT_SEND_MAIL_CLIENTE' => 1,
            'CTT_PERCENTUAL' => 80
        );

        $this->upddata($data, $this->tabelas->tb_contratos, $url, 'CTT_URL_INFO');

        $this->db->select('*');
        $this->db->from($this->tabelas->tb_contratos);
        $this->db->where('CTT_URL_INFO="' . $url . '"');
        $this->db->limit(1);

        $query = $this->db->get();

        return $query->result();
    }

    function getContratoId($id)
    {
        $this->db->select('*');
        $this->db->from($this->tabelas->tb_contratos);
        $this->db->where('CTT_ID', $id);
        $this->db->limit(1);

        $query = $this->db->get();

        return $query->result();
    }

    function getModeloContratoId($id)
    {

        $this->db->select('*');
        $this->db->from($this->tabelas->tb_contratos);
        $this->db->where('CTT_ID', $id);
        $this->db->limit(1);

        $query = $this->db->get();

        return $query->row();
    }

    function validaToken($token)
    {

        $now = date("Y-m-d H:i:s");

        $this->db->select('*');
        $this->db->from($this->tabelas->tb_acesso);
        $this->db->where('AC_TOKEN', $token);
        $this->db->where('AC_DATA_EXP >= "' . $now . '"');

        $query = $this->db->get();

        $result = $query->result();

        if (!empty($result)) {

            return true;
        } else {

            $acesso = array(
                'CONNECTED' => 0
            );

            $this->upddata($acesso, $this->tabelas->tb_acesso, $token, 'AC_TOKEN');

            return false;
        }
    }

    function disconnect($token, $data)
    {
        return $this->upddata($data, $this->tabelas->tb_acesso, $token, 'AC_TOKEN');
    }

    function aprovar($modulo, $oppag, $id, $ip)
    {
        $now = date("d/m/Y H:i:s");

        if ($modulo == "propostas") {

            $proposta = array(
                'PRP_DT_APROV' => $now,
                'PRP_APROVADA' => 1,
                'PRP_STATUS' => 4,
                'PRP_PERCENTUAL' => 100,
                'PRP_OP_PAG_SLCT' => $oppag,
                'PRP_IP_CLIENTE' => $ip,
                'PRP_KEY_ASSINATURA' => strtoupper(md5($ip . $now))
            );

            return $this->upddata($proposta, $this->tabelas->tb_propostas, $id, 'PRP_ID');
        }

        if ($modulo == "contratos") {

            $contt = $this->getContratoId($id);
            $modelo_id = $contt[0]->MDL_ID;

            $contrato = array(
                'CTT_DT_APROV' => $now,
                'CTT_STATUS' => 3,
                'CTT_IP_CLIENTE' => $ip,
                'CTT_PERCENTUAL' => 100,
                'CTT_KEY_ASSINATURA' => strtoupper(md5($ip . $now))
            );

            $this->gerarLayoutContrato($modelo_id, $id);

            return $this->upddata($contrato, $this->tabelas->tb_contratos, $id, 'CTT_ID');
        }
    }

    function reprova($table, $id, $ip)
    {

        $now = date("d/m/Y H:i:s");

        if ($table == "propostas") {

            $proposta = array(
                'PRP_DT_REJECT' => $now,
                'PRP_APROVADA' => 2,
                'PRP_STATUS' => 0,
                'PRP_PERCENTUAL' => 100
            );

            return $this->upddata($proposta, $this->tabelas->tb_propostas, $id, 'PRP_ID');
        }

        if ($table == "contratos") {

            $contrato = array(
                'CTT_DT_REJECT' => $now,
                'CTT_STATUS' => 4,
                'CTT_IP_CLIENTE' => $ip,
                'CTT_PERCENTUAL' => 100
            );

            return $this->upddata($contrato, $this->tabelas->tb_contratos, $id, 'CTT_ID');
        }
    }

    function valorParcela($valor, $taxa, $parcelas)
    {
        $comum = new Comum();

        return $comum->valorParcela($valor, $taxa, $parcelas);
    }

    function decrypt($q)
    {

        $comum = new Comum();

        return $comum->decrypt($q);
    }

    function encrypt($q)
    {

        $comum = new Comum();

        return $comum->encrypt($q);
    }

    function removeacentos($str)
    {
        $comum = new Comum();

        return $comum->tirarAcentos($str);
    }
}

?>