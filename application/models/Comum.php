<?php 

if (!defined('BASEPATH'))    
exit('No direct script access allowed');
    
class Comum{    
    
    protected $key;    
    public $tb_usuario;    
    public $tb_acesso;    
    public $tb_tipos_usuarios;    
    public $tb_propostas;    
    public $tb_modelos;    
    public $tb_contratos;    
    public $tb_servicos;    
    public $tb_log;    
    public $tb_config;    
    public $tb_estados;    
    public $tb_config_status;    
    public $tb_config_aprovacao;    
    public $tb_config_ct_status;    
    public $tb_config_ct_envio;    
    
    function __construct()    {        
        $this->key = "tgB425oF4fdso51l";
        $this->tb_usuario = "TB_USUARIOS";
        $this->tb_acesso = "TB_ACESSO_USUARIO";
        $this->tb_tipos_usuarios = "TB_TIPO_USUARIO";
        $this->tb_propostas = "TB_PROPOSTAS";
        $this->tb_modelos = "TB_MODELOS";
        $this->tb_contratos = "TB_CONTRATOS";
        $this->tb_servicos = "TB_TIPOS_SERVICOS";
        $this->tb_log = "TB_LOG_CONTRATACAO";
        $this->tb_config = "TB_CONFIGURACOES";
        $this->tb_estados = "TB_ESTADOS";
        $this->tb_config_aprovacao = "TB_CONFIG_PROP_APROVACAO";
        $this->tb_config_status = "TB_CONFIG_PROP_STATUS";
        $this->tb_config_ct_status = "TB_CONFIG_CTT_STATUS";
        $this->tb_config_ct_envio = "TB_CONFIG_CTT_ENVIO";
    }    
    
    function decrypt($value){
        if ($value != null) {
            $crypttext = $this->safe_b64decode($value);
            $iv_size = @mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
            $iv = @mcrypt_create_iv($iv_size, MCRYPT_RAND);
            $decrypttext = @mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->key, $crypttext, MCRYPT_MODE_ECB, $iv);
            
            return trim($decrypttext);
            
        } else {
            return null;
        }
    }
    
    function encrypt($value){
        
        if ($value != null) {
            $text = $value;
            $iv_size = @mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
            $iv = @mcrypt_create_iv($iv_size, MCRYPT_RAND);
            $crypttext = @mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->key, $text, MCRYPT_MODE_ECB, $iv);
            return trim($this->safe_b64encode($crypttext));
        } else {
            return null;
        }
    }
    
    private function safe_b64encode($string){
        $data = base64_encode($string);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return $data;
    }
    
    private function safe_b64decode($string){
        $data = str_replace(array('-', '_'), array('+', '/'), $string);
        $mod4 = strlen($data) % 4;
        
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        
        return base64_decode($data);
    }
    
    function tirarAcentos($str){
        $str = strtolower($str);
        $str = preg_replace('/[áàãâä]/ui', 'a', $str);
        $str = preg_replace('/[éèêë]/ui', 'e', $str);
        $str = preg_replace('/[íìîï]/ui', 'i', $str);
        $str = preg_replace('/[óòõôö]/ui', 'o', $str);
        $str = preg_replace('/[úùûü]/ui', 'u', $str);
        $str = preg_replace('/[ç]/ui', 'c', $str);
        $str = preg_replace('/[^a-z0-9]/i', '_', $str);
        $str = preg_replace('/_+/', '_', $str);       
        
        return $str;    
    }
    
    function valorParcela($valor, $taxa, $parcelas){
        $taxa = floatval($taxa) / 100;
        $parcelas = (int) $parcelas;
        $valor = str_replace('.', '', $valor);
        $valor = str_replace(',', '.', $valor);
        $valor = (double) $valor;
        $valParcela = $valor * pow((1 + $taxa), $parcelas);
        $valParcela = number_format($valParcela / $parcelas, 2, ",", ".");
        
        return $valParcela;
    }
}?>