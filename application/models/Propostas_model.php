<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once 'Comum.php';

class Propostas_model extends CI_Model
{

    protected $tabelas;

    function __construct()
    {
        parent::__construct();
        $this->tabelas = new Comum();
    }
    /* PROPOSTAS */

    var $column_order = array('PRP_ID', 'PRP_DESC', 'PRP_URL_INFO', 'CLI_CONTATO_NOME', 'SERV_DESC', 'USER_CREATED', 'PRP_DT_CREATED', 'SITUACAO', 'SITUACAO', null);
    var $column_search = array('PRP_ID', 'PRP_DESC', 'PRP_URL_INFO', 'CLI_CONTATO_NOME', 'SERV_DESC', 'USER_CREATED', 'PRP_DT_CREATED', 'cap.DESCRICAO', 'cst.DESCRICAO');
    var $order = array('PRP_ID' => 'desc');

    private function _getpropostas($per, $user)
    {
        $this->db->select("
				prop.PRP_ID as PRP_ID,
				prop.PRP_DESC as PRP_DESC,
                prop.PRP_STATUS_VIEW as STATUSVIEW,
				prop.PRP_URL_INFO as PRP_URL_INFO,
				prop.CLI_CONTATO_NOME as CLI_CONTATO_NOME,
				prop.SERV_DESC as SERV_DESC,
                us.USER_NAME_FULL as USER_CREATED,
				prop.PRP_DT_CREATED as PRP_DT_CREATED,
				cap.DESCRICAO as SITUACAO,
				cst.DESCRICAO as STATUS
		");
        $this->db->from($this->tabelas->tb_propostas . " prop");
        $this->db->join($this->tabelas->tb_config_aprovacao . " cap", 'cap.ID = prop.PRP_APROVADA', 'left');
        $this->db->join($this->tabelas->tb_config_status . " cst", 'cst.ID = prop.PRP_STATUS', 'left');
        $this->db->join($this->tabelas->tb_usuario . " us", 'us.US_ID = prop.PRP_USER_CREATED', 'left');
        //$this->db->where('PRP_STATUS_VIEW', 'S');

        if (in_array(20, $per)) {
            $this->db->where('PRP_USER_CREATED', $user->id);
        }

        $i = 0;

        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $this->db->group_start();

                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $dir = $_POST['order']['0']['dir'];

            switch ($_POST['order']['0']['column']) {
                case 0 :
                    $order = 'PRP_ID';
                    break;
                case 1 :
                    $order = 'PRP_DESC';
                    break;
                case 2 :
                    $order = 'CLI_CONTATO_NOME';
                    break;
                case 3 :
                    $order = 'SERV_DESC';
                    break;
                case 4 :
                    $order = 'PRP_DT_CREATED';
                    break;
                case 5 :
                    $order = 'cap.DESCRICAO';
                    break;
                case 6 :
                    $order = 'cst.DESCRICAO';
                    break;
            }

            $this->db->order_by($order, $dir);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function _get_custom_propostas()
    {
        if (!empty($_POST['columns'][1]['search']['value'])) {
            $this->db->like('PRP_DESC', $_POST['columns'][1]['search']['value']);
        }

        if (!empty($_POST['columns'][2]['search']['value'])) {
            $this->db->like('us.CLI_CONTATO_NOME', $_POST['columns'][2]['search']['value']);
        }

        if (!empty($_POST['columns'][3]['search']['value'])) {
            $this->db->like('us.USER_NAME_FULL', $_POST['columns'][3]['search']['value']);
        }

        if (!empty($_POST['columns'][4]['search']['value'])) {
            $this->db->like('SERV_DESC', $_POST['columns'][4]['search']['value']);
        }

        if (!empty($_POST['columns'][5]['search']['value'])) {

            if (!empty($_POST['columns'][9]['search']['value'])) {

                $dataini = $_POST['columns'][5]['search']['value'];
                $datafim = $_POST['columns'][9]['search']['value'];

                $this->db->where("DATE(PRP_DT_CREATED) BETWEEN '" . $dataini . "' AND '" . $datafim . "'");
            } else {

                $this->db->like("DATE(PRP_DT_CREATED)", $_POST['columns'][5]['search']['value']);
            }
        } else {

            $this->db->like("DATE(PRP_DT_CREATED)", $_POST['columns'][5]['search']['value']);
        }

        if (!empty($_POST['columns'][6]['search']['value'])) {
            $this->db->like('cap.DESCRICAO', $_POST['columns'][6]['search']['value']);
        }

        if (!empty($_POST['columns'][7]['search']['value'])) {
            $this->db->like('cst.DESCRICAO', $_POST['columns'][7]['search']['value']);
        }
    }

    function getpropostas($token)
    {
        $user = $this->infoUser($token);

        $permissoes = explode(', ', $user->permissoes);

        $this->_getpropostas($permissoes, $user);
        $this->_get_custom_propostas();

        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    function filtropropostas($token)
    {
        $user = $this->infoUser($token);

        $permissoes = explode(', ', $user->permissoes);

        $this->_getpropostas($permissoes, $user);
        $this->_get_custom_propostas();

        $query = $this->db->get();
        return $query->num_rows();
    }

    public function todospropostas($token)
    {
        $user = $this->infoUser($token);

        $permissoes = explode(', ', $user->permissoes);

        $this->db->from($this->tabelas->tb_propostas);
        return $this->db->count_all_results();
    }
    /* PROPOSTAS */

    //VERFICA SE JÁ EXISTE COM O MESMO NOME
    function verificaExiste($nome)
    {
        $this->db->select('');
        $this->db->from($this->tabelas->tb_propostas);
        $this->db->where('PRP_DESC', $nome);

        $query = $this->db->get();

        return $query->num_rows();
    }

    function insert($modulo, $data)
    {

        $tbmodulos = array('propostas');

        if (in_array($modulo, $tbmodulos) && $modulo == "propostas") {

            $token = $data['token'];

            if ($data['PRP_URL_DEFAULT'] == 'sim') {

                $contador = $this->verificaExiste($data['PRP_DESC']);

                if (intval($contador) > 0) {
                    $url_info = $this->removeacentos($data['PRP_DESC'] . '_' . (intval($contador) + 1));
                } else {
                    $url_info = $this->removeacentos($data['PRP_DESC']);
                }
            } else {
                $url_info = $data['PRP_URL_INFO'];
            }

            $now = new DateTime();

            $dados = array(
                'MDL_ID' => $data['MDL_ID'],
                'SERV_ID' => $data['SERV_ID'],
                'CTT_ID' => $data['CTT_ID'],
                'PRP_APROVADA' => 0,
                'PRP_STATUS' => 1, /* 1-criado, 2-finalizado, 3- enviado, 4-concluÍdo */
                'PRP_SEND_NOTIFICATION' => 0, /* 0-nÃo enviado, 1-enviado */
                'PRP_SEND_MAIL' => $data['PRP_SEND_MAIL'],
                'PRP_EMAIL_DEFAULT' => $data['PRP_EMAIL_DEFAULT'],
                'PRP_URL_DEFAULT' => $data['PRP_URL_DEFAULT'],
                'PRP_URL_INFO' => $url_info,
                'PRP_DESC' => $data['PRP_DESC'],
                'CLI_CONTATO_NOME' => $data['CLI_CONTATO_NOME'],
                'CLI_EMAIL_CONTATO' => $data['CLI_EMAIL_CONTATO'],
                'SERV_DESC' => $data['SERV_DESC'],
                'PRP_VALOR_TOTAL' => $data['PRP_VALOR_TOTAL'],
                'PRP_VALOR_PARCELADO' => $data['PRP_VALOR_PARCELADO'],
                'PRP_PARCELAS' => $data['PRP_PARCELAS'],
                'PRP_INTERVALO_PARCELAS' => $data['PRP_INTERVALO_PARCELAS'],
                'CTT_CONVENIOS' => $data['CTT_CONVENIOS'],
                'PRP_QTDE_FARMACIAS' => $data['PRP_QTDE_FARMACIAS'],
                'PRP_USER_CREATED' => $this->infoUser($token)->id,
                'PRP_DT_CREATED' => $now->format('Y-m-d H:i:s'),
                'PRP_PERCENTUAL' => 20
            );

            $this->db->insert($this->tabelas->tb_propostas, $dados);

            //adiciona as informações no layout
            $proposta_id = $this->db->insert_id();
            $modelo_id = $data['MDL_ID'];

            if ($this->gerarLayoutProposta($modulo, $modelo_id, $proposta_id)) {
                return $proposta_id;
            } else {
                return null;
            }
        }

        return false;
    }

    function edit($modulo, $data, $id)
    {

        $tbmodulos = array('propostas');

        if (in_array($modulo, $tbmodulos) && $modulo == "propostas") {

            $token = $data['token'];

            if ($data['PRP_URL_DEFAULT'] == 'sim') {
                $url_info = $this->removeacentos($data['PRP_DESC']);
            } else {
                $url_info = $data['PRP_URL_INFO'];
            }

            $dados = array(
                'MDL_ID' => $data['MDL_ID'],
                'SERV_ID' => $data['SERV_ID'],
                'CTT_ID' => $data['CTT_ID'],
                'PRP_SEND_MAIL' => $data['PRP_SEND_MAIL'],
                'PRP_EMAIL_DEFAULT' => $data['PRP_EMAIL_DEFAULT'],
                'PRP_URL_DEFAULT' => $data['PRP_URL_DEFAULT'],
                'PRP_URL_INFO' => $url_info,
                'PRP_DESC' => $data['PRP_DESC'],
                'CLI_CONTATO_NOME' => $data['CLI_CONTATO_NOME'],
                'CLI_EMAIL_CONTATO' => $data['CLI_EMAIL_CONTATO'],
                'SERV_DESC' => $data['SERV_DESC'],
                'PRP_VALOR_TOTAL' => $data['PRP_VALOR_TOTAL'],
                'PRP_VALOR_PARCELADO' => $data['PRP_VALOR_PARCELADO'],
                'CTT_CONVENIOS' => $data['CTT_CONVENIOS'],
                'PRP_PARCELAS' => $data['PRP_PARCELAS'],
                'PRP_QTDE_FARMACIAS' => $data['PRP_QTDE_FARMACIAS'],
            );

            if ($this->upddata($dados, $this->tabelas->tb_propostas, $id, 'PRP_ID')) {

                $modelo_id = $data['MDL_ID'];

                $this->gerarLayoutProposta($modulo, $modelo_id, $id);

                return $id;
            } else {
                return null;
            }
        }
    }

    function delete($modulo, $id)
    {

        $tbmodulos = array('propostas');

        if (in_array($modulo, $tbmodulos) && $modulo == "propostas") {
            $this->db->delete($this->tabelas->tb_propostas, array('PRP_ID' => $id));

            if ($this->db->affected_rows()) {
                return true;
            } else {
                return false;
            }
        }
    }

    function ativarInativar($modulo, $id)
    {

        $tbmodulos = array('propostas');

        if (in_array($modulo, $tbmodulos) && $modulo == "propostas") {

            $tipo = null;

            $this->db->select('');
            $this->db->from($this->tabelas->tb_propostas);
            $this->db->where('PRP_ID', $id);
            $this->db->limit(1);

            $query = $this->db->get();

            $tiporeg = $query->row()->PRP_STATUS_VIEW;

            if ($tiporeg == 'S') {
                $tipo = 'N';
            } else {
                $tipo = 'S';
            }

            $dados = array(
                'PRP_STATUS_VIEW' => $tipo
            );

            if ($this->upddata($dados, $this->tabelas->tb_propostas, $id, 'PRP_ID')) {
                return true;
            } else {
                return false;
            }
        }
    }

    function gerarLayoutProposta($modulo, $modulo_id, $proposta_id)
    {

        //busca o modelo
        $this->db->select('*');
        $this->db->from($this->tabelas->tb_modelos);
        $this->db->where('MDL_ID', $modulo_id);
        $this->db->limit(1);

        $querymdl = $this->db->get();

        $layout_modelo = $querymdl->row()->MDL_LAYOUT;

        //busca a proposta
        $this->db->select('*');
        $this->db->from($this->tabelas->tb_propostas);
        $this->db->where('PRP_ID', $proposta_id);
        $this->db->limit(1);

        $queryprp = $this->db->get();

        $proposta = $queryprp->row();

        $chaves = array(
            'nome_contato' => '[nome-rl]',
            'valor_total' => '[valor-t]',
            'valor_parcelado' => '[valor-p]',
            'parcelas' => '[qtde-parcelas]',
            'detalhe_parcelamento' => '[DETALHE-PARCELAMENTO]',
            'convenios' => '[CONV-PBM]',
            'qde_farmacias' => '[qtd-farm]',
            'data_proposta' => '[dt-proposta]',
            'intervalo_parcelas' => '[intervalo-parcelas]'
        );

        $valor_parcelado = str_replace(".", "", $proposta->PRP_VALOR_PARCELADO);
        $valor = (float) str_replace(",", ".", $valor_parcelado);

        $par = (int) $proposta->PRP_PARCELAS;

        $valparcela = $valor / $par;

        $detalhe_parcelamento = $par . " parcelas de R$ " . number_format($valparcela, 2, ',', '.');
        $intervalo_parcelas = $proposta->PRP_INTERVALO_PARCELAS . " dias";

        $dt_proposta = new DateTime($proposta->PRP_DT_CREATED);

        $troca = array(
            'nome_contato' => $proposta->CLI_CONTATO_NOME,
            'valor_total' => "R$ " . $proposta->PRP_VALOR_TOTAL,
            'valor_parcelado' => "R$ " . $proposta->PRP_VALOR_PARCELADO,
            'parcelas' => $proposta->PRP_PARCELAS,
            'detalhe_parcelamento' => $detalhe_parcelamento,
            'convenios' => $proposta->CTT_CONVENIOS,
            'qde_farmacias' => $proposta->PRP_QTDE_FARMACIAS,
            'data_proposta' => $dt_proposta->format('d/m/Y H:m:s'),
            'intervalo_parcelas' => $intervalo_parcelas,
        );

        $data = array(
            'PRP_LAYOUT' => str_replace($chaves, $troca, $layout_modelo)
        );

        if ($this->upddata($data, $this->tabelas->tb_propostas, $proposta_id, 'PRP_ID')) {
            return true;
        } else {
            return false;
        }
    }

    function infoUser($token)
    {
        if ($this->validaToken($token)) {
            $this->db->select('*');
            $this->db->from($this->tabelas->tb_acesso);
            $this->db->where('AC_TOKEN', $token);

            $query = $this->db->get();

            $result = $query->row();

            if (!empty($result)) {

                $this->db->select('
                        us.US_ID as id,
                        us.USER_NAME_FULL as nome_completo,
                        us.US_MAIL as email,
                        us.US_LOGIN as login,
                        us.US_STATUS as status,
                        us.TYPE_ID as tipo,
                        tp.TYPE_PERMISSIONS as permissoes,
                        us.US_SMTP_HOST as smtphost,
                        us.US_SMTP_PORT as smtpporta,
                        us.US_SMTP_USER as smtpusuario,
                        us.US_SMTP_PASS as smtpsenha');
                $this->db->from($this->tabelas->tb_acesso . " acc");
                $this->db->join($this->tabelas->tb_usuario . " us", 'us.US_ID = acc.US_ID');
                $this->db->join($this->tabelas->tb_tipos_usuarios . " tp", 'tp.TYPE_ID = us.TYPE_ID');
                $this->db->where('acc.AC_TOKEN', $token);
                $this->db->limit(1);

                $query = $this->db->get();
                $array = $query->row();

                return $array;
            }
        } else {
            return null;
        }
    }

    function infoSistema($modulo)
    {
        $tbmodulos = array('configuracoes', 'propostas', 'contratos');

        if ((in_array($modulo, $tbmodulos))) {
            $this->db->select('*');
            $this->db->from($this->tabelas->tb_config);
            $this->db->limit(1);

            $query = $this->db->get();

            return $query->result();
        } else {
            return null;
        }
    }

    function get($modulo, $id)
    {
        $tbmodulos = array('propostas', 'servicos');

        if (in_array($modulo, $tbmodulos) && $modulo == "propostas") {
            $this->db->select('PRP_ID as id,
					PRP_DESC as descricao,
					MDL_ID as modelo,
					CLI_CONTATO_NOME as nome_contato,
					CLI_EMAIL_CONTATO as email_contato,
					PRP_EMAIL_DEFAULT as email_padrao,
					PRP_URL_DEFAULT as url_padrao,
					PRP_URL_INFO as urlproposta,
					SERV_ID as servico,
					SERV_DESC as outro_servicos,
					CTT_ID as modelo_contrato,
					PRP_VALOR_TOTAL as valor_total,
					PRP_VALOR_PARCELADO as valor_parcelado,
					PRP_INTERVALO_PARCELAS as intervalo_parcelas,
                    CTT_CONVENIOS as convenios,
					PRP_PARCELAS as parcelas,
					PRP_QTDE_FARMACIAS as qde_farmacias,
					PRP_DT_CREATED as data_gerado');
            $this->db->from($this->tabelas->tb_propostas);
            $this->db->where('PRP_ID', $id);
            $this->db->limit(1);

            $query = $this->db->get();

            return $query->result();
        }

        if (in_array($modulo, $tbmodulos) && $modulo == "servicos") {
            $this->db->select('SERV_ID as id,
					SERV_DESC as nome,
					SERV_INFO as descricao');
            $this->db->from($this->tabelas->tb_servicos);
            $this->db->where('SERV_ID', $id);
            $this->db->limit(1);

            $query = $this->db->get();

            return $query->result();
        }
    }

    function duplicar($modulo, $id, $token)
    {
        $tbmodulos = array('propostas');

        if (in_array($modulo, $tbmodulos) && $modulo == "propostas") {
            $this->db->select('*');
            $this->db->from($this->tabelas->tb_propostas);
            $this->db->where('PRP_ID', $id);
            $this->db->limit(1);

            $linha = $this->db->get()->row();

            //insere
            $now = new DateTime();

            $data['PRP_DESC'] = $linha->PRP_DESC . " - CÓPIA";
            $data['MDL_ID'] = $linha->MDL_ID;
            $data['PRP_APROVADA'] = 0;
            $data['PRP_STATUS'] = 1;
            $data['PRP_SEND_NOTIFICATION'] = 0;
            $data['CLI_CONTATO_NOME'] = $linha->CLI_CONTATO_NOME;
            $data['CLI_EMAIL_CONTATO'] = $linha->CLI_EMAIL_CONTATO;
            $data['PRP_EMAIL_DEFAULT'] = $linha->PRP_EMAIL_DEFAULT;
            $data['PRP_SEND_MAIL'] = $linha->PRP_SEND_MAIL;
            $data['PRP_URL_DEFAULT'] = $linha->PRP_URL_DEFAULT;
            $data['PRP_URL_INFO'] = $linha->PRP_URL_INFO;
            $data['SERV_ID'] = $linha->SERV_ID;
            $data['SERV_DESC'] = $linha->SERV_DESC;
            $data['CTT_ID'] = $linha->CTT_ID;
            $data['PRP_VALOR_TOTAL'] = $linha->PRP_VALOR_TOTAL;
            $data['PRP_VALOR_PARCELADO'] = $linha->PRP_VALOR_PARCELADO;
            $data['PRP_INTERVALO_PARCELAS'] = $linha->PRP_INTERVALO_PARCELAS;
            $data['CTT_CONVENIOS'] = $linha->CTT_CONVENIOS;
            $data['PRP_PARCELAS'] = $linha->PRP_PARCELAS;
            $data['PRP_QTDE_FARMACIAS'] = $linha->PRP_QTDE_FARMACIAS;
            $data['PRP_USER_CREATED'] = $this->infoUser($token)->id;
            $data['PRP_DT_CREATED'] = $now->format('Y-m-d H:i:s');
            $data['PRP_PERCENTUAL'] = 20;

            $this->db->insert($this->tabelas->tb_propostas, $data);

            return ($this->db->affected_rows() != 1) ? false : true;
        }

        if (in_array($modulo, $tbmodulos) && $modulo == "servicos") {
            $this->db->select('SERV_ID as id,
					SERV_DESC as nome,
					SERV_INFO as descricao');
            $this->db->from($this->tabelas->tb_servicos);
            $this->db->where('SERV_ID', $id);
            $this->db->limit(1);

            $query = $this->db->get();

            return $query->result();
        }
    }

    function validaToken($token)
    {

        $now = date("Y-m-d H:i:s");

        $this->db->select('*');
        $this->db->from($this->tabelas->tb_acesso);
        $this->db->where('AC_TOKEN', $token);
        $this->db->where('AC_DATA_EXP >= "' . $now . '"');

        $query = $this->db->get();

        $result = $query->result();

        if (!empty($result)) {

            return true;
        } else {

            $acesso = array(
                'CONNECTED' => 0
            );

            $this->upddata($acesso, $this->tabelas->tb_acesso, $token, 'AC_TOKEN');

            return false;
        }
    }

    function upddata($data, $table, $id, $field)
    {
        $this->db->where($field, $id);
        $this->db->update($table, $data);

        return true;
    }

    function disconnect($token, $data)
    {
        return $this->upddata($data, $this->tabelas->tb_acesso, $token, 'AC_TOKEN');
    }

    function removeacentos($str)
    {
        $comum = new Comum();

        return $comum->tirarAcentos($str);
    }
}

?>