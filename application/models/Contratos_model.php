<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once 'Comum.php';

class Contratos_model extends CI_Model
{

    protected $tabelas;

    function __construct()
    {

        parent::__construct();

        $this->tabelas = new Comum();
    }
    /* CONTRATOS */

    var $column_order = array('CTT_ID', 'CTT_DESC', 'CLI_RAZAO_SOCIAL', 'SERV_DESC', 'USER_CREATED', 'CTT_TAGS', 'CTT_DT_CREATE', 'STATUS', 'ENVIADO');
    var $column_search = array('CTT_ID', 'CTT_DESC', 'CLI_RAZAO_SOCIAL', 'SERV_DESC', 'USER_CREATED', 'CTT_TAGS', 'CTT_DT_CREATE', 'stt.DESCRICAO', 'env.DESCRICAO');
    var $order = array('CTT_ID' => 'desc');

    private function _getcontratos($per, $user)
    {

        $this->db->select("
				ctt.CTT_ID as CTT_ID,
				ctt.CTT_DESC as CTT_DESC,
                ctt.CTT_STATUS_VIEW as STATUSVIEW,
				ctt.CLI_RAZAO_SOCIAL as CLI_RAZAO_SOCIAL,
				ctt.SERV_DESC as SERV_DESC,
                us.USER_NAME_FULL as USER_CREATED,
				ctt.CTT_TAGS as CTT_TAGS,
				ctt.CTT_DT_CREATE as CTT_DT_CREATE,
				stt.DESCRICAO as STATUS,
				env.DESCRICAO as ENVIADO");
        $this->db->from($this->tabelas->tb_contratos . " ctt");
        $this->db->join($this->tabelas->tb_config_ct_status . " stt", 'stt.ID = ctt.CTT_STATUS', 'left');
        $this->db->join($this->tabelas->tb_config_ct_envio . " env", 'env.ID = ctt.CTT_SEND_MAIL_CLIENTE', 'left');
        $this->db->join($this->tabelas->tb_usuario . " us", 'us.US_ID = ctt.CTT_USER_CREATED', 'left');
        //$this->db->where('CTT_STATUS_VIEW', 'S');

        if (in_array(20, $per)) {
            $this->db->where('CTT_USER_CREATED', $user->id);
        }

        $i = 0;

        foreach ($this->column_search as $item) {
            if (!empty($_POST['search']['value'])) {

                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (!empty($_POST['order'])) {
            $dir = $_POST['order']['0']['dir'];

            switch ($_POST['order']['0']['column']) {
                case 0 :
                    $order = 'CTT_ID';
                    break;
                case 1 :
                    $order = 'CTT_DESC';
                    break;
                case 2 :
                    $order = 'CLI_RAZAO_SOCIAL';
                    break;
                case 3 :
                    $order = 'USER_NAME_FULL';
                    break;
                case 4 :
                    $order = 'SERV_DESC';
                    break;
                case 5 :
                    $order = 'CTT_DT_CREATE';
                    break;
                case 6 :
                    $order = 'stt.DESCRICAO';
                    break;
                case 7 :
                    $order = 'env.DESCRICAO';
                    break;
            }

            $this->db->order_by($order, $dir);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function _get_custom_contratos()
    {
        if (!empty($_POST['columns'][1]['search']['value'])) {
            $this->db->like('CTT_DESC', $_POST['columns'][1]['search']['value']);
        }

        if (!empty($_POST['columns'][2]['search']['value'])) {
            $this->db->like('CLI_RAZAO_SOCIAL', $_POST['columns'][2]['search']['value']);
        }

        if (!empty($_POST['columns'][3]['search']['value'])) {
            $this->db->like('us.USER_NAME_FULL', $_POST['columns'][3]['search']['value']);
        }

        if (!empty($_POST['columns'][4]['search']['value'])) {
            $this->db->like('SERV_DESC', $_POST['columns'][4]['search']['value']);
        }

        if (!empty($_POST['columns'][5]['search']['value'])) {

            if (!empty($_POST['columns'][10]['search']['value'])) {

                $dataini = $_POST['columns'][5]['search']['value'];
                $datafim = $_POST['columns'][10]['search']['value'];

                $this->db->where("DATE(CTT_DT_CREATE) BETWEEN '" . $dataini . "' AND '" . $datafim . "'");
            } else {

                $this->db->like("DATE(CTT_DT_CREATE)", $_POST['columns'][5]['search']['value']);
            }
        } else {

            $this->db->like("DATE(CTT_DT_CREATE)", $_POST['columns'][5]['search']['value']);
        }

        if (isset($_POST['columns'][6]['search']['value'])) {
            $this->db->like('stt.DESCRICAO', $_POST['columns'][6]['search']['value']);
        }

        if (isset($_POST['columns'][7]['search']['value'])) {
            $this->db->like('env.DESCRICAO', $_POST['columns'][7]['search']['value']);
        }

        if (!empty($_POST['columns'][9]['search']['value'])) {
            $this->db->like('CTT_TAGS', $_POST['columns'][9]['search']['value']);
        }
    }

    function getcontratos($token)
    {
        $user = $this->infoUser($token);

        $permissoes = explode(', ', $user->permissoes);

        $this->_getcontratos($permissoes, $user);
        $this->_get_custom_contratos();

        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    function filtrocontratos($token)
    {
        $user = $this->infoUser($token);

        $permissoes = explode(', ', $user->permissoes);

        $this->_getcontratos($permissoes, $user);
        $this->_get_custom_contratos();

        $query = $this->db->get();
        return $query->num_rows();
    }

    public function todoscontratos($token)
    {
        //$user = $this->infoUser($token);
        //$permissoes = explode(', ', $user->permissoes);

        $this->db->from($this->tabelas->tb_contratos);
        return $this->db->count_all_results();
    }
    /* CONTRATOS */

    //VERFICA SE JÁ EXISTE COM O MESMO NOME
    function verificaExiste($nome)
    {
        $this->db->select('');
        $this->db->from($this->tabelas->tb_contratos);
        $this->db->where('CTT_DESC', $nome);

        $query = $this->db->get();

        return $query->num_rows();
    }

    function insert($modulo, $data)
    {

        $tbmodulos = array('contratos');

        if (in_array($modulo, $tbmodulos) && $modulo == "contratos") {

            $token = $data['token'];

            if ($data['CTT_URL_DEFAULT'] == 'sim') {
                $contador = $this->verificaExiste($data['CTT_DESC']);

                if (intval($contador) > 0) {
                    $url_info = $this->removeacentos($data['CTT_DESC'] . '_' . (intval($contador) + 1));
                } else {
                    $url_info = $this->removeacentos($data['CTT_DESC']);
                }
            } else {
                $url_info = $data['CTT_URL_INFO'];
            }

            $now = new DateTime();

            $dados = array(
                'CTT_DESC' => $data['CTT_DESC'],
                'MDL_ID' => $data['MDL_ID'],
                'CTT_STATUS' => 1, /* 1-criado, 2-enviado, 3-aprovado, 4-reprovado */
                'CTT_SEND_NOTIFICATION' => 0, /* 0-não enviado, 1-enviado */
                'CTT_SEND_MAIL' => $data['CTT_SEND_MAIL'],
                'CTT_EMAIL_DEFAULT' => $data['CTT_EMAIL_DEFAULT'],
                'CTT_URL_DEFAULT' => $data['CTT_URL_DEFAULT'],
                'CTT_URL_INFO' => $url_info,
                'CTT_SERVICO' => $data['CTT_SERVICO'],
                'CTT_SERVICO_ID' => $data['CTT_SERVICO_ID'],
                'CTT_TAGS' => $data['CTT_TAGS'],
                'CLI_NOME_FANTASIA' => $data['CLI_NOME_FANTASIA'],
                'CLI_RAZAO_SOCIAL' => $data['CLI_RAZAO_SOCIAL'],
                'CLI_CNPJ' => $data['CLI_CNPJ'],
                'CLI_LOGRADOURO_EMPRESA' => $data['CLI_LOGRADOURO_EMPRESA'],
                'CLI_NUMERO_EMPRESA' => $data['CLI_NUMERO_EMPRESA'],
                'CLI_COMPLEMENTO_EMPRESA' => $data['CLI_COMPLEMENTO_EMPRESA'],
                'CLI_BAIRRO_EMPRESA' => $data['CLI_BAIRRO_EMPRESA'],
                'CLI_CIDADE_EMPRESA' => $data['CLI_CIDADE_EMPRESA'],
                'CLI_UF_EMPRESA' => $data['CLI_UF_EMPRESA'],
                'CLI_CEP_EMPRESA' => $data['CLI_CEP_EMPRESA'],
                'CLI_CONTATO_NOME' => $data['CLI_CONTATO_NOME'],
                'CLI_CONTATO_CARGO' => $data['CLI_CONTATO_CARGO'],
                'CLI_RG_CONTATO' => $data['CLI_RG_CONTATO'],
                'CLI_CPF_CONTATO' => $data['CLI_CPF_CONTATO'],
                'CLI_LOGRADOURO_CONTATO' => $data['CLI_LOGRADOURO_CONTATO'],
                'CLI_NUMERO_CONTATO' => $data['CLI_NUMERO_CONTATO'],
                'CLI_COMPLEMENTO_CONTATO' => $data['CLI_COMPLEMENTO_CONTATO'],
                'CLI_BAIRRO_CONTATO' => $data['CLI_BAIRRO_CONTATO'],
                'CLI_CIDADE_CONTATO' => $data['CLI_CIDADE_CONTATO'],
                'CLI_UF_CONTATO' => $data['CLI_UF_CONTATO'],
                'CLI_CEP_CONTATO' => $data['CLI_CEP_CONTATO'],
                'CLI_CONTATO_CARGO' => $data['CLI_CONTATO_CARGO'],
                'CLI_EMAIL_CONTATO' => $data['CLI_EMAIL_CONTATO'],
                'CTT_VALOR_TOTAL' => $data['CTT_VALOR_TOTAL'],
                'CTT_VALOR_PARCELADO' => $data['CTT_VALOR_PARCELADO'],
                'CTT_PARCELAS' => $data['CTT_PARCELAS'],
                'CTT_INTERVALO_PARCELAS' => $data['CTT_INTERVALO_PARCELAS'],
                'CTT_DATA_PRIMEIRA_PARCELA' => $data['CTT_DATA_PRIMEIRA_PARCELA'],
                'CTT_VENCIMENTOS' => $data['CTT_VENCIMENTOS'],
                'SERV_DESC' => $data['SERV_DESC'],
                'CTT_CONVENIOS' => $data['CTT_CONVENIOS'],
                'CTT_OP_PAG_SLCT' => $data['CTT_OP_PAG_SLCT'],
                'CTT_OUTROS_CNPJS' => $data['CTT_OUTROS_CNPJS'],
                'CTT_QTDE_VENDAS' => $data['CTT_QTDE_VENDAS'],
                'CTT_VENDEDOR' => $data['CTT_VENDEDOR'],
                'CTT_DATA_CONTRATO' => $data['CTT_DATA_CONTRATO'],
                'CTT_USER_CREATED' => $this->infoUser($token)->id,
                'CTT_DT_CREATE' => $now->format('Y-m-d H:i:s'),
                'CTT_PERCENTUAL' => 20
            );

            $this->db->insert($this->tabelas->tb_contratos, $dados);

            //adiciona as informações no layout
            $contrato_id = $this->db->insert_id();
            $modelo_id = $data['MDL_ID'];

            $this->gerarLayoutContrato($modelo_id, $contrato_id);

            return $contrato_id;
        } else {
            return false;
        }
    }

    function edit($modulo, $data, $id)
    {

        $tbmodulos = array('contratos');

        if (in_array($modulo, $tbmodulos) && $modulo == "contratos") {

            $token = $data['token'];

            if ($data['CTT_URL_DEFAULT'] == 'sim') {
                $url_info = $this->removeacentos($data['CTT_DESC']);
            } else {
                $url_info = $data['CTT_URL_INFO'];
            }

            $dados = array(
                'CTT_EMAIL_DEFAULT' => $data['CTT_EMAIL_DEFAULT'],
                'CTT_URL_DEFAULT' => $data['CTT_URL_DEFAULT'],
                'CTT_SEND_MAIL' => $data['CTT_SEND_MAIL'],
                'CTT_URL_INFO' => $url_info,
                'CTT_SERVICO' => $data['CTT_SERVICO'],
                'CTT_SERVICO_ID' => $data['CTT_SERVICO_ID'],
                'CTT_DESC' => $data['CTT_DESC'],
                'MDL_ID' => $data['MDL_ID'],
                'CLI_RAZAO_SOCIAL' => $data['CLI_RAZAO_SOCIAL'],
                'CLI_NOME_FANTASIA' => $data['CLI_NOME_FANTASIA'],
                'CLI_CNPJ' => $data['CLI_CNPJ'],
                'CLI_LOGRADOURO_EMPRESA' => $data['CLI_LOGRADOURO_EMPRESA'],
                'CLI_NUMERO_EMPRESA' => $data['CLI_NUMERO_EMPRESA'],
                'CLI_COMPLEMENTO_EMPRESA' => $data['CLI_COMPLEMENTO_EMPRESA'],
                'CLI_BAIRRO_EMPRESA' => $data['CLI_BAIRRO_EMPRESA'],
                'CLI_UF_EMPRESA' => $data['CLI_UF_EMPRESA'],
                'CLI_CIDADE_EMPRESA' => $data['CLI_CIDADE_EMPRESA'],
                'CLI_CEP_EMPRESA' => $data['CLI_CEP_EMPRESA'],
                'CLI_CONTATO_NOME' => $data['CLI_CONTATO_NOME'],
                'CLI_CONTATO_CARGO' => $data['CLI_CONTATO_CARGO'],
                'CLI_RG_CONTATO' => $data['CLI_RG_CONTATO'],
                'CLI_CPF_CONTATO' => $data['CLI_CPF_CONTATO'],
                'CLI_EMAIL_CONTATO' => $data['CLI_EMAIL_CONTATO'],
                'CLI_LOGRADOURO_CONTATO' => $data['CLI_LOGRADOURO_CONTATO'],
                'CLI_NUMERO_CONTATO' => $data['CLI_NUMERO_CONTATO'],
                'CLI_COMPLEMENTO_CONTATO' => $data['CLI_COMPLEMENTO_CONTATO'],
                'CLI_BAIRRO_CONTATO' => $data['CLI_BAIRRO_CONTATO'],
                'CLI_UF_CONTATO' => $data['CLI_UF_CONTATO'],
                'CLI_CIDADE_CONTATO' => $data['CLI_CIDADE_CONTATO'],
                'CLI_CEP_CONTATO' => $data['CLI_CEP_CONTATO'],
                'SERV_DESC' => $data['SERV_DESC'],
                'CTT_TAGS' => $data['CTT_TAGS'],
                'CTT_VALOR_TOTAL' => $data['CTT_VALOR_TOTAL'],
                'CTT_VALOR_PARCELADO' => $data['CTT_VALOR_PARCELADO'],
                'CTT_PARCELAS' => $data['CTT_PARCELAS'],
                'CTT_INTERVALO_PARCELAS' => $data['CTT_INTERVALO_PARCELAS'],
                'CTT_DATA_PRIMEIRA_PARCELA' => $data['CTT_DATA_PRIMEIRA_PARCELA'],
                'CTT_VENCIMENTOS' => $data['CTT_VENCIMENTOS'],
                'CTT_OP_PAG_SLCT' => $data['CTT_OP_PAG_SLCT'],
                'CTT_CONVENIOS' => $data['CTT_CONVENIOS'],
                'CTT_OUTROS_CNPJS' => $data['CTT_OUTROS_CNPJS'],
                'CTT_QTDE_VENDAS' => $data['CTT_QTDE_VENDAS'],
                'CTT_VENDEDOR' => $data['CTT_VENDEDOR'],
                'CTT_DATA_CONTRATO' => $data['CTT_DATA_CONTRATO']
            );

            if ($this->upddata($dados, $this->tabelas->tb_contratos, $id, 'CTT_ID')) {

                $modelo_id = $data['MDL_ID'];

                $this->gerarLayoutContrato($modelo_id, $id);

                return $id;
            } else {
                return null;
            }
        }
    }

    function delete($modulo, $id)
    {

        $tbmodulos = array('contratos');

        if (in_array($modulo, $tbmodulos) && $modulo == "contratos") {
            $this->db->delete($this->tabelas->tb_contratos, array('CTT_ID' => $id));

            if ($this->db->affected_rows()) {
                return true;
            } else {
                return false;
            }
        }
    }

    function ativarInativar($modulo, $id)
    {

        $tbmodulos = array('contratos');

        if (in_array($modulo, $tbmodulos) && $modulo == "contratos") {

            $tipo = null;

            $this->db->select('');
            $this->db->from($this->tabelas->tb_contratos);
            $this->db->where('CTT_ID', $id);
            $this->db->limit(1);

            $query = $this->db->get();

            $tiporeg = $query->row()->CTT_STATUS_VIEW;

            if ($tiporeg == 'S') {
                $tipo = 'N';
            } else {
                $tipo = 'S';
            }

            $dados = array(
                'CTT_STATUS_VIEW' => $tipo
            );

            if ($this->upddata($dados, $this->tabelas->tb_contratos, $id, 'CTT_ID')) {
                return true;
            } else {
                return false;
            }
        }
    }

    function getUF($id)
    {

        if ($id != null || $id != 0) {
            $this->db->select('id_estado as id, est_nome as sigla');
            $this->db->from($this->tabelas->tb_estados);
            $this->db->where('id_estado', $id);

            $query = $this->db->get();

            return $query->row()->sigla;
        } else {

            return null;
        }
    }

    function gerarLayoutContrato($modelo_id, $contrato_id)
    {

        //busca o modelo
        $this->db->select('*');
        $this->db->from($this->tabelas->tb_modelos);
        $this->db->where('MDL_ID', $modelo_id);
        $this->db->limit(1);

        $querymdl = $this->db->get();

        $layout_modelo = $querymdl->row()->MDL_LAYOUT;

        //busca a proposta
        $this->db->select('*');
        $this->db->from($this->tabelas->tb_contratos);
        $this->db->where('CTT_ID', $contrato_id);
        $this->db->limit(1);

        $queryprp = $this->db->get();

        $contrato = $queryprp->row();

        $chaves = array(
            'razao_social' => '[razao]',
            'nome_fantasia' => '[nome-fant]',
            'cnpj_cliente' => '[CNPJ]',
            'end_cliente' => '[ENDERECO-CLIENTE]',
            'nome_contato' => '[nome-rl]',
            'cargo_contato' => '[carg]',
            'rg_contato' => '[identid]',
            'cpf_contato' => '[CPF-RL-CLIENTE]',
            'end_contato' => '[END-RL-CLIENTE]',
            'valor_total' => '[valor-t]',
            'valor_parcelado' => '[valor-p]',
            'parcelas' => '[qtde-parcelas]',
            'detalhe_parcelamento' => '[DETALHE-PARCELAMENTO]',
            'convenios' => '[CONV-PBM]',
            'outros_cnpjs' => '[mCNPJ]',
            'qtde_vendas' => '[numero-vendas]',
            'vendedor' => '[vendedor]',
            'dta_contrato' => '[data-contrato]',
            'ip_usuario' => '[ip-usuario]',
            'data_assinatura' => '[data-assinatura]',
            'chave_gerada' => '[chave-gerada]',
            'intervalo_parcelas' => '[intervalo-parcelas]',
            'primeira_parcela' => '[primeira-parcela]'
        );

        $detalhe_parcelamento = "";
        $intervalo_parcelas = "";
        $primeiraparcela = "";

        $valor_parcelado = str_replace(".", "", $contrato->CTT_VALOR_PARCELADO);
        $valor = (float) str_replace(",", ".", $valor_parcelado);

        $oppag = $contrato->CTT_OP_PAG_SLCT;

        $par = (int) $contrato->CTT_PARCELAS;

        if ((int) $oppag == 1) {
            $detalhe_parcelamento .= "R$ " . $contrato->CTT_VALOR_TOTAL . ' à vista';

            $dataassinatura = $this->atualizaData($contrato->CTT_DATA_PRIMEIRA_PARCELA, $contrato_id);

            $dataaprovacao = $contrato->CTT_DT_APROV;

            $datapripag = new DateTime($dataaprovacao);
            $datapripag->add(new DateInterval("P3D"));

            $primeiraparcela = $datapripag->format('d/m/Y');
        } else {
            $valparcela = $valor / $par;

            $dataassinatura = $this->atualizaData($contrato->CTT_DATA_PRIMEIRA_PARCELA, $contrato_id);
            $intervaloparcelas = (int) $contrato->CTT_INTERVALO_PARCELAS;

            $datas = array();

            for ($i = 1; $i <= $par; $i++) {
                if ($i == 1) {
                    array_push($datas, $dataassinatura);
                } else {
                    $intervalo = $i * $intervaloparcelas;
                    $strinterval = "P" . $intervalo . "D";

                    $datapag = DateTime::createFromFormat('d/m/Y', $dataassinatura);
                    $datapag->add(new DateInterval($strinterval));
                    array_push($datas, $datapag->format('d/m/Y'));
                }
            }

            $detalhe_parcelamento = $par . " parcelas de R$ " . number_format($valparcela, 2, ',', '.');
            $intervalo_parcelas = $contrato->CTT_INTERVALO_PARCELAS . " dias";

            $dataaprovacao = $contrato->CTT_DT_APROV;

            $datapripag = new DateTime($dataaprovacao);
            $datapripag->add(new DateInterval("P3D"));

            $primeiraparcela = $datapripag->format('d/m/Y');
        }

        $uf = $this->getUF((int) $contrato->CLI_UF_EMPRESA);
        $ufcontato = $this->getUF((int) $contrato->CLI_UF_CONTATO);

        $troca = array(
            'razao_social' => $contrato->CLI_RAZAO_SOCIAL,
            'nome_fantasia' => $contrato->CLI_NOME_FANTASIA,
            'cnpj_cliente' => $contrato->CLI_CNPJ,
            'end_cliente' => $contrato->CLI_LOGRADOURO_EMPRESA . ", " . $contrato->CLI_NUMERO_EMPRESA . " " . $contrato->CLI_COMPLEMENTO_EMPRESA . " - " . $contrato->CLI_BAIRRO_EMPRESA . " - " . $contrato->CLI_CIDADE_EMPRESA . " - " . $uf . ", CEP: " . $contrato->CLI_CEP_EMPRESA,
            'nome_contato' => $contrato->CLI_CONTATO_NOME,
            'cargo_contato' => $contrato->CLI_CONTATO_CARGO,
            'rg_contato' => $contrato->CLI_RG_CONTATO,
            'cpf_contato' => $contrato->CLI_CPF_CONTATO,
            'end_contato' => $contrato->CLI_LOGRADOURO_CONTATO . ", " . $contrato->CLI_NUMERO_CONTATO . " " . $contrato->CLI_COMPLEMENTO_CONTATO . " - " . $contrato->CLI_BAIRRO_CONTATO . " - " . $contrato->CLI_CIDADE_CONTATO . " - " . $ufcontato . ", CEP: " . $contrato->CLI_CEP_CONTATO,
            'valor_total' => 'R$ ' . $contrato->CTT_VALOR_TOTAL,
            'valor_parcelado' => 'R$ ' . $contrato->CTT_VALOR_PARCELADO,
            'parcelas' => $contrato->CTT_PARCELAS,
            'detalhe_parcelamento' => $detalhe_parcelamento,
            'convenios' => $contrato->CTT_CONVENIOS,
            'outros_cnpjs' => $contrato->CTT_OUTROS_CNPJS,
            'qtde_vendas' => $contrato->CTT_QTDE_VENDAS,
            'vendedor' => $contrato->CTT_VENDEDOR,
            'dta_contrato' => $contrato->CTT_DATA_CONTRATO,
            'ip_usuario' => $contrato->CTT_IP_CLIENTE,
            'data_assinatura' => $contrato->CTT_DT_APROV,
            'chave_gerada' => $contrato->CTT_KEY_ASSINATURA,
            'intervalo_parcelas' => $intervalo_parcelas,
            'primeira_parcela' => $primeiraparcela,
        );

        $data = array(
            'CTT_LAYOUT' => str_replace($chaves, $troca, $layout_modelo)
        );

        if ($this->upddata($data, $this->tabelas->tb_contratos, $contrato_id, 'CTT_ID')) {
            return true;
        } else {
            return false;
        }
    }

    function atualizaData($data, $id)
    {
        $now = new DateTime();
        $hoje = $now->format("d/m/Y");

        $dthoje = DateTime::createFromFormat('d/m/Y', $hoje);
        $dthoje->add(new DateInterval("P3D"));

        if (strtotime($data) <= strtotime($dthoje->format("d/m/Y"))) {

            //atualiza data da primeira parcela do contrato
            $datactt = array(
                'CTT_DATA_PRIMEIRA_PARCELA' => $dthoje->format("d/m/Y")
            );

            $this->upddata($datactt, $this->tabelas->tb_contratos, $id, 'CTT_ID');

            //atualiza data da primeira parcela da proposta
            $dataprp = array(
                'PRP_DATA_PRIMEIRA_PARCELA' => $dthoje->format("d/m/Y")
            );

            $this->upddata($dataprp, $this->tabelas->tb_propostas, $id, 'CTT_ID');

            return $dthoje->format("d/m/Y");
        } else {
            return $data;
        }
    }

    function get($modulo, $id)
    {
        $tbmodulos = array('contratos', 'servicos');

        if (in_array($modulo, $tbmodulos) && $modulo == "contratos") {
            $this->db->select('CTT_ID as id,
					MDL_ID as modelo_contrato,
					CTT_STATUS as status,
					CTT_DESC as descricao,
					CTT_SEND_MAIL as email_notificacao,
					CTT_EMAIL_DEFAULT as email_padrao,
					CTT_URL_INFO as urlcontrato,
					CTT_URL_DEFAULT as url_padrao,
					CTT_SERVICO as outro_servicos,
					CTT_SERVICO_ID as servico,
					CTT_TAGS as tags,
					CLI_RAZAO_SOCIAL as razao_social,
					CLI_NOME_FANTASIA as nome_fantasia,
					CLI_CNPJ as cnpj,
					CLI_LOGRADOURO_EMPRESA as logradouro,
					CLI_NUMERO_EMPRESA as numero,
					CLI_COMPLEMENTO_EMPRESA as complemento,
					CLI_BAIRRO_EMPRESA as bairro,
					CLI_UF_EMPRESA as estado,
					CLI_CIDADE_EMPRESA as cidade,
					CLI_CEP_EMPRESA as cep,
					CLI_CONTATO_NOME as nome_contato,
					CLI_CONTATO_CARGO as cargo_contato,
					CLI_RG_CONTATO as rg_contato,
					CLI_CPF_CONTATO as cpf_contato,
					CLI_EMAIL_CONTATO as email_contato,
					CLI_LOGRADOURO_CONTATO as logradouro_contato,
					CLI_NUMERO_CONTATO as numero_contato,
					CLI_COMPLEMENTO_CONTATO as complemento_contato,
					CLI_BAIRRO_CONTATO as bairro_contato,
					CLI_UF_CONTATO as estado_contato,
					CLI_CIDADE_CONTATO as cidade_contato,
					CLI_CEP_CONTATO as cep_contato,
					CTT_VALOR_TOTAL as valor_total,
					CTT_VALOR_PARCELADO as valor_parcelado,
					CTT_PARCELAS as parcelas,
                    CTT_OP_PAG_SLCT as forma_pagamento,
                    CTT_INTERVALO_PARCELAS as intervalo_parcelas,
                    CTT_DATA_PRIMEIRA_PARCELA as primeira_parcela,
					CTT_CONVENIOS as convenios,
					CTT_OUTROS_CNPJS as outros_cnpjs,
					CTT_QTDE_VENDAS as qtde_vendas,
					CTT_VENDEDOR as vendedor,
					CTT_DATA_CONTRATO as data_contrato,
					CTT_DT_CREATE as data_gerado,
					CTT_LAYOUT as texto,
					CTT_AUTO as automatico');
            $this->db->from($this->tabelas->tb_contratos);
            $this->db->where('CTT_ID', $id);
            $this->db->limit(1);

            $query = $this->db->get();

            return $query->result();
        }

        if (in_array($modulo, $tbmodulos) && $modulo == "servicos") {
            $this->db->select('SERV_ID as id,
					SERV_DESC as nome,
					SERV_INFO as descricao');
            $this->db->from($this->tabelas->tb_servicos);
            $this->db->where('SERV_ID', $id);
            $this->db->limit(1);

            $query = $this->db->get();

            return $query->result();
        }
    }

    function duplicar($modulo, $id, $token)
    {
        $tbmodulos = array('contratos');

        if (in_array($modulo, $tbmodulos) && $modulo == "contratos") {
            $this->db->select('*');
            $this->db->from($this->tabelas->tb_contratos);
            $this->db->where('CTT_ID', $id);
            $this->db->limit(1);

            $linha = $this->db->get()->row();

            $now = new DateTime();

            $data['MDL_ID'] = $linha->MDL_ID;
            $data['CTT_STATUS'] = 1;
            $data['CTT_SEND_NOTIFICATION'] = 0;
            $data['CTT_DESC'] = $linha->CTT_DESC;
            $data['CTT_SEND_MAIL'] = $linha->CTT_SEND_MAIL;
            $data['CTT_EMAIL_DEFAULT'] = $linha->CTT_EMAIL_DEFAULT;
            $data['CTT_URL_INFO'] = $linha->CTT_URL_INFO;
            $data['CTT_URL_DEFAULT'] = $linha->CTT_URL_DEFAULT;
            $data['CTT_SERVICO'] = $linha->CTT_SERVICO;
            $data['CTT_SERVICO_ID'] = $linha->CTT_SERVICO_ID;
            $data['CTT_TAGS'] = $linha->CTT_TAGS;
            $data['CLI_RAZAO_SOCIAL'] = $linha->CLI_RAZAO_SOCIAL;
            $data['CLI_NOME_FANTASIA'] = $linha->CLI_NOME_FANTASIA;
            $data['CLI_CNPJ'] = $linha->CLI_CNPJ;
            $data['CLI_LOGRADOURO_EMPRESA'] = $linha->CLI_LOGRADOURO_EMPRESA;
            $data['CLI_NUMERO_EMPRESA'] = $linha->CLI_NUMERO_EMPRESA;
            $data['CLI_COMPLEMENTO_EMPRESA'] = $linha->CLI_COMPLEMENTO_EMPRESA;
            $data['CLI_BAIRRO_EMPRESA'] = $linha->CLI_BAIRRO_EMPRESA;
            $data['CLI_UF_EMPRESA'] = $linha->CLI_UF_EMPRESA;
            $data['CLI_CIDADE_EMPRESA'] = $linha->CLI_CIDADE_EMPRESA;
            $data['CLI_CEP_EMPRESA'] = $linha->CLI_CEP_EMPRESA;
            $data['CLI_CONTATO_NOME'] = $linha->CLI_CONTATO_NOME;
            $data['CLI_CONTATO_CARGO'] = $linha->CLI_CONTATO_CARGO;
            $data['CLI_RG_CONTATO'] = $linha->CLI_RG_CONTATO;
            $data['CLI_CPF_CONTATO'] = $linha->CLI_CPF_CONTATO;
            $data['CLI_EMAIL_CONTATO'] = $linha->CLI_EMAIL_CONTATO;
            $data['CLI_LOGRADOURO_CONTATO'] = $linha->CLI_LOGRADOURO_CONTATO;
            $data['CLI_NUMERO_CONTATO'] = $linha->CLI_NUMERO_CONTATO;
            $data['CLI_COMPLEMENTO_CONTATO'] = $linha->CLI_COMPLEMENTO_CONTATO;
            $data['CLI_BAIRRO_CONTATO'] = $linha->CLI_BAIRRO_CONTATO;
            $data['CLI_UF_CONTATO'] = $linha->CLI_UF_CONTATO;
            $data['CLI_CIDADE_CONTATO'] = $linha->CLI_CIDADE_CONTATO;
            $data['CLI_CEP_CONTATO'] = $linha->CLI_CEP_CONTATO;
            $data['CTT_VALOR_TOTAL'] = $linha->CTT_VALOR_TOTAL;
            $data['CTT_VALOR_PARCELADO'] = $linha->CTT_VALOR_PARCELADO;
            $data['CTT_PARCELAS'] = $linha->CTT_PARCELAS;
            $data['CTT_INTERVALO_PARCELAS'] = $linha->CTT_INTERVALO_PARCELAS;
            $data['CTT_DATA_PRIMEIRA_PARCELA'] = $linha->CTT_DATA_PRIMEIRA_PARCELA;
            $data['CTT_CONVENIOS'] = $linha->CTT_CONVENIOS;
            $data['CTT_OUTROS_CNPJS'] = $linha->CTT_OUTROS_CNPJS;
            $data['CTT_QTDE_VENDAS'] = $linha->CTT_QTDE_VENDAS;
            $data['CTT_VENDEDOR'] = $linha->CTT_VENDEDOR;
            $data['CTT_DATA_CONTRATO'] = $linha->CTT_DATA_CONTRATO;
            $data['CTT_OP_PAG_SLCT'] = $linha->CTT_OP_PAG_SLCT;
            $data['CTT_USER_CREATED'] = $this->infoUser($token)->id;
            $data['CTT_DT_CREATE'] = $now->format('Y-m-d H:i:s');
            $data['CTT_LAYOUT'] = $linha->CTT_LAYOUT;
            $data['CTT_AUTO'] = 1;
            $data['CTT_PERCENTUAL'] = 20;

            $this->db->insert($this->tabelas->tb_contratos, $data);

            return ($this->db->affected_rows() != 1) ? false : true;
        }
    }

    function upddata($data, $table, $id, $field)
    {
        $this->db->where($field, $id);
        $this->db->update($table, $data);

        return true;
    }

    function disconnect($token, $data)
    {
        return $this->upddata($data, $this->tabelas->tb_acesso, $token, 'AC_TOKEN');
    }

    function validaToken($token)
    {

        $now = date("Y-m-d H:i:s");

        $this->db->select('*');
        $this->db->from($this->tabelas->tb_acesso);
        $this->db->where('AC_TOKEN', $token);
        $this->db->where('AC_DATA_EXP >= "' . $now . '"');

        $query = $this->db->get();

        $result = $query->result();

        if (!empty($result)) {

            return true;
        } else {

            $acesso = array(
                'CONNECTED' => 0
            );

            $this->upddata($acesso, $this->tabelas->tb_acesso, $token, 'AC_TOKEN');

            return false;
        }
    }

    function infoSistema($modulo)
    {
        $tbmodulos = array('configuracoes', 'propostas', 'contratos');

        if ((in_array($modulo, $tbmodulos))) {
            $this->db->select('*');
            $this->db->from($this->tabelas->tb_config);
            $this->db->limit(1);

            $query = $this->db->get();

            return $query->result();
        } else {
            return null;
        }
    }

    function infoUser($token)
    {
        if ($this->validaToken($token)) {
            $this->db->select('*');
            $this->db->from($this->tabelas->tb_acesso);
            $this->db->where('AC_TOKEN', $token);

            $query = $this->db->get();

            $result = $query->row();

            if (!empty($result)) {

                $this->db->select('
						us.US_ID as id,
						us.USER_NAME_FULL as nome_completo,
						us.US_MAIL as email,
						us.US_LOGIN as login,
						us.US_STATUS as status,
						us.TYPE_ID as tipo,
						tp.TYPE_PERMISSIONS as permissoes');
                $this->db->from($this->tabelas->tb_acesso . " acc");
                $this->db->join($this->tabelas->tb_usuario . " us", 'us.US_ID = acc.US_ID');
                $this->db->join($this->tabelas->tb_tipos_usuarios . " tp", 'tp.TYPE_ID = us.TYPE_ID');
                $this->db->where('acc.AC_TOKEN', $token);
                $this->db->limit(1);

                $query = $this->db->get();
                $array = $query->row();

                return $array;
            }
        } else {
            return null;
        }
    }

    function removeacentos($str)
    {
        $comum = new Comum();

        return $comum->tirarAcentos($str);
    }
}

?>