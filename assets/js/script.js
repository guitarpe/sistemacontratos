// Element Attribute Helper
function attrDefault($el, data_var, default_val)
{
	if(typeof $el.data(data_var) != 'undefined')
	{
		return $el.data(data_var);
	}

	return default_val;
}

function onAddTag(tag) {
	alert("Added a tag: " + tag);
}
function onRemoveTag(tag) {
	alert("Removed a tag: " + tag);
}

function onChangeTag(input,tag) {
	alert("Changed a tag: " + tag);
}

jQuery(document).ready(function($){
	
	$('.currency').on('keydown',function(e){    
	    if ($.inArray(e.keyCode, [9, 27, 13]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 40)) {
	        return;
	    }
	    
	    e.preventDefault();
	    
	    if($.inArray(e.keyCode,[8,46]) !== -1){
	        $(this).val('');
	        return;
	    }
	    
	    var a = ["a","b","c","d","e","f","g","h","i","`"];
	    var n = ["1","2","3","4","5","6","7","8","9","0"];
	    
	    var value = $(this).val();
	    var clean = value.replace(/,/g,'').replace(/\./g,'').replace(/^0+/, '');   
	    
	    var charCode = String.fromCharCode(e.keyCode);
	    var p = $.inArray(charCode,a);
	    
	    if(p !== -1){
	        value = clean + n[p];
	        
	        if(value.length == 2) value = '0' + value;
	        if(value.length == 1) value = '00' + value;
	        
	        var formatted = '';
	        for(var i=0;i<value.length;i++){
	            var sep = '';
	            if(i == 2) sep = ',';
	            if(i > 3 && (i+1) % 3 == 0) sep = '.';
	            formatted = value.substring(value.length-1-i,value.length-i) + sep + formatted;
	        }
	        
	        $(this).val(formatted);
	    }else{
	    	console.log(e.keyCode);
	    }
	    
	    return;
	});
	
	$('.numbers').keyup(function () { 
	    this.value = this.value.replace(/[^0-9\/]/g,'');
	});
	
	$('.percentual').keyup(function () { 
	    this.value = this.value.replace(/[^0-9.\/]/g,'');
	});
	
	replaceCheckboxes();
	
	$('[data-toggle="buttons-checkbox"]').each(function(){
		var $buttons = $(this).children();

		$buttons.each(function(i, el){
			var $this = $(el);

			$this.click(function(ev){
				$this.removeClass('active');
			});
		});
	});
		
	$(".datepicker").each(function(i, el){
		var $this = $(el);
		opts = {
			format: attrDefault($this, 'format', 'dd/mm/yyyy'),
			startDate: attrDefault($this, 'startDate', ''),
			endDate: attrDefault($this, 'endDate', ''),
			daysOfWeekDisabled: attrDefault($this, 'disabledDays', ''),
			startView: attrDefault($this, 'startView', 0),
			rtl: rtl()
		};
		$n = $this.next();
		$p = $this.prev();
		$this.datepicker(opts);

		if($n.is('.input-group-addon') && $n.has('a')){
			$n.on('click', function(ev){
				ev.preventDefault();
				$this.datepicker('show');
			});
		}

		if($p.is('.input-group-addon') && $p.has('a')){
			$p.on('click', function(ev){
				ev.preventDefault();
				$this.datepicker('show');
			});
		}
	});
		
	// Timepicker
	$(".timepicker").each(function(i, el){
		var $this = $(el),
		opts = {
			template: attrDefault($this, 'template', false),
			showSeconds: attrDefault($this, 'showSeconds', false),
			defaultTime: attrDefault($this, 'defaultTime', 'current'),
			showMeridian: attrDefault($this, 'showMeridian', true),
			minuteStep: attrDefault($this, 'minuteStep', 15),
			secondStep: attrDefault($this, 'secondStep', 15)
		};
		$n = $this.next();
		$p = $this.prev();
		$this.timepicker(opts);

		if($n.is('.input-group-addon') && $n.has('a')){
			$n.on('click', function(ev){
				ev.preventDefault();
				$this.timepicker('showWidget');
			});
		}

		if($p.is('.input-group-addon') && $p.has('a')){
			$p.on('click', function(ev){
				ev.preventDefault();
				$this.timepicker('showWidget');
			});
		}
	});
		
		// Colorpicker
	$(".colorpicker").each(function(i, el){
		var $this = $(el);
		opts = {
			//format: attrDefault($this, 'format', false)
		};
		$n = $this.next();
		$p = $this.prev();

		$preview = $this.siblings('.input-group-addon').find('.color-preview');

		$this.colorpicker(opts);

		if($n.is('.input-group-addon') && $n.has('a')){
			$n.on('click', function(ev){
				ev.preventDefault();
				$this.colorpicker('show');
			});
		}

		if($p.is('.input-group-addon') && $p.has('a')){
			$p.on('click', function(ev){
				ev.preventDefault();
				$this.colorpicker('show');
			});
		}

		if($preview.length){
			$this.on('changeColor', function(ev){
				$preview.css('background-color', ev.color.toHex());
			});
			if($this.val().length){
				$preview.css('background-color', $this.val());
			}
		}
	});

	// Date Range Picker
	$(".daterange").each(function(i, el){
		// Change the range as you desire
		var ranges = {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
			'Last 7 Days': [moment().subtract('days', 6), moment()],
			'Last 30 Days': [moment().subtract('days', 29), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
		};

		var $this = $(el);
		opts = {
			format: attrDefault($this, 'format', 'MM/DD/YYYY'),
			timePicker: attrDefault($this, 'timePicker', false),
			timePickerIncrement: attrDefault($this, 'timePickerIncrement', false),
			separator: attrDefault($this, 'separator', ' - '),
		};
		min_date = attrDefault($this, 'minDate', '');
		max_date = attrDefault($this, 'maxDate', '');
		start_date = attrDefault($this, 'startDate', '');
		end_date = attrDefault($this, 'endDate', '');

		if($this.hasClass('add-ranges')){
			opts['ranges'] = ranges;
		}

		if(min_date.length){
			opts['minDate'] = min_date;
		}

		if(max_date.length){
			opts['maxDate'] = max_date;
		}

		if(start_date.length){
			opts['startDate'] = start_date;
		}

		if(end_date.length){
			opts['endDate'] = end_date;
		}

		$this.daterangepicker(opts, function(start, end){
			var drp = $this.data('daterangepicker');

			if($this.is('[data-callback]')){
				//daterange_callback(start, end);
				callback_test(start, end);
			}

			if($this.hasClass('daterange-inline')){
				$this.find('span').html(start.format(drp.format) + drp.separator + end.format(drp.format));
			}
		});
	});

	// Input Mask
	$("[data-mask]").each(function(i, el){
		var $this = $(el);
		mask = $this.data('mask').toString();
		opts = {
			numericInput: attrDefault($this, 'numeric', false),
			radixPoint: attrDefault($this, 'radixPoint', ''),
			rightAlignNumerics: attrDefault($this, 'numericAlign', 'left') == 'right'
		};
		placeholder = attrDefault($this, 'placeholder', '');
		is_regex = attrDefault($this, 'isRegex', '');

		if(placeholder.length){
			opts[placeholder] = placeholder;
		}

		switch(mask.toLowerCase()){
			case "percentual":
				mask = "9.9999";
				break;
			case "cep":
				mask = "99999-999";
				break;
			case "cnpj":
				mask = "99.999.999/9999-99";
				break;
			case "cpf":
				mask = "999.999.999-99";
				break;
			case "rg":
				mask = "99.999.999-9";
				break;
			case "celphone":
				mask = "(99) 9999-99999";
				break;
			case "phone":
				mask = "(99) 9999-9999";
				break;
			case "date":
				mask = "99/99/9999";
				break;
			case "currency":
			case "rcurrency":
				var sign = attrDefault($this, 'sign', '$');;
				mask = "999,999,999.99";
				if($this.data('mask').toLowerCase() == 'rcurrency'){
					mask += ' ' + sign;
				}else{
					mask = sign + ' ' + mask;
				}

				opts.numericInput = true;
				opts.rightAlignNumerics = false;
				opts.radixPoint = '.';
				break;
				
			case "email":
				mask = 'Regex';
				//opts.regex = "[a-zA-Z0-9._%-]+@[a-zA-Z0-9-]+\\.[a-zA-Z]{2,3}";
				opts.regex = "[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}";
				break;

			case "fdecimal":
				mask = 'decimal';
				$.extend(opts, {
					autoGroup		: true,
					groupSize		: 3,
					radixPoint		: attrDefault($this, 'rad', '.'),
					groupSeparator	: attrDefault($this, 'dec', ',')
				});
		}

		if(is_regex){
			opts.regex = mask;
			mask = 'Regex';
		}

		$this.inputmask(mask, opts);
	});

	$(".form-wizard").each(function(i, el){
		var $this = $(el);
			$progress = $this.find(".steps-progress div");
			_index = $this.find('> ul > li.active').index();

		// Validation
		var checkFormWizardValidaion = function(tab, navigation, index){
  			if($this.hasClass('validate')){
				var $valid = $this.valid();

				if( ! $valid){
					$this.data('validator').focusInvalid();
					return false;
				}
			}

	  		return true;
		};

		$this.bootstrapWizard({
			tabClass: "",
	  		onTabShow: function($tab, $navigation, index){
				setCurrentProgressTab($this, $navigation, $tab, $progress, index);
	  		},

	  		onNext: checkFormWizardValidaion,
	  		onTabClick: checkFormWizardValidaion
	  	});

	  	$this.data('bootstrapWizard').show( _index );
	});
});

jQuery('.leftpanel .nav .parent > a').click(function() {
    
    var coll = jQuery(this).parents('.collapsed').length;
    
    if (!coll) {
       jQuery('.leftpanel .nav .parent-focus').each(function() {
          jQuery(this).find('.children').slideUp('fast');
          jQuery(this).removeClass('parent-focus');
       });
       
       var child = jQuery(this).parent().find('.children');
       if(!child.is(':visible')) {
          child.slideDown('fast');
          if(!child.parent().hasClass('active'))
             child.parent().addClass('parent-focus');
       } else {
          child.slideUp('fast');
          child.parent().removeClass('parent-focus');
       }
    }
    return false;
 
    // Menu Toggle
    jQuery('.menu-collapse').click(function() {
    	if (!$('body').hasClass('hidden-left')) {
    		if ($('.headerwrapper').hasClass('collapsed')) {
    			$('.headerwrapper, .mainwrapper').removeClass('collapsed');
    		} else {
    			$('.headerwrapper, .mainwrapper').addClass('collapsed');
    			$('.children').hide(); // hide sub-menu if leave open
    		}
    	} else {
    		if (!$('body').hasClass('show-left')) {
    			$('body').addClass('show-left');
    		} else {
    			$('body').removeClass('show-left');
    		}
    	}
    	return false;
    });
 
    // Add class nav-hover to mene. Useful for viewing sub-menu
    jQuery('.leftpanel .nav li').hover(function(){
    	$(this).addClass('nav-hover');
    }, function(){
    	$(this).removeClass('nav-hover');
    });
 
    // For Media Queries
    jQuery(window).resize(function() {
    	hideMenu();
    });
});

 hideMenu(); // for loading/refreshing the page
 function hideMenu() {
    
    if($('.header-right').css('position') == 'relative') {
       $('body').addClass('hidden-left');
       $('.headerwrapper, .mainwrapper').removeClass('collapsed');
    } else {
       $('body').removeClass('hidden-left');
    }
    
    // Seach form move to left
    if ($(window).width() <= 360) {
       if ($('.leftpanel .form-search').length == 0) {
          $('.form-search').insertAfter($('.profile-left'));
       }
    } else {
       if ($('.header-right .form-search').length == 0) {
          $('.form-search').insertBefore($('.btn-group-notification'));
       }
    }
 }
 
 collapsedMenu(); // for loading/refreshing the page
 function collapsedMenu() {
    
    if($('.logo').css('position') == 'relative') {
       $('.headerwrapper, .mainwrapper').addClass('collapsed');
    } else {
       $('.headerwrapper, .mainwrapper').removeClass('collapsed');
    }
 }

function replaceCheckboxes()
{
	var $ = jQuery;

	$(".checkbox-replace:not(.neon-cb-replacement), .radio-replace:not(.neon-cb-replacement)").each(function(i, el){
		var $this = $(el),
			$input = $this.find('input:first'),
			$wrapper = $('<label class="cb-wrapper" />'),
			$checked = $('<div class="checked" />'),
			checked_class = 'checked',
			is_radio = $input.is('[type="radio"]'),
			$related,
			name = $input.attr('name');

		$this.addClass('neon-cb-replacement');

		$input.wrap($wrapper);

		$wrapper = $input.parent();

		$wrapper.append($checked).next('label').on('click', function(ev){
			$wrapper.click();
		});

		$input.on('change', function(ev){
			if(is_radio){
				//$(".neon-cb-replacement input[type=radio][name='"+name+"']").closest('.neon-cb-replacement').removeClass(checked_class);
				$(".neon-cb-replacement input[type=radio][name='"+name+"']:not(:checked)").closest('.neon-cb-replacement').removeClass(checked_class);
			}

			if($input.is(':disabled')){
				$wrapper.addClass('disabled');
			}

			$this[$input.is(':checked') ? 'addClass' : 'removeClass'](checked_class);

		}).trigger('change');
	});
}